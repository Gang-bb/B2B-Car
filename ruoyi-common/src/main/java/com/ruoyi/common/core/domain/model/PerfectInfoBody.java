package com.ruoyi.common.core.domain.model;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * 用户app登录完善信息请求参数
 *
 * @author Gangbb
 * @date 2023-03-28
 **/
@Data
public class PerfectInfoBody {

    /**
     * 用户真实姓名
     */
    @NotBlank(message = "姓名必填")
    private String realName;
    /**
     * 用户公司名称
     */
    @NotBlank(message = "公司名称必填")
    private String companyName;
    /**
     * 所在地区id
     */
    @NotNull(message = "地区必填")
    private Long areaId;
    /**
     * 所在地区全称
     */
    @NotBlank(message = "地区必填")
    private String areaMergeName;
    /**
     * 地址
     */
    @NotBlank(message = "地址必填")
    private String operateAddress;
}
