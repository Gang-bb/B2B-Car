package com.ruoyi.common.core.domain.model;

import lombok.Data;

import java.io.Serializable;

/**
 * 用户app我的页面信息vo
 *
 * @author Gangbb
 * @date 2023-03-31
 **/
@Data
public class UserInfoAppMineVo implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 用户真实姓名
     */
    private String realName;
    /**
     * 用户公司名称
     */
    private String companyName;
    /**
     * 个人认证是否通过(0-否 1-是)
     */
    private Integer personalAuthRes;
    /**
     * 公司认证是否通过(0-否 1-是)
     */
    private Integer companyAuthRes;
    /**
     * 授信认证是否通过(0-否 1-是)
     */
    private Integer creditAuth;
    /**
     * 经营汽车品牌id(多个逗号隔开,最多12个)
     */
    private String operateBrandId;
    /**
     * 经营汽车品牌名称(多个逗号隔开,最多12个)
     */
    private String operateBrandName;
    /**
     * 经营汽车品牌已有个数
     */
    private Integer operateBrandCount;
}
