package com.ruoyi.common.core.domain.model;

import lombok.Data;

import java.io.Serializable;

/**
 * 短信登录返回参数
 *
 * @author Gangbb
 * @date 2023-03-31
 **/
@Data
public class SmsLoginVo implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 用户是否已经首登完善信息
     */
    private Boolean isPerfectInfo;

    /**
     * token
     */
    private String token;

    /**
     * 用户我的页面信息
     */
    private UserInfoAppMineVo userInfoMine;
}
