package com.ruoyi.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;


/**
 * 用户角色枚举
 *
 * @author Gangbb
 * @date 2022/7/4
 **/
@Getter
@AllArgsConstructor
public enum RoleEnum {


    ADMIN(1L, "admin","超级管理员"),
    VISITOR(2L, "visitor","app游客"),
    PA(3L, "pa","app游客"),
    CA(4L, "ca","app游客"),
    SYS_ADMIN(5L, "sys_admin","系统管理员"),
    ;
    private final Long roleId;
    private final String key;
    private final String name;



}
