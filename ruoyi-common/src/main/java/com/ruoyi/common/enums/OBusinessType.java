package com.ruoyi.common.enums;


import lombok.AllArgsConstructor;
import lombok.Getter;


/**
 * 存储关联业务枚举
 *
 * @author Gangbb
 * @date 2023-03-28
 **/
@Getter
@AllArgsConstructor
public enum OBusinessType {

    TEST("1", "测试"),
    CAR_BRAND("2", "汽车品牌"),
    CAR_SOURCE_RELEASE("3", "车源发布图片"),
    ;
    private final String code;
    private final String name;
}
