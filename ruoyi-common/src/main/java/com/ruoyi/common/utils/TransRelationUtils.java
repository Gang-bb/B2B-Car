package com.ruoyi.common.utils;

import cn.hutool.core.collection.CollStreamUtil;
import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.ReflectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.core.toolkit.support.SFunction;
import com.ruoyi.common.utils.spring.SpringUtils;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;

/**
 * 翻译关联字段工具
 *
 * @author Gangbb
 * @date 2023-04-06
 **/
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class TransRelationUtils {


    /**
     * 获取翻译map
     * (例如：<xxId, xxId对应的name>)
     *
     * @param voList          需要翻译的列表
     * @param voIdFunction    需要翻译单项实体中的id字段Function
     * @param mapper          数据库操作mapper
     * @param idFunction      实体id字段Function
     * @param columnFunction  实体需要转换字段Function
     * @return map
     * @date 2023-04-06
     **/
    public static <E, T, D, M extends BaseMapper<D>> Map<?, ?> getTransMap(List<E> voList, Function<E, T> voIdFunction,
                                                                           Class<M> mapper, SFunction<D, ?> idFunction,
                                                                           SFunction<D, ?> columnFunction) {
        Set<T> idList = CollStreamUtil.toSet(voList, voIdFunction);
        BaseMapper<D> mapperInstance = SpringUtils.getBean(mapper);
        LambdaQueryWrapper<D> queryWrapper = Wrappers.lambdaQuery();
        queryWrapper.select(idFunction, columnFunction);
        queryWrapper.in(idFunction, idList);
        List<D> entityList = mapperInstance.selectList(queryWrapper);
        return CollStreamUtil.toMap(entityList, idFunction, columnFunction);
    }

    /**
     * 获取翻译对象map
     * (例如：<xxId, xxId对应的对象>)
     *
     * @param voList          需要翻译的列表
     * @param voIdFunction    需要翻译单项实体中的id字段Function
     * @param mapper          数据库操作mapper
     * @param idFunction      实体id字段Function
     * @return Map<?,D>
     * @date 2023-04-16
     **/
    public static <E, T, D, M extends BaseMapper<D>> Map<?, D> getTransMapObj(List<E> voList, Function<E, T> voIdFunction,
                                                                           Class<M> mapper, SFunction<D, ?> idFunction) {

        Set<T> idList = CollStreamUtil.toSet(voList, voIdFunction);
        BaseMapper<D> mapperInstance = SpringUtils.getBean(mapper);
        LambdaQueryWrapper<D> queryWrapper = Wrappers.lambdaQuery();
        queryWrapper.in(idFunction, idList);
        List<D> entityList = mapperInstance.selectList(queryWrapper);
        return CollStreamUtil.toIdentityMap(entityList, idFunction);
    }


    /**
     * 翻译关联id字段到其他属性
     *
     * @param voList          需要翻译的列表
     * @param voIdFunction    需要翻译单项实体中的id字段Function
     * @param mapper          数据库操作mapper
     * @param idFunction      实体id字段Function
     * @param transColumn     需要翻译列表中需要翻译字段名称
     * @date 2023-04-06
     **/
    public static <E, T, D, M extends BaseMapper<D>> void transIdToOther(List<E> voList, Function<E, T> voIdFunction,
                                                                           Class<M> mapper, SFunction<D, ?> idFunction,
                                                                           SFunction<D, ?> nameFunction, String transColumn) {
        Map<?, ?> transMap = getTransMap(voList, voIdFunction, mapper, idFunction, nameFunction);
        if(MapUtil.isNotEmpty(transMap)){
            String setColumMethodName = "set" + StrUtil.upperFirst(transColumn);
            voList.forEach(item -> {
                ReflectUtil.invoke(item, setColumMethodName, transMap.get(voIdFunction.apply(item)));
            });
        }
    }

    public static <VO, T, D, M extends BaseMapper<D>> void transOne(VO vo, String keyFieldName, String dFieldName,
                                                                 Class<M> mapper, String voToFieldName){
        BaseMapper<D> mapperInstance = SpringUtils.getBean(mapper);
        QueryWrapper<D> wrapper = Wrappers.query();
        wrapper.select(dFieldName).eq(keyFieldName, ReflectUtil.getFieldValue(vo, keyFieldName)).last("LIMIT 1");
        D d = mapperInstance.selectOne(wrapper);
        if(ObjectUtil.isNotEmpty(d)){
            ReflectUtil.setFieldValue(vo, voToFieldName, ReflectUtil.getFieldValue(d, dFieldName));
        }
    }

}
