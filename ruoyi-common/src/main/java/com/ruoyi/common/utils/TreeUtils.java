package com.ruoyi.common.utils;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.lang.tree.Tree;
import cn.hutool.core.lang.tree.TreeNodeConfig;
import cn.hutool.core.lang.tree.TreeUtil;
import cn.hutool.core.lang.tree.parser.NodeParser;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.ReflectUtil;
import cn.hutool.core.util.StrUtil;
import com.ruoyi.common.utils.reflect.ReflectUtils;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.Field;
import java.util.*;


/**
 * Hutool树工具扩展
 *
 * @author lyx
 * @date 2023-03-31
 **/
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Slf4j
public class TreeUtils extends TreeUtil {

    /**
     * 默认统计结点字段名
     */
    public static final String DEFAULT_COUNT_FLAG = "count";
    /**
     * 默认顶层父id
     */
    public static final Integer DEFAULT_TOP_FLAG = 0;

    /**
     * 树顶层id数组判断 默认有 0和-1
     */
    private static final String[] DEFAULT_TOP_TAG = {"0", "-1"};

    /**
     * 默认父id字段名
     */
    private static final String DEFAULT_PARENT_ID_NAME = "parentId";


    /**
     * 默认子列表字段名
     */
    private static final String DEFAULT_CHILD_NAME = "children";


    /**
     * 将 List 转为树形结,使用默认判断顶层节点(0或-1)
     *
     * @param origList     : 要转换的 List
     * @param idFieldName    : id字段名
     * @param <T>        : 拥有父子结构的 Entity
     * @return : 树形结果
     */
    public static <T> List<T> build(List<T> origList, String idFieldName) {

        return build(origList, idFieldName, DEFAULT_PARENT_ID_NAME, DEFAULT_CHILD_NAME, DEFAULT_TOP_TAG);
    }


    /**
     * 将 List 转为树形结,使用默认判断顶层节点(0或-1)
     *
     * @param origList     : 要转换的 List
     * @param idFieldName    : id字段名
     * @param parentIdFieldName : parentId 字段名
     * @param <T>        : 拥有父子结构的 Entity
     * @return : 树形结果
     */
    public static <T> List<T> build(List<T> origList, String idFieldName, String parentIdFieldName) {

        return build(origList, idFieldName, parentIdFieldName, DEFAULT_CHILD_NAME, DEFAULT_TOP_TAG);
    }

    /**
     * 将 List 转为树形结,使用默认判断顶层节点(0或-1)
     *
     * @param origList     : 要转换的 List
     * @param idFieldName    : id字段名
     * @param parentIdFieldName : parentId 字段名
     * @param childrenFieldName : children 字段名
     * @param <T>        : 拥有父子结构的 Entity
     * @return : 树形结果
     */
    public static <T> List<T> build(List<T> origList, String idFieldName,
                                      String parentIdFieldName, String childrenFieldName) {

        return build(origList, idFieldName, parentIdFieldName, childrenFieldName, DEFAULT_TOP_TAG);
    }



    /**
     * 将 List 转为树形结(自定义顶层节点,支持多顶层节点)
     *
     * @param origList     : 要转换的 List
     * @param idFieldName    : id字段名
     * @param parentIdFieldName : parentId 字段名
     * @param childrenFieldName : children 字段名
     * @param topTags : 顶层节点可能的标识数组
     * @param <T>        : 拥有父子结构的 Entity
     * @return : 树形结果
     */
    public static <T> List<T> build(List<T> origList, String idFieldName,
                                      String parentIdFieldName, String childrenFieldName, String[] topTags) {
        if(CollUtil.isEmpty(origList)){
            return CollUtil.newArrayList();
        }
        // 用于保存当前 id 索引的实体类
        Map<String, T> idMaps = new HashMap<>(origList.size());
        // 暂存区, 用于暂存没有根据父id找到对应实体的记录
        List<T> tempList = new ArrayList<>();
        //
        List<T> result = new ArrayList<>();
        // 遍历处理源列表
        for (T origItem : origList) {
            // 1. 获取 id, parentId, children
            String id = null;
            String parentId = null;

            // 2. 每项存入map
            try {
                id = Objects.toString(getFieldValue(origItem, idFieldName), "");
                parentId = Objects.toString(getFieldValue(origItem, parentIdFieldName), "");
            } catch (Exception e) {
                throw new RuntimeException("树形转换工具出错" + e.getMessage());
            }
            if (StrUtil.isEmpty(id)) {
                throw new RuntimeException("树形转换工具使用出错,存在id为空的资料");
            }
            idMaps.put(id, origItem);

            // 3. 判断层级
            if (StrUtil.isEmpty(parentId) || isTopItem(parentId, topTags)) {
                // 如果满足顶层条件, 则实体类为第一层
                result.add(origItem);
            } else {
                // 根据父 id 获取实体类
                T parentEntity = idMaps.get(parentId);
                if (parentEntity == null) {
                    // 没找到先放入暂存区
                    tempList.add(origItem);
                } else {
                    // 父组件判断是否存在children, 不存在:新增children列表加入当前entity, 存在:直接加入当前entity
                    try {
                        setChildrenValue(childrenFieldName, origItem, parentEntity);
                    } catch (Exception e) {
                        throw new RuntimeException("树形转换工具出错" + e.getMessage());
                    }
                }
            }
        }
        // 处理暂存区, 暂存区的一定不为根节点, 所以它只要父节点存在, 那么此轮查询一定能找到父节点(上一轮已经将全部节点放入 idMaps)
        for (T entity : tempList) {
            // 获取 parentId
            String parentId = null;
            try {
                parentId = Objects.toString(getFieldValue(entity, parentIdFieldName), "");
            } catch (Exception e) {
                throw new RuntimeException("树形转换工具出错" + e.getMessage());
            }
            // 根据父id获取实体类
            T parentEntity = idMaps.get(parentId);
            if (parentEntity == null) {
                //throw new RuntimeException("树形转换工具出错存在孤立的子节点(脏数据):" + parentId);
            } else {
                // 父组件判断是否存在children, 不存在:新增children列表加入当前entity, 存在:直接加入当前entity
                try {
                    setChildrenValue(childrenFieldName, entity, parentEntity);
                } catch (Exception e) {
                    throw new RuntimeException("树形转换工具出错" + e.getMessage());
                }
            }
        }
        return result;

    }

    /**
     * 简易树构建
     *
     * @param list        数据列表
     * @param nodeParser  转换器
     * @return List<Tree<K>>
     * @date 2023/1/17
     **/
    public static <T, K> List<Tree<K>> build(List<T> list, NodeParser<T, K> nodeParser) {
        if (CollUtil.isEmpty(list)) {
            return null;
        }
        K k = ReflectUtils.invokeGetter(list.get(0), "parentId");
        return TreeUtil.build(list, k, new TreeNodeConfig(), nodeParser);
    }


    /**
     * 树构建,并统计子结点数
     * <默认统计标记字段名为{@value  #DEFAULT_COUNT_FLAG}>
     *
     * @param <T>          转换的实体 为数据源里的对象类型
     * @param <E>          ID类型
     * @param list         源数据集合
     * @param parentId     最顶层父id值 一般为 0 之类
     * @param nodeParser   转换器
     * @return List
     */
    public static <T, E> List<Tree<E>> buildWithChildCount(List<T> list, E parentId, NodeParser<T, E> nodeParser) {
        return buildWithChildCount(list, parentId, DEFAULT_COUNT_FLAG, nodeParser);
    }


    /**
     * 树构建,并统计子结点数
     *
     * @param <T>          转换的实体 为数据源里的对象类型
     * @param <E>          ID类型
     * @param list         源数据集合
     * @param parentId     最顶层父id值 一般为 0 之类
     * @param countFlag    指定统计标记字段名
     * @param nodeParser   转换器
     * @return List
     */
    public static <T, E> List<Tree<E>> buildWithChildCount(List<T> list, E parentId, String countFlag,
                                                           NodeParser<T, E> nodeParser) {
        List<Tree<E>> treeList = build(list, parentId, nodeParser);
        doTreeCount(treeList, parentId, countFlag);
        return treeList;
    }

    /**
     * 树构建,并统计指定字段数
     * <默认统计标记字段名为{@value  #DEFAULT_COUNT_FLAG}>
     *
     * @param <T>          转换的实体 为数据源里的对象类型
     * @param <E>          ID类型
     * @param list         源数据集合
     * @param parentId     最顶层父id值 一般为 0 之类
     * @param filedName    需统计的字段名
     * @param nodeParser   转换器
     * @return List
     */
    public static <T, E> List<Tree<E>> buildWithFiledCount(List<T> list, E parentId, String filedName,
                                                           NodeParser<T, E> nodeParser) {
        return buildWithFiledCount(list, parentId, DEFAULT_COUNT_FLAG, filedName, nodeParser);
    }


    /**
     * 树构建,并统计指定字段数
     *
     * @param <T>          转换的实体 为数据源里的对象类型
     * @param <E>          ID类型
     * @param list         源数据集合
     * @param parentId     最顶层父id值 一般为 0 之类
     * @param countFlag    指定统计标记字段名
     * @param filedName    需统计的字段名
     * @param nodeParser   转换器
     * @return List
     */
    public static <T, E> List<Tree<E>> buildWithFiledCount(List<T> list, E parentId, String countFlag, String filedName,
                                                           NodeParser<T, E> nodeParser) {
        List<Tree<E>> treeList = build(list, parentId, nodeParser);
        doTreeFiledCount(treeList, parentId, filedName, countFlag);
        return treeList;
    }



    /**
     * 树指定字段统计
     *
     * @param <T>            转换的实体 为数据源里的对象类型
     * @param <E>            ID类型
     * @param treeList       构建好的树
     * @param parentId       最顶层父id值 一般为 0 之类
     * @param countFlag      指定统计标记字段名
     * @param filedName      需统计的字段名
     * @date 2022/12/7
     **/
    public static <T, E> void doTreeFiledCount(List<Tree<E>> treeList, E parentId, String filedName, String countFlag) {
        if(CollUtil.isNotEmpty(treeList)){
            Iterator<Tree<E>> iterator = treeList.iterator();
            while (iterator.hasNext()){
                Tree<E> tree = iterator.next();
                if(parentId.equals(tree.getParentId())){
                    doSingleFiledCount(tree, filedName, "count");
                }
            }
        }
    }


    /**
     * 树子结点统计
     * <默认顶层结点为{@value  #DEFAULT_TOP_FLAG}>
     * <默认统计标记字段名为{@value  #DEFAULT_COUNT_FLAG}>
     *
     * @param treeList       构建好的树
     * @date 2022/12/7
     **/
    public static <T> void doTreeCount(List<Tree<T>> treeList) {
        doTreeCount(treeList, DEFAULT_TOP_FLAG, DEFAULT_COUNT_FLAG);
    }

    /**
     * 树子结点统计
     *
     * @param <T>            转换的实体 为数据源里的对象类型
     * @param <E>            ID类型
     * @param treeList       构建好的树
     * @param parentId       最顶层父id值 一般为 0 之类
     * @param countFlag      指定统计标记字段名
     * @date 2022/12/7
     **/
    public static <T, E> void doTreeCount(List<Tree<T>> treeList, E parentId, String countFlag) {
        if(CollUtil.isNotEmpty(treeList)){
            Iterator<Tree<T>> iterator = treeList.iterator();
            while (iterator.hasNext()){
                Tree<T> tree = iterator.next();
                if(parentId.equals(tree.getParentId())){
                    doSingleCount(tree, countFlag);
                }
            }
        }
    }


    /**
     * 单root节点树指定字段统计 <默认统计标记字段名为{@value  #DEFAULT_COUNT_FLAG}>
     *
     * @param root        树root结点
     * @param filedName   指定字段名
     * @return int root结点指定字段数量
     * @date 2022/12/7
     **/
    public static <T> int doSingleFiledCount(Tree<T> root, String filedName) {
        return doSingleFiledCount(root, filedName, DEFAULT_COUNT_FLAG);
    }


    /**
     * 单root节点树指定字段统计
     *
     * @param root        树root结点
     * @param filedName   指定字段名
     * @param countFlag   指定统计标记字段名
     * @return int root结点指定字段数量
     * @date 2022/12/7
     **/
    public static <T> int doSingleFiledCount(Tree<T> root, String filedName, String countFlag) {
        int count = 0;
        if(root.containsKey(filedName)){
            Object children = root.getChildren();
            count = count + getFiledCount(root.get(filedName));
            if(ObjectUtil.isNotEmpty(children)){
                List<Tree<T>> childrenRealList = (List<Tree<T>>) children;
                for (Tree<T> child : childrenRealList) {
                    //count = count + getFiledCount(child.get(filedName));
                    //统计子节点的孩子总数
                    int childCount = doSingleFiledCount(child, filedName, countFlag);
                    count += childCount;
                }
            }
        }
        root.putExtra(countFlag, count);
        return count;
    }

    /**
     * 单root节点树子结点统计 <默认统计标记字段名为{@value  #DEFAULT_COUNT_FLAG}>
     *
     * @param root        树root结点
     * @return int root结点子结点数量
     * @date 2022/12/7
     **/
    public static <T> int doSingleCount(Tree<T> root) {
        return doSingleCount(root, DEFAULT_COUNT_FLAG);
    }


    /**
     * 单root节点树子结点统计
     *
     * @param root        树root结点
     * @param countFlag   指定统计标记字段名
     * @return int root结点子结点数量
     * @date 2022/12/7
     **/
    public static <T> int doSingleCount(Tree<T> root, String countFlag) {
        int count = 0;
        Object children = root.getChildren();
        if(ObjectUtil.isNotEmpty(children)){
            List<Tree<T>> childrenRealList = (List<Tree<T>>) children;
            for (Tree<T> child : childrenRealList) {
                //统计当前元素的子节点个数
                count++;
                //统计子节点的孩子总数
                int childCount = doSingleCount(child, countFlag);
                child.putExtra(countFlag, childCount);

                count += childCount;
            }
        }
        root.putExtra(countFlag, count);
        return count;
    }


    /**
     * 获取指定集合字段Object的size
     *
     * @param filedObject 指定字段Object
     * @return int 对象集合size
     * @date 2022/12/7
     **/
    private static int getFiledCount(Object filedObject){
        if(ObjectUtil.isNotEmpty(filedObject) && filedObject instanceof Collection){
            Collection collection = (Collection) filedObject;
            return collection.size();
        }
        return 0;
    }



    /**
     *  判断是否是顶层节点
     */
    private static boolean isTopItem(String currentTag, String[] topTags) {
        for (String str : topTags) {
            if (str.equalsIgnoreCase(currentTag)) {
                return true;
            }
        }
        return false;
    }

    /**
     * 赋值子列表
     */
    private static <T> void setChildrenValue(String childrenFieldName, T entity, T parentEntity) throws Exception {
        // 取出parentEntity中的子列表
        Object children = getFieldValue(parentEntity, childrenFieldName);
        List<T> childrenList= new ArrayList<>();
        if (children == null) {
            childrenList.add(entity);
            setFieldValue(parentEntity, childrenFieldName , childrenList);
        } else {
            List<T> childrenReal = (List<T>) children;
            childrenReal.add(entity);
        }
    }

    /**
     * 利用反射获取字段值
     */
    private static <T> Object getFieldValue(T entity, String fieldName) throws Exception {

        Field field = ReflectUtil.getField(entity.getClass(), fieldName);
        if (ObjectUtil.isNotNull(field)) {
            return ReflectUtil.getFieldValue(entity, field);
        }else {
            throw new Exception(String.format("字段名称[%s]不存在", fieldName));
        }
    }

    /**
     * 利用反射给字段赋值
     */
    private static <T> void setFieldValue(T entity, String fieldName, Object value) {
        Class<?> entityClass = entity.getClass();
        Field field = ReflectUtil.getField(entityClass, fieldName);
        if (ObjectUtil.isNotNull(field)) {
            ReflectUtil.setFieldValue(entity, field, value);
        }else{
            try {
                throw new Exception(String.format("字段名称[%s]不存在", fieldName));
            } catch (Exception e) {
                log.error("entity:{}、fieldName:{}、 value: {} and errorMessage:{}",
                        entity, fieldName, value, e.getMessage(), e);
            }
        }
    }

    public static <T> int doChildCount(T root, String idFieldName,
                                       String childrenFieldName, Map<String,Object> result) throws Exception {
        int count = 0;
        if(ObjectUtil.isEmpty(root)){
            return count;
        }
        Object children = getFieldValue(root, childrenFieldName);
        if(children==null){
            return count;
        }else {
            List<T> childrenRealList = (List<T>) children;
            for (T child : childrenRealList) {
                //统计当前元素的子节点个数
                count++;

                //统计子节点的孩子总数
                int childCount=doChildCount(child, idFieldName, childrenFieldName, result);
                result.put(String.valueOf(Objects.toString(getFieldValue(child, idFieldName), "")), childCount);

                count += childCount;
            }
        }

        //返回前记录当前节点的统计个数
        result.put(String.valueOf(Objects.toString(getFieldValue(root, idFieldName), "")), count);
        return count;
    }
}
