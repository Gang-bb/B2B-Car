package com.ruoyi.business.framework.car.service;

import com.ruoyi.business.app.car.domain.vo.CarSeriesPickListOneVo;
import com.ruoyi.business.framework.car.domain.CarSeries;
import com.ruoyi.business.framework.car.domain.vo.CarSeriesVo;
import com.ruoyi.business.framework.car.domain.bo.CarSeriesBo;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotNull;
import java.util.Collection;
import java.util.List;

/**
 * 汽车车系Service接口
 *
 * @author Gangbb
 * @date 2023-03-25
 */
@Validated
public interface ICarSeriesService {

    /**
     * 查询汽车车系
     */
    CarSeries queryById(Long carSeriesId);


    /**
     * 查询汽车车系列表
     */
    List<CarSeriesVo> queryList(CarSeriesBo bo);

    /**
     * 新增汽车车系
     */
    Boolean insertByBo(CarSeriesBo bo);

    /**
     * 修改汽车车系
     */
    Boolean updateByBo(CarSeriesBo bo);

    /**
     * 校验并批量删除汽车车系信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);

    /**
     * 选择车系页面1 对应查询
     */
    List<CarSeriesPickListOneVo> pickListOne(@NotNull Long carBrandId);
}
