package com.ruoyi.business.framework.business.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;



/**
 * 车源收藏视图对象 b_source_collection
 *
 * @author Gangbb
 * @date 2023-04-15
 */
@Data
@ExcelIgnoreUnannotated
public class SourceCollectionVo {

    private static final long serialVersionUID = 1L;

    /**
     * 车源收藏id
     */
    @ExcelProperty(value = "车源收藏id")
    private Long sourceCollectionId;

    /**
     * 关联用户id;(sys_user.id)
     */
    @ExcelProperty(value = "关联用户id;(sys_user.id)")
    private Long userId;

    /**
     * 关联车源id;(b_car_source.id)
     */
    @ExcelProperty(value = "关联车源id;(b_car_source.id)")
    private Long carSourceId;


}
