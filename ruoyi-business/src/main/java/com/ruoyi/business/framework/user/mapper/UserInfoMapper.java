package com.ruoyi.business.framework.user.mapper;

import com.ruoyi.business.framework.user.domain.UserInfo;
import com.ruoyi.business.framework.user.domain.vo.UserInfoVo;
import com.ruoyi.common.core.mapper.BaseMapperPlus;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * 用户平台信息Mapper接口
 *
 * @author Gangbb
 * @date 2023-03-28
 */
public interface UserInfoMapper extends BaseMapperPlus<UserInfoMapper, UserInfo, UserInfoVo> {

    /**
     * 判断用户是否首登完善信息
     *
     * @param userId 用户id
     * @return int 记录数
     * @date 2023-03-28
     **/
    @Select("SELECT COUNT(1) FROM u_user_info WHERE user_id = #{userId} AND del_flag = '0' LIMIT 1")
    int selectUserHasInfo(@Param("userId") Long userId);
}
