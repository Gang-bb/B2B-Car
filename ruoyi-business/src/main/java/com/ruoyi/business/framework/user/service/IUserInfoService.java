package com.ruoyi.business.framework.user.service;

import com.ruoyi.business.framework.user.domain.UserInfo;
import com.ruoyi.business.framework.user.domain.vo.UserInfoVo;
import com.ruoyi.business.framework.user.domain.bo.UserInfoBo;
import com.ruoyi.common.core.domain.model.UserInfoAppMineVo;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.core.domain.PageQuery;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotNull;
import java.util.Collection;
import java.util.List;

/**
 * 用户平台信息Service接口
 *
 * @author Gangbb
 * @date 2023-03-28
 */
@Validated
public interface IUserInfoService {

    /**
     * 查询用户平台信息
     */
    UserInfo queryById(Long userInfoId);

    /**
     * 查询用户平台信息列表
     */
    TableDataInfo<UserInfoVo> queryPageList(UserInfoBo bo, PageQuery pageQuery);

    /**
     * 查询用户平台信息列表
     */
    List<UserInfoVo> queryList(UserInfoBo bo);

    /**
     * 新增用户平台信息
     */
    Boolean insertByBo(UserInfoBo bo);

    /**
     * 修改用户平台信息
     */
    Boolean updateByBo(UserInfoBo bo);

    /**
     * 校验并批量删除用户平台信息信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);

    /**
     * 获取app我的页面信息
     */
    UserInfoAppMineVo getAppMyInfo(@NotNull Long userId);
}
