package com.ruoyi.business.framework.user.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ruoyi.business.framework.user.domain.UserInfo;
import com.ruoyi.business.framework.user.domain.bo.UserInfoBo;
import com.ruoyi.business.framework.user.domain.vo.UserInfoVo;
import com.ruoyi.business.framework.user.mapper.UserInfoMapper;
import com.ruoyi.business.framework.user.service.IUserInfoService;
import com.ruoyi.common.core.domain.PageQuery;
import com.ruoyi.common.core.domain.model.UserInfoAppMineVo;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.utils.StringUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * 用户平台信息Service业务层处理
 *
 * @author Gangbb
 * @date 2023-03-28
 */
@RequiredArgsConstructor
@Service
public class UserInfoServiceImpl implements IUserInfoService {

    private final UserInfoMapper baseMapper;

    /**
     * 查询用户平台信息
     */
    @Override
    public UserInfo queryById(Long userInfoId){
        return baseMapper.selectById(userInfoId);
    }

    /**
     * 查询用户平台信息列表
     */
    @Override
    public TableDataInfo<UserInfoVo> queryPageList(UserInfoBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<UserInfo> lqw = buildQueryWrapper(bo);
        Page<UserInfoVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询用户平台信息列表
     */
    @Override
    public List<UserInfoVo> queryList(UserInfoBo bo) {
        LambdaQueryWrapper<UserInfo> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<UserInfo> buildQueryWrapper(UserInfoBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<UserInfo> lqw = Wrappers.lambdaQuery();
        lqw.eq(StringUtils.isNotBlank(bo.getLoginPhone()), UserInfo::getLoginPhone, bo.getLoginPhone());
        lqw.eq(bo.getPersonalAuthRes() != null, UserInfo::getPersonalAuthRes, bo.getPersonalAuthRes());
        lqw.like(StringUtils.isNotBlank(bo.getRealName()), UserInfo::getRealName, bo.getRealName());
        lqw.like(StringUtils.isNotBlank(bo.getIdCard()), UserInfo::getIdCard, bo.getIdCard());
        lqw.eq(bo.getCompanyAuthRes() != null, UserInfo::getCompanyAuthRes, bo.getCompanyAuthRes());
        lqw.eq(bo.getCreditAuth() != null, UserInfo::getCreditAuth, bo.getCreditAuth());
        lqw.eq(bo.getUserGradeId() != null, UserInfo::getUserGradeId, bo.getUserGradeId());
        return lqw;
    }

    /**
     * 新增用户平台信息
     */
    @Override
    public Boolean insertByBo(UserInfoBo bo) {
        UserInfo add = BeanUtil.toBean(bo, UserInfo.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setUserInfoId(add.getUserInfoId());
        }
        return flag;
    }

    /**
     * 修改用户平台信息
     */
    @Override
    public Boolean updateByBo(UserInfoBo bo) {
        UserInfo update = BeanUtil.toBean(bo, UserInfo.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(UserInfo entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除用户平台信息
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }

    /**
     * 获取app我的页面信息
     *
     */
    @Override
    public UserInfoAppMineVo getAppMyInfo(Long userId) {
        UserInfo userInfo = baseMapper.selectOne(new LambdaQueryWrapper<UserInfo>()
                .eq(UserInfo::getUserId, userId)
                .last("LIMIT 1"));

        UserInfoAppMineVo appMineVo = BeanUtil.copyProperties(userInfo, UserInfoAppMineVo.class);
        if(ObjectUtil.isNotEmpty(appMineVo) && ObjectUtil.isEmpty(appMineVo.getOperateBrandCount())){
            appMineVo.setOperateBrandCount(0);
        }
        return appMineVo;
    }
}
