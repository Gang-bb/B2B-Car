package com.ruoyi.business.framework.user.domain;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 用户平台信息对象 u_user_info
 *
 * @author Gangbb
 * @date 2023-03-28
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("u_user_info")
public class UserInfo extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     * 用户平台信息id
     */
    @TableId(value = "user_info_id")
    private Long userInfoId;
    /**
     * 关联用户id;(sys_user.id)
     */
    private Long userId;
    /**
     * 账号头像
     */
    private String avatar;
    /**
     * 展示照片url(多个逗号,最多4张)
     */
    private String showImgUrl;
    /**
     * 一句话简介
     */
    private String oneWord;
    /**
     * 登陆手机号
     */
    private String loginPhone;
    /**
     * 备用手机号
     */
    private String reservePhone;
    /**
     * 个人认证是否通过(0-否 1-是)
     */
    private Integer personalAuthRes;
    /**
     * 用户真实姓名
     */
    private String realName;
    /**
     * 身份证号
     */
    private String idCard;
    /**
     * 公司认证是否通过(0-否 1-是)
     */
    private Integer companyAuthRes;
    /**
     * 用户公司名称
     */
    private String companyName;
    /**
     * 所在地区id
     */
    private Long areaId;
    /**
     * 所在地区全称
     */
    private String areaMergeName;
    /**
     * 实际经营地址
     */
    private String operateAddress;
    /**
     * 公司性质(字典：company_property)
     */
    private String companyProperty;
    /**
     * 经营汽车品牌id(多个逗号隔开,最多12个)
     */
    private String operateBrandId;
    /**
     * 经营汽车品牌名称(多个逗号隔开,最多12个)
     */
    private String operateBrandName;
    /**
     * 经营类别(字典：operate_category)
     */
    private Integer operateCategory;
    /**
     * 授信认证是否通过(0-否 1-是)
     */
    private Integer creditAuth;
    /**
     * 授信认证评级id;(u_user_grade.id)
     */
    private Long userGradeId;
    /**
     * 授信有效截止日期
     */
    private Date creditExpiration;
    /**
     * 删除标志（0代表存在 2代表删除）
     */
    @TableLogic
    private String delFlag;

}
