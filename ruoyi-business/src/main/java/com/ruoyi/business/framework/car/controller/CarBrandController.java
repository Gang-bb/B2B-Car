package com.ruoyi.business.framework.car.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.ruoyi.business.framework.car.domain.CarBrand;
import com.ruoyi.business.framework.car.domain.bo.CarBrandBo;
import com.ruoyi.business.framework.car.domain.vo.CarBrandVo;
import com.ruoyi.business.framework.car.service.ICarBrandService;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.annotation.RepeatSubmit;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.PageQuery;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.core.validate.AddGroup;
import com.ruoyi.common.core.validate.EditGroup;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Arrays;
import java.util.List;

/**
 * 汽车品牌
 *
 * @author Gangbb
 * @date 2023-03-25
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/car/brand")
public class CarBrandController extends BaseController {

    private final ICarBrandService iCarBrandService;

    /**
     * 分页查询汽车品牌列表
     */
    @SaCheckPermission("car:brand:list")
    @GetMapping("/list")
    public TableDataInfo<CarBrandVo> list(CarBrandBo bo, PageQuery pageQuery) {
        return iCarBrandService.queryPageList(bo, pageQuery);
    }

    /**
     * 查询汽车品牌列表
     */
    @SaCheckPermission("car:brand:list")
    @GetMapping("/allList")
    public R<List<CarBrandVo>> allList(CarBrandBo bo) {
        return R.ok(iCarBrandService.queryList(bo));
    }

    /**
     * 导出汽车品牌列表
     */
    @SaCheckPermission("car:brand:export")
    @Log(title = "汽车品牌", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(CarBrandBo bo, HttpServletResponse response) {
        List<CarBrandVo> list = iCarBrandService.queryList(bo);
        ExcelUtil.exportExcel(list, "汽车品牌", CarBrandVo.class, response);
    }

    /**
     * 获取汽车品牌详细信息
     *
     * @param carBrandId 主键
     */
    @SaCheckPermission("car:brand:query")
    @GetMapping("/{carBrandId}")
    public R<CarBrand> getInfo(@NotNull(message = "主键不能为空")
                               @PathVariable Long carBrandId) {
        return R.ok(iCarBrandService.queryById(carBrandId));
    }

    /**
     * 新增汽车品牌
     */
    @SaCheckPermission("car:brand:add")
    @Log(title = "汽车品牌", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody CarBrandBo bo) {
        return toAjax(iCarBrandService.insertByBo(bo));
    }

    /**
     * 修改汽车品牌
     */
    @SaCheckPermission("car:brand:edit")
    @Log(title = "汽车品牌", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody CarBrandBo bo) {
        return toAjax(iCarBrandService.updateByBo(bo));
    }

    /**
     * 删除汽车品牌
     *
     * @param carBrandIds 主键串
     */
    @SaCheckPermission("car:brand:remove")
    @Log(title = "汽车品牌", businessType = BusinessType.DELETE)
    @DeleteMapping("/{carBrandIds}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] carBrandIds) {
        return toAjax(iCarBrandService.deleteWithValidByIds(Arrays.asList(carBrandIds), true));
    }
}
