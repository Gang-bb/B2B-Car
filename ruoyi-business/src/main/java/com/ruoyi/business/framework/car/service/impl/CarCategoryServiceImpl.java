package com.ruoyi.business.framework.car.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.core.domain.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import com.ruoyi.business.framework.car.domain.bo.CarCategoryBo;
import com.ruoyi.business.framework.car.domain.vo.CarCategoryVo;
import com.ruoyi.business.framework.car.domain.CarCategory;
import com.ruoyi.business.framework.car.mapper.CarCategoryMapper;
import com.ruoyi.business.framework.car.service.ICarCategoryService;

import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 汽车类型Service业务层处理
 *
 * @author Gangbb
 * @date 2023-04-02
 */
@RequiredArgsConstructor
@Service
public class CarCategoryServiceImpl implements ICarCategoryService {

    private final CarCategoryMapper baseMapper;

    /**
     * 查询汽车类型
     */
    @Override
    public CarCategory queryById(Long carCategoryId){
        return baseMapper.selectById(carCategoryId);
    }

    /**
     * 查询汽车类型列表
     */
    @Override
    public TableDataInfo<CarCategoryVo> queryPageList(CarCategoryBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<CarCategory> lqw = buildQueryWrapper(bo);
        Page<CarCategoryVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询汽车类型列表
     */
    @Override
    public List<CarCategoryVo> queryList(CarCategoryBo bo) {
        LambdaQueryWrapper<CarCategory> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<CarCategory> buildQueryWrapper(CarCategoryBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<CarCategory> lqw = Wrappers.lambdaQuery();
        lqw.like(StringUtils.isNotBlank(bo.getCarCategoryName()), CarCategory::getCarCategoryName, bo.getCarCategoryName());
        lqw.eq((StringUtils.isNotBlank(bo.getOperateCategory()) && !"1".equals(bo.getOperateCategory())),
                CarCategory::getOperateCategory, bo.getOperateCategory());
        lqw.orderByAsc(CarCategory::getSort);
        return lqw;
    }

    /**
     * 新增汽车类型
     */
    @Override
    public Boolean insertByBo(CarCategoryBo bo) {
        CarCategory add = BeanUtil.toBean(bo, CarCategory.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setCarCategoryId(add.getCarCategoryId());
        }
        return flag;
    }

    /**
     * 修改汽车类型
     */
    @Override
    public Boolean updateByBo(CarCategoryBo bo) {
        CarCategory update = BeanUtil.toBean(bo, CarCategory.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(CarCategory entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除汽车类型
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }
}
