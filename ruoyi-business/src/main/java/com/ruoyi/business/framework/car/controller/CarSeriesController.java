package com.ruoyi.business.framework.car.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.ruoyi.business.framework.car.domain.CarSeries;
import com.ruoyi.business.framework.car.domain.bo.CarSeriesBo;
import com.ruoyi.business.framework.car.domain.vo.CarSeriesVo;
import com.ruoyi.business.framework.car.service.ICarSeriesService;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.annotation.RepeatSubmit;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.core.validate.AddGroup;
import com.ruoyi.common.core.validate.EditGroup;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Arrays;
import java.util.List;

/**
 * 汽车车系
 *
 * @author Gangbb
 * @date 2023-03-25
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/car/series")
public class CarSeriesController extends BaseController {

    private final ICarSeriesService iCarSeriesService;

    /**
     * 查询汽车车系列表
     */
    @SaCheckPermission("car:series:list")
    @GetMapping("/list")
    public R<List<CarSeriesVo>> list(CarSeriesBo bo) {
        List<CarSeriesVo> list = iCarSeriesService.queryList(bo);
        return R.ok(list);
    }

    /**
     * 导出汽车车系列表
     */
    @SaCheckPermission("car:series:export")
    @Log(title = "汽车车系", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(CarSeriesBo bo, HttpServletResponse response) {
        List<CarSeriesVo> list = iCarSeriesService.queryList(bo);
        ExcelUtil.exportExcel(list, "汽车车系", CarSeriesVo.class, response);
    }

    /**
     * 获取汽车车系详细信息
     *
     * @param carSeriesId 主键
     */
    @SaCheckPermission("car:series:query")
    @GetMapping("/{carSeriesId}")
    public R<CarSeries> getInfo(@NotNull(message = "主键不能为空")
                                @PathVariable Long carSeriesId) {
        return R.ok(iCarSeriesService.queryById(carSeriesId));
    }

    /**
     * 新增汽车车系
     */
    @SaCheckPermission("car:series:add")
    @Log(title = "汽车车系", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody CarSeriesBo bo) {
        return toAjax(iCarSeriesService.insertByBo(bo));
    }

    /**
     * 修改汽车车系
     */
    @SaCheckPermission("car:series:edit")
    @Log(title = "汽车车系", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody CarSeriesBo bo) {
        return toAjax(iCarSeriesService.updateByBo(bo));
    }

    /**
     * 删除汽车车系
     *
     * @param carSeriesIds 主键串
     */
    @SaCheckPermission("car:series:remove")
    @Log(title = "汽车车系", businessType = BusinessType.DELETE)
    @DeleteMapping("/{carSeriesIds}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] carSeriesIds) {
        return toAjax(iCarSeriesService.deleteWithValidByIds(Arrays.asList(carSeriesIds), true));
    }
}
