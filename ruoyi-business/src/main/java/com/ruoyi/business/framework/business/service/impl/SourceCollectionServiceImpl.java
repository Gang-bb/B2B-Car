package com.ruoyi.business.framework.business.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ruoyi.business.app.business.domain.bo.SourceCollectionAppBo;
import com.ruoyi.business.app.business.domain.vo.SourceCollectionAppVo;
import com.ruoyi.business.app.car.domain.bo.CarSourceAppBo;
import com.ruoyi.business.app.car.domain.vo.CarSourceAppVo;
import com.ruoyi.business.framework.business.domain.SourceCollection;
import com.ruoyi.business.framework.business.domain.bo.SourceCollectionBo;
import com.ruoyi.business.framework.business.domain.vo.SourceCollectionVo;
import com.ruoyi.business.framework.business.mapper.SourceCollectionMapper;
import com.ruoyi.business.framework.business.service.ISourceCollectionService;
import com.ruoyi.business.framework.car.service.ICarSourceService;
import com.ruoyi.common.core.domain.PageQuery;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.helper.LoginHelper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * 车源收藏Service业务层处理
 *
 * @author Gangbb
 * @date 2023-04-15
 */
@RequiredArgsConstructor
@Service
public class SourceCollectionServiceImpl implements ISourceCollectionService {

    private final SourceCollectionMapper baseMapper;
    private final ICarSourceService iCarSourceService;

    /**
     * 查询车源收藏
     */
    @Override
    public SourceCollection queryById(Long sourceCollectionId){
        return baseMapper.selectById(sourceCollectionId);
    }

    /**
     * 查询车源收藏列表
     */
    @Override
    public TableDataInfo<SourceCollectionVo> queryPageList(SourceCollectionBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<SourceCollection> lqw = buildQueryWrapper(bo);
        Page<SourceCollectionVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询车源收藏列表
     */
    @Override
    public List<SourceCollectionVo> queryList(SourceCollectionBo bo) {
        LambdaQueryWrapper<SourceCollection> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<SourceCollection> buildQueryWrapper(SourceCollectionBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<SourceCollection> lqw = Wrappers.lambdaQuery();
        lqw.eq(bo.getUserId() != null, SourceCollection::getUserId, bo.getUserId());
        lqw.eq(bo.getCarSourceId() != null, SourceCollection::getCarSourceId, bo.getCarSourceId());
        lqw.orderByDesc(SourceCollection::getUpdateTime);
        return lqw;
    }

    /**
     * 新增车源收藏
     */
    @Override
    public Boolean insertByBo(SourceCollectionBo bo) {
        SourceCollection add = BeanUtil.toBean(bo, SourceCollection.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setSourceCollectionId(add.getSourceCollectionId());
        }
        return flag;
    }

    /**
     * 修改车源收藏
     */
    @Override
    public Boolean updateByBo(SourceCollectionBo bo) {
        SourceCollection update = BeanUtil.toBean(bo, SourceCollection.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(SourceCollection entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除车源收藏
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }

    /**
     * app 查询车源收藏列表
     */
    @Override
    public TableDataInfo<SourceCollectionAppVo> appList(PageQuery pageQuery) {
        // 根据当前登录用户id 获取车源收藏列表
        Long userId = LoginHelper.getUserId();
        SourceCollectionBo sourceCollectionBo = new SourceCollectionBo();
        sourceCollectionBo.setUserId(userId);
        List<SourceCollectionVo> sourceCollectionVoList = queryList(sourceCollectionBo);
//        TableDataInfo<SourceCollectionVo> scTableDataInfo = queryPageList(sourceCollectionBo, pageQuery);
//        List<SourceCollectionVo> sourceCollectionVoList = scTableDataInfo.getRows();

        // 获取车源id 集合，集合数据量可能会过大，导致 select in() sql 报错
        List<Long> onlyNeedIdList = new ArrayList<>();
        sourceCollectionVoList.forEach((item) -> {
            onlyNeedIdList.add(item.getCarSourceId());
        });

        // 根据车源id查询车源
        CarSourceAppBo carSourceAppBo = new CarSourceAppBo();
        carSourceAppBo.setOnlyNeedIdList(onlyNeedIdList);
        TableDataInfo<CarSourceAppVo> csTableDataInfo = iCarSourceService.appList(carSourceAppBo, pageQuery);

        // 加上车源收藏id
        List<SourceCollectionAppVo> appVoList = new ArrayList<>();
        for (CarSourceAppVo vo : csTableDataInfo.getRows()) {
            sourceCollectionVoList.forEach((item) -> {
                if (vo.getCarSourceId() == item.getCarSourceId()) {
                    SourceCollectionAppVo appVo = BeanUtil.toBean(vo, SourceCollectionAppVo.class);
                    appVo.setSourceCollectionId(item.getSourceCollectionId());
                    appVoList.add(appVo);
                }
            });
        }

        // TODO 排序还有点问题，从 iCarSourceService.appList() 查询回来的数据 和
        //  从 车源收藏表查询回来的数据排序不一样

        return TableDataInfo.build(appVoList);
    }

    /**
     * app 根据车源id查询车源收藏信息
     */
    @Override
    public Long queryByCarSourceId(Long carSourceId) {
        Long userId = LoginHelper.getUserId();
        LambdaQueryWrapper<SourceCollection> lqw = Wrappers.lambdaQuery();
        lqw.eq(userId != null, SourceCollection::getUserId, userId);
        lqw.eq(carSourceId != null, SourceCollection::getCarSourceId, carSourceId);
        SourceCollection sourceCollection = baseMapper.selectOne(lqw);
        if (sourceCollection != null) {
            return sourceCollection.getSourceCollectionId();
        }
        return null;
    }

    /**
     * app 新增车源收藏
     */
    @Override
    public Boolean insertBycCarSourceId(SourceCollectionAppBo bo) {

        Long userId = LoginHelper.getUserId();
        SourceCollectionBo sourceCollectionBo = new SourceCollectionBo();
        sourceCollectionBo.setCarSourceId(bo.getCarSourceId());
        sourceCollectionBo.setUserId(userId);
        return insertByBo(sourceCollectionBo);
    }

    /**
     *app 根据车源id删除车源收藏信息
     */
    @Override
    public Boolean deleteByCarSourceId(Long carSourceId) {
        Long userId = LoginHelper.getUserId();
        LambdaQueryWrapper<SourceCollection> lqw = Wrappers.lambdaQuery();
        lqw.eq(userId != null, SourceCollection::getUserId, userId);
        lqw.eq(carSourceId != null, SourceCollection::getCarSourceId, carSourceId);

        return baseMapper.delete(lqw) > 0;
    }

    /**
     * app 根据用户id删除所有相关车源收藏信息
     */
    @Override
    public Boolean deleteAllByUserId() {
        Long userId = LoginHelper.getUserId();
        LambdaQueryWrapper<SourceCollection> lqw = Wrappers.lambdaQuery();
        lqw.eq(userId != null, SourceCollection::getUserId, userId);

        Long count = baseMapper.selectCount(lqw);
        if (count == 0) {
            return true;
        }
        return baseMapper.delete(lqw) > 0;
    }
}
