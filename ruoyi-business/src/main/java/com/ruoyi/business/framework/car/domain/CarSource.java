package com.ruoyi.business.framework.car.domain;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 车源对象 car_source
 *
 * @author Gangbb
 * @date 2023-04-16
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("car_source")
public class CarSource extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     * 车源id
     */
    @TableId(value = "car_source_id")
    private Long carSourceId;
    /**
     * 关联汽车类型id;(car_category.id)
     */
    private Long carCategoryId;
    /**
     * 关联汽车品牌id;(car_brand.id)
     */
    private Long carBrandId;
    /**
     * 关联汽车车系id;(car_series.id)
     */
    private Long carSeriesId;
    /**
     * 关联汽车型号id;(car_model.id)
     */
    private Long carModelId;
    /**
     * 关联用户id;(sys_user.id)
     */
    private Long userId;
    /**
     * 车源信息(汽车型号名 +指导价)
     */
    private String carSourceInfo;
    /**
     * 外观颜色
     */
    private String appearanceColor;
    /**
     * 内饰颜色
     */
    private String interiorColor;
    /**
     * 报价类型(字典：quote_type)
     */
    private String quoteType;
    /**
     * 报价单位
     */
    private String quoteUnit;
    /**
     * 报价数额
     */
    private BigDecimal quoteNumber;
    /**
     * 最终价格
     */
    private BigDecimal finalPrice;
    /**
     * 车源有效期(天数)
     */
    private Integer sourceTermOfValidity;
    /**
     * 车源过期时间
     */
    private Date sourceExpirationTime;
    /**
     * 车源手续(字典：source_procedures)
     */
    private String sourceProcedures;
    /**
     * 车源所在地
     */
    private String sourceLocation;
    /**
     * 销售区域
     */
    private String salesArea;
    /**
     * 车源地区id(没填填发起人地区)
     */
    private Long sourceAreaId;
    /**
     * 车源地区全称
     */
    private String sourceAreaMergeName;
    /**
     * 排放标准
     */
    private String sourceDischarge;
    /**
     * 车源性质(字典：source_nature)
     */
    private String sourceNature;
    /**
     * 车源货期(字典：source_delivery)
     */
    private String sourceDelivery;
    /**
     * 车架号
     */
    private String vehicleFrame;
    /**
     * 其他配置
     */
    private String otherFeature;
    /**
     * 车源图片id(多个逗号隔开)
     */
    private String sourceImg;
    /**
     * 是否自营车源(0-否 1-是)
     */
    private Integer isSelfSupport;
    /**
     * 删除标志（0代表存在 2代表删除）
     */
    @TableLogic
    private String delFlag;

}
