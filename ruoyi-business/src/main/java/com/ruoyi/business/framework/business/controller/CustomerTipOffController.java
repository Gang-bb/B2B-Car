package com.ruoyi.business.framework.business.controller;

import java.util.List;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import lombok.RequiredArgsConstructor;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.*;
import cn.dev33.satoken.annotation.SaCheckPermission;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import com.ruoyi.common.annotation.RepeatSubmit;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.PageQuery;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.core.validate.AddGroup;
import com.ruoyi.common.core.validate.EditGroup;
import com.ruoyi.common.core.validate.QueryGroup;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.business.framework.business.domain.CustomerTipOff;
import com.ruoyi.business.framework.business.domain.vo.CustomerTipOffVo;
import com.ruoyi.business.framework.business.domain.bo.CustomerTipOffBo;
import com.ruoyi.business.framework.business.service.ICustomerTipOffService;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 客户举报
 *
 * @author FYZ
 * @date 2023-04-16
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/business/customerTipOff")
public class CustomerTipOffController extends BaseController {

    private final ICustomerTipOffService iCustomerTipOffService;

    /**
     * 查询客户举报列表
     */
    @SaCheckPermission("business:customerTipOff:list")
    @GetMapping("/list")
    public TableDataInfo<CustomerTipOffVo> list(CustomerTipOffBo bo, PageQuery pageQuery) {
        return iCustomerTipOffService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出客户举报列表
     */
    @SaCheckPermission("business:customerTipOff:export")
    @Log(title = "客户举报", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(CustomerTipOffBo bo, HttpServletResponse response) {
        List<CustomerTipOffVo> list = iCustomerTipOffService.queryList(bo);
        ExcelUtil.exportExcel(list, "客户举报", CustomerTipOffVo.class, response);
    }

    /**
     * 获取客户举报详细信息
     *
     * @param customerTipOffId 主键
     */
    @SaCheckPermission("business:customerTipOff:query")
    @GetMapping("/{customerTipOffId}")
    public R<CustomerTipOff> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long customerTipOffId) {
        return R.ok(iCustomerTipOffService.queryById(customerTipOffId));
    }

    /**
     * 新增客户举报
     */
    @SaCheckPermission("business:customerTipOff:add")
    @Log(title = "客户举报", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody CustomerTipOffBo bo) {
        return toAjax(iCustomerTipOffService.insertByBo(bo));
    }

    /**
     * 修改客户举报
     */
    @SaCheckPermission("business:customerTipOff:edit")
    @Log(title = "客户举报", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody CustomerTipOffBo bo) {
        return toAjax(iCustomerTipOffService.updateByBo(bo));
    }

    /**
     * 删除客户举报
     *
     * @param customerTipOffIds 主键串
     */
    @SaCheckPermission("business:customerTipOff:remove")
    @Log(title = "客户举报", businessType = BusinessType.DELETE)
    @DeleteMapping("/{customerTipOffIds}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] customerTipOffIds) {
        return toAjax(iCustomerTipOffService.deleteWithValidByIds(Arrays.asList(customerTipOffIds), true));
    }
}
