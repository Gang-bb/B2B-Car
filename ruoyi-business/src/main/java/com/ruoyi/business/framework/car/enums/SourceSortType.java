package com.ruoyi.business.framework.car.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * app车源列表pai
 *
 * @author Gangbb
 * @date 2023-04-16
 **/
@Getter
@AllArgsConstructor
public enum SourceSortType {



    /**
     * 价格从低到高
     */
    PRICE_ASC("3"),
    /**
     * 价格从高到低
     */
    PRICE_DESC("2");

    private final String code;
}
