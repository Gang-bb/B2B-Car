package com.ruoyi.business.framework.business.domain.bo;

import com.ruoyi.common.core.validate.AddGroup;
import com.ruoyi.common.core.validate.EditGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;
import javax.validation.constraints.NotNull;
import java.util.Date;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 车源浏览足迹业务对象 b_car_history
 *
 * @author FYZ
 * @date 2023-04-16
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class CarHistoryBo extends BaseEntity {

    /**
     * 车源浏览足迹id
     */
    @NotNull(message = "车源浏览足迹id不能为空", groups = { EditGroup.class })
    private Long carHistoryId;

    /**
     * 关联用户id;(sys_user.id)
     */
    @NotNull(message = "关联用户id;(sys_user.id)不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long userId;

    /**
     * 关联车源id;(car_source.id)
     */
    @NotNull(message = "关联车源id;(car_source.id)不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long carSourceId;

    /**
     * 浏览时间
     */
    @NotNull(message = "浏览时间不能为空", groups = { AddGroup.class, EditGroup.class })
    private Date readTime;


}
