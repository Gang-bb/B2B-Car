package com.ruoyi.business.framework.business.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.Date;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 车源浏览足迹对象 b_car_history
 *
 * @author FYZ
 * @date 2023-04-16
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("b_car_history")
public class CarHistory extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     * 车源浏览足迹id
     */
    @TableId(value = "car_history_id")
    private Long carHistoryId;
    /**
     * 关联用户id;(sys_user.id)
     */
    private Long userId;
    /**
     * 关联车源id;(car_source.id)
     */
    private Long carSourceId;
    /**
     * 浏览时间
     */
    private Date readTime;
    /**
     * 删除标志（0代表存在 2代表删除）
     */
    @TableLogic
    private String delFlag;

}
