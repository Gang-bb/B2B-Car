package com.ruoyi.business.framework.business.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.ruoyi.business.app.business.domain.bo.CarHistoryAppBo;
import com.ruoyi.business.app.business.domain.vo.CarHistoryAppVo;
import com.ruoyi.business.app.car.domain.bo.CarSourceAppBo;
import com.ruoyi.business.app.car.domain.vo.CarSourceAppVo;
import com.ruoyi.business.framework.car.service.ICarSourceService;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.core.domain.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.ruoyi.common.helper.LoginHelper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import com.ruoyi.business.framework.business.domain.bo.CarHistoryBo;
import com.ruoyi.business.framework.business.domain.vo.CarHistoryVo;
import com.ruoyi.business.framework.business.domain.CarHistory;
import com.ruoyi.business.framework.business.mapper.CarHistoryMapper;
import com.ruoyi.business.framework.business.service.ICarHistoryService;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 车源浏览足迹Service业务层处理
 *
 * @author FYZ
 * @date 2023-04-16
 */
@RequiredArgsConstructor
@Service
public class CarHistoryServiceImpl implements ICarHistoryService {

    private final CarHistoryMapper baseMapper;
    private final ICarSourceService iCarSourceService;

    /**
     * 查询车源浏览足迹
     */
    @Override
    public CarHistory queryById(Long carHistoryId){
        return baseMapper.selectById(carHistoryId);
    }

    /**
     * 查询车源浏览足迹列表
     */
    @Override
    public TableDataInfo<CarHistoryVo> queryPageList(CarHistoryBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<CarHistory> lqw = buildQueryWrapper(bo);
        Page<CarHistoryVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询车源浏览足迹列表
     */
    @Override
    public List<CarHistoryVo> queryList(CarHistoryBo bo) {
        LambdaQueryWrapper<CarHistory> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<CarHistory> buildQueryWrapper(CarHistoryBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<CarHistory> lqw = Wrappers.lambdaQuery();
        lqw.eq(bo.getUserId() != null, CarHistory::getUserId, bo.getUserId());
        lqw.eq(bo.getCarSourceId() != null, CarHistory::getCarSourceId, bo.getCarSourceId());
        lqw.orderByDesc(CarHistory::getUpdateTime);
        return lqw;
    }

    /**
     * 新增车源浏览足迹
     */
    @Override
    public Boolean insertByBo(CarHistoryBo bo) {
        CarHistory add = BeanUtil.toBean(bo, CarHistory.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setCarHistoryId(add.getCarHistoryId());
        }
        return flag;
    }

    /**
     * 新增或更新车源浏览足迹
     */
    public Boolean insertOrUpdate(CarHistoryBo bo) {
        CarHistory add = BeanUtil.toBean(bo, CarHistory.class);
        LambdaQueryWrapper<CarHistory> lqw = Wrappers.lambdaQuery();
        lqw.eq(bo.getUserId() != null, CarHistory::getUserId, bo.getUserId());
        lqw.eq(bo.getCarSourceId() != null, CarHistory::getCarSourceId, bo.getCarSourceId());
        CarHistoryVo carHistoryVo = baseMapper.selectVoOne(lqw);

        boolean flag = false;
        if (carHistoryVo != null) {
            CarHistory update = BeanUtil.toBean(carHistoryVo, CarHistory.class);
            flag = baseMapper.updateById(update) > 0;
        } else {
            flag = baseMapper.insert(add) > 0;
        }
        return flag;
    }

    /**
     * 修改车源浏览足迹
     */
    @Override
    public Boolean updateByBo(CarHistoryBo bo) {
        CarHistory update = BeanUtil.toBean(bo, CarHistory.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(CarHistory entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除车源浏览足迹
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }

    /**
     * app 查询车源浏览足迹列表
     */
    @Override
    public TableDataInfo<CarHistoryAppVo> appList(PageQuery pageQuery) {
        // 根据当前登录用户id 获取车源收藏列表
        Long userId = LoginHelper.getUserId();
        CarHistoryBo carHistoryBo = new CarHistoryBo();
        carHistoryBo.setUserId(userId);
        List<CarHistoryVo> historyVoList = queryList(carHistoryBo);

        // 获取车源id 集合，集合数据量可能会过大，导致 select in() sql 报错
        List<Long> onlyNeedIdList = new ArrayList<>();
        historyVoList.forEach((item) -> {
            onlyNeedIdList.add(item.getCarSourceId());
        });

        // 根据车源id查询车源
        CarSourceAppBo carSourceAppBo = new CarSourceAppBo();
        carSourceAppBo.setOnlyNeedIdList(onlyNeedIdList);
        TableDataInfo<CarSourceAppVo> csTableDataInfo = iCarSourceService.appList(carSourceAppBo, pageQuery);

        // 加上车源浏览足迹id
        List<CarHistoryAppVo> appVoList = new ArrayList<>();
        for (CarSourceAppVo vo : csTableDataInfo.getRows()) {
            historyVoList.forEach((item) -> {
                if (vo.getCarSourceId() == item.getCarSourceId()) {
                    CarHistoryAppVo appVo = BeanUtil.toBean(vo, CarHistoryAppVo.class);
                    appVo.setCarHistoryId(item.getCarHistoryId());
                    appVoList.add(appVo);
                }
            });
        }

        // TODO 排序还有点问题，从 iCarSourceService.appList() 查询回来的数据 和
        //  从 车源收藏表查询回来的数据排序不一样

        return TableDataInfo.build(appVoList);
    }

    /**
     * app 新增车源浏览足迹
     */
    @Override
    public Boolean insertBycCarSourceId(CarHistoryAppBo bo) {
        Long userId = LoginHelper.getUserId();
        CarHistoryBo carHistoryBo = new CarHistoryBo();
        carHistoryBo.setCarSourceId(bo.getCarSourceId());
        carHistoryBo.setUserId(userId);
        carHistoryBo.setReadTime(new Date());

        return insertOrUpdate(carHistoryBo);
    }

    /**
     * app 根据车源id删除车源浏览足迹信息
     */
    @Override
    public Boolean deleteByCarSourceId(Long carSourceId) {
        Long userId = LoginHelper.getUserId();
        LambdaQueryWrapper<CarHistory> lqw = Wrappers.lambdaQuery();
        lqw.eq(userId != null, CarHistory::getUserId, userId);
        lqw.eq(carSourceId != null, CarHistory::getCarSourceId, carSourceId);

        return baseMapper.delete(lqw) > 0;
    }

    /**
     * app 根据用户id删除所有相关车源浏览足迹信息
     */
    @Override
    public Boolean deleteAllByUserId() {
        Long userId = LoginHelper.getUserId();
        LambdaQueryWrapper<CarHistory> lqw = Wrappers.lambdaQuery();
        lqw.eq(userId != null, CarHistory::getUserId, userId);

        Long count = baseMapper.selectCount(lqw);
        if (count == 0) {
            return true;
        }
        return baseMapper.delete(lqw) > 0;
    }
}
