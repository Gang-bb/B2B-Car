package com.ruoyi.business.framework.car.domain.bo;

import com.ruoyi.common.core.domain.BaseEntity;
import com.ruoyi.common.core.validate.AddGroup;
import com.ruoyi.common.core.validate.EditGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * 车源业务对象 car_source
 *
 * @author Gangbb
 * @date 2023-04-04
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class CarSourceBo extends BaseEntity {

    /**
     * 车源id
     */
    @NotNull(message = "车源id不能为空", groups = { EditGroup.class })
    private Long carSourceId;

    /**
     * 关联汽车类型id;(car_category.id)
     */
    @NotNull(message = "关联汽车类型id;(car_category.id)不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long carCategoryId;

    /**
     * 关联汽车品牌id;(car_brand.id)
     */
    @NotNull(message = "关联汽车品牌id;(car_brand.id)不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long carBrandId;

    /**
     * 关联汽车车系id;(car_series.id)
     */
    @NotNull(message = "关联汽车车系id;(car_series.id)不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long carSeriesId;

    /**
     * 关联汽车型号id;(car_model.id)
     */
    @NotNull(message = "关联汽车型号id;(car_model.id)不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long carModelId;

    /**
     * 车源信息(汽车型号名 +指导价)
     */
    @NotBlank(message = "车源信息(汽车型号名 +指导价)不能为空", groups = { AddGroup.class, EditGroup.class })
    private String carSourceInfo;

    /**
     * 外观颜色
     */
    @NotBlank(message = "外观颜色不能为空", groups = { AddGroup.class, EditGroup.class })
    private String appearanceColor;

    /**
     * 内饰颜色
     */
    @NotBlank(message = "内饰颜色不能为空", groups = { AddGroup.class, EditGroup.class })
    private String interiorColor;

    /**
     * 报价类型(字典：quote_type)
     */
    @NotBlank(message = "报价类型(字典：quote_type)不能为空", groups = { AddGroup.class, EditGroup.class })
    private String quoteType;

    /**
     * 报价单位
     */
    @NotBlank(message = "报价单位不能为空", groups = { AddGroup.class, EditGroup.class })
    private String quoteUnit;

    /**
     * 报价数额
     */
    @NotNull(message = "报价数额不能为空", groups = { AddGroup.class, EditGroup.class })
    private BigDecimal quoteNumber;

    /**
     * 最终价格
     */
    @NotNull(message = "最终价格不能为空", groups = { AddGroup.class, EditGroup.class })
    private BigDecimal finalPrice;

    /**
     * 车源有效期(天数)
     */
    @NotNull(message = "车源有效期(天数)不能为空", groups = { AddGroup.class, EditGroup.class })
    private Integer sourceTermOfValidity;

    /**
     * 车源手续(字典：source_procedures)
     */
    private String sourceProcedures;

    /**
     * 车源所在地
     */
    private String sourceLocation;

    /**
     * 销售区域
     */
    private String salesArea;

    /**
     * 排放标准
     */
    private String sourceDischarge;

    /**
     * 车架号
     */
    private String vehicleFrame;

    /**
     * 其他配置
     */
    private String otherFeature;

    /**
     * 车源图片id(多个逗号隔开)
     */
    private String sourceImg;

    /**
     * 是否自营车源(0-否 1-是)
     */
    private Integer isSelfSupport;


}
