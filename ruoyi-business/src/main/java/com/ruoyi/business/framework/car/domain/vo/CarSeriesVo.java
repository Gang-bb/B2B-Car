package com.ruoyi.business.framework.car.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.ruoyi.common.annotation.ExcelDictFormat;
import com.ruoyi.common.convert.ExcelDictConvert;
import lombok.Data;



/**
 * 汽车车系视图对象 car_series
 *
 * @author Gangbb
 * @date 2023-04-04
 */
@Data
@ExcelIgnoreUnannotated
public class CarSeriesVo {

    private static final long serialVersionUID = 1L;

    /**
     * 汽车车系id
     */
    @ExcelProperty(value = "汽车车系id")
    private Long carSeriesId;

    /**
     * 父id;(顶层为0)
     */
    @ExcelProperty(value = "父id;(顶层为0)")
    private Long parentId;

    /**
     * 关联汽车类型id(car_category.id)
     */
    private Long carCategoryId;

    /**
     * 汽车类型
     */
    @ExcelProperty(value = "汽车类型")
    private String carCategoryName;

    /**
     * 关联汽车品牌id;(car_brand.id)
     */
    @ExcelProperty(value = "关联汽车品牌id;(car_brand.id)")
    private Long carBrandId;

    /**
     * 汽车品牌
     */
    @ExcelProperty(value = "汽车品牌")
    private String carBrandName;

    /**
     * 汽车车系名称
     */
    @ExcelProperty(value = "汽车车系名称")
    private String carSeriesName;

    /**
     * 排序字段
     */
    @ExcelProperty(value = "排序字段")
    private Integer sort;

    /**
     * 状态(0-正常 2-停用)
     */
    @ExcelProperty(value = "状态(0-正常 2-停用)", converter = ExcelDictConvert.class)
    @ExcelDictFormat(dictType = "status")
    private String status;


}
