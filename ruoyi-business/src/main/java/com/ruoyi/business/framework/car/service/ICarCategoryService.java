package com.ruoyi.business.framework.car.service;

import com.ruoyi.business.framework.car.domain.CarCategory;
import com.ruoyi.business.framework.car.domain.vo.CarCategoryVo;
import com.ruoyi.business.framework.car.domain.bo.CarCategoryBo;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.core.domain.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 汽车类型Service接口
 *
 * @author Gangbb
 * @date 2023-04-02
 */
public interface ICarCategoryService {

    /**
     * 查询汽车类型
     */
    CarCategory queryById(Long carCategoryId);

    /**
     * 查询汽车类型列表
     */
    TableDataInfo<CarCategoryVo> queryPageList(CarCategoryBo bo, PageQuery pageQuery);

    /**
     * 查询汽车类型列表
     */
    List<CarCategoryVo> queryList(CarCategoryBo bo);

    /**
     * 新增汽车类型
     */
    Boolean insertByBo(CarCategoryBo bo);

    /**
     * 修改汽车类型
     */
    Boolean updateByBo(CarCategoryBo bo);

    /**
     * 校验并批量删除汽车类型信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
