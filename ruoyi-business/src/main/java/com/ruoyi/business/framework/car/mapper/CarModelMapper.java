package com.ruoyi.business.framework.car.mapper;

import com.ruoyi.business.framework.car.domain.CarModel;
import com.ruoyi.business.framework.car.domain.vo.CarModelVo;
import com.ruoyi.common.core.mapper.BaseMapperPlus;

/**
 * 汽车型号Mapper接口
 *
 * @author Gangbb
 * @date 2023-04-03
 */
public interface CarModelMapper extends BaseMapperPlus<CarModelMapper, CarModel, CarModelVo> {

}
