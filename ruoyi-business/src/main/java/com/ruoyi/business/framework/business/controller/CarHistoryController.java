package com.ruoyi.business.framework.business.controller;

import java.util.List;
import java.util.Arrays;

import lombok.RequiredArgsConstructor;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.*;
import cn.dev33.satoken.annotation.SaCheckPermission;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import com.ruoyi.common.annotation.RepeatSubmit;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.PageQuery;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.core.validate.AddGroup;
import com.ruoyi.common.core.validate.EditGroup;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.business.framework.business.domain.CarHistory;
import com.ruoyi.business.framework.business.domain.vo.CarHistoryVo;
import com.ruoyi.business.framework.business.domain.bo.CarHistoryBo;
import com.ruoyi.business.framework.business.service.ICarHistoryService;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 车源浏览足迹
 *
 * @author FYZ
 * @date 2023-04-16
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/business/carHistory")
public class CarHistoryController extends BaseController {

    private final ICarHistoryService iCarHistoryService;

    /**
     * 查询车源浏览足迹列表
     */
    @SaCheckPermission("business:carHistory:list")
    @GetMapping("/list")
    public TableDataInfo<CarHistoryVo> list(CarHistoryBo bo, PageQuery pageQuery) {
        return iCarHistoryService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出车源浏览足迹列表
     */
    @SaCheckPermission("business:carHistory:export")
    @Log(title = "车源浏览足迹", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(CarHistoryBo bo, HttpServletResponse response) {
        List<CarHistoryVo> list = iCarHistoryService.queryList(bo);
        ExcelUtil.exportExcel(list, "车源浏览足迹", CarHistoryVo.class, response);
    }

    /**
     * 获取车源浏览足迹详细信息
     *
     * @param carHistoryId 主键
     */
    @SaCheckPermission("business:carHistory:query")
    @GetMapping("/{carHistoryId}")
    public R<CarHistory> getInfo(@NotNull(message = "主键不能为空") @PathVariable Long carHistoryId) {
        return R.ok(iCarHistoryService.queryById(carHistoryId));
    }

    /**
     * 新增车源浏览足迹
     */
    @SaCheckPermission("business:carHistory:add")
    @Log(title = "车源浏览足迹", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody CarHistoryBo bo) {
        return toAjax(iCarHistoryService.insertByBo(bo));
    }

    /**
     * 修改车源浏览足迹
     */
    @SaCheckPermission("business:carHistory:edit")
    @Log(title = "车源浏览足迹", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody CarHistoryBo bo) {
        return toAjax(iCarHistoryService.updateByBo(bo));
    }

    /**
     * 删除车源浏览足迹
     *
     * @param carHistoryIds 主键串
     */
    @SaCheckPermission("business:carHistory:remove")
    @Log(title = "车源浏览足迹", businessType = BusinessType.DELETE)
    @DeleteMapping("/{carHistoryIds}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空") @PathVariable Long[] carHistoryIds) {
        return toAjax(iCarHistoryService.deleteWithValidByIds(Arrays.asList(carHistoryIds), true));
    }
}
