package com.ruoyi.business.framework.car.domain;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 汽车品牌对象 car_brand
 *
 * @author Gangbb
 * @date 2023-03-25
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("car_brand")
public class CarBrand extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     * 汽车品牌id
     */
    @TableId(value = "car_brand_id")
    private Long carBrandId;
    /**
     * 汽车品牌名称
     */
    private String carBrandName;
    /**
     * 汽车品牌logo
     */
    private String carBrandLogo;
    /**
     * 检索首字母
     */
    private String firstLetter;
    /**
     * 汽车品牌介绍
     */
    private String carBrandDescription;
    /**
     * 删除标志（0代表存在 2代表删除）
     */
    @TableLogic
    private String delFlag;

}
