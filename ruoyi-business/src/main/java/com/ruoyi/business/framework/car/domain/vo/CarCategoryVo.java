package com.ruoyi.business.framework.car.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;



/**
 * 汽车类型视图对象 car_category
 *
 * @author Gangbb
 * @date 2023-04-02
 */
@Data
@ExcelIgnoreUnannotated
public class CarCategoryVo {

    private static final long serialVersionUID = 1L;

    /**
     * 汽车类型id
     */
    @ExcelProperty(value = "汽车类型id")
    private Long carCategoryId;

    /**
     * 汽车类型名
     */
    @ExcelProperty(value = "汽车类型名")
    private String carCategoryName;

    /**
     * 经营类别(字典：operate_category)
     */
    @ExcelProperty(value = "经营类别(字典：operate_category)")
    private String operateCategory;

    /**
     * 排序字段
     */
    @ExcelProperty(value = "排序字段")
    private Integer sort;

    /**
     * 状态(0-正常 2-停用)
     */
    @ExcelProperty(value = "状态(0-正常 2-停用)")
    private String status;


}
