package com.ruoyi.business.framework.car.domain;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 汽车型号对象 car_model
 *
 * @author Gangbb
 * @date 2023-04-03
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("car_model")
public class CarModel extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     * 汽车型号id
     */
    @TableId(value = "car_model_id")
    private Long carModelId;
    /**
     * 父id;(顶层为0)
     */
    private Long parentId;
    /**
     * 关联汽车品牌id;(car_brand.id)
     */
    private Long carBrandId;
    /**
     * 关联汽车车系id;(car_series.id)
     */
    private Long carSeriesId;
    /**
     * 关联汽车车系名称
     */
    private String carSeriesName;
    /**
     * 汽车型号名称
     */
    private String carModelName;
    /**
     * 汽车型号指导价
     */
    private BigDecimal carModelGuidePrice;
    /**
     * 汽车型号发布时间
     */
    private Date carModelReleaseTime;
    /**
     * 可选颜色(多个逗号隔开)
     */
    private String colorOptional;
    /**
     * 可选排放标准(多个逗号隔开)
     */
    private String dischargeOptional;
    /**
     * 排序字段
     */
    private Integer sort;
    /**
     * 删除标志（0代表存在 2代表删除）
     */
    @TableLogic
    private String delFlag;

}
