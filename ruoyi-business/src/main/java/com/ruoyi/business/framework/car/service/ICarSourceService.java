package com.ruoyi.business.framework.car.service;

import com.ruoyi.business.app.car.domain.bo.CarSourceAppBo;
import com.ruoyi.business.app.car.domain.vo.CarSourceAppVo;
import com.ruoyi.business.framework.car.domain.CarSource;
import com.ruoyi.business.framework.car.domain.bo.CarSourceBo;
import com.ruoyi.business.framework.car.domain.vo.CarSourceVo;
import com.ruoyi.common.core.domain.PageQuery;
import com.ruoyi.common.core.page.TableDataInfo;

import java.util.Collection;
import java.util.List;

/**
 * 车源Service接口
 *
 * @author Gangbb
 * @date 2023-04-04
 */
public interface ICarSourceService {

    /**
     * 查询车源
     */
    CarSource queryById(Long carSourceId);

    /**
     * 查询车源列表
     */
    TableDataInfo<CarSourceVo> queryPageList(CarSourceBo bo, PageQuery pageQuery);

    /**
     * 查询车源列表
     */
    List<CarSourceVo> queryList(CarSourceBo bo);

    /**
     * 新增车源
     */
    Boolean insertByBo(CarSourceBo bo);

    /**
     * 修改车源
     */
    Boolean updateByBo(CarSourceBo bo);

    /**
     * 校验并批量删除车源信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);

    /**
     * app查询车源列表
     */
    TableDataInfo<CarSourceAppVo> appList(CarSourceAppBo bo, PageQuery pageQuery);
}
