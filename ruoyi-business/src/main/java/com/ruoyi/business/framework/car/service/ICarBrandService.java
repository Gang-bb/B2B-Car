package com.ruoyi.business.framework.car.service;

import com.ruoyi.business.framework.car.domain.CarBrand;
import com.ruoyi.business.framework.car.domain.vo.CarBrandVo;
import com.ruoyi.business.framework.car.domain.bo.CarBrandBo;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.core.domain.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 汽车品牌Service接口
 *
 * @author Gangbb
 * @date 2023-03-25
 */
public interface ICarBrandService {

    /**
     * 查询汽车品牌
     */
    CarBrand queryById(Long carBrandId);

    /**
     * 查询汽车品牌列表
     */
    TableDataInfo<CarBrandVo> queryPageList(CarBrandBo bo, PageQuery pageQuery);

    /**
     * 查询汽车品牌列表
     */
    List<CarBrandVo> queryList(CarBrandBo bo);

    /**
     * 新增汽车品牌
     */
    Boolean insertByBo(CarBrandBo bo);

    /**
     * 修改汽车品牌
     */
    Boolean updateByBo(CarBrandBo bo);

    /**
     * 校验并批量删除汽车品牌信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
