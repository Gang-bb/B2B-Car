package com.ruoyi.business.framework.business.mapper;

import com.ruoyi.business.framework.business.domain.SourceCollection;
import com.ruoyi.business.framework.business.domain.vo.SourceCollectionVo;
import com.ruoyi.common.core.mapper.BaseMapperPlus;

/**
 * 车源收藏Mapper接口
 *
 * @author Gangbb
 * @date 2023-04-15
 */
public interface SourceCollectionMapper extends BaseMapperPlus<SourceCollectionMapper, SourceCollection, SourceCollectionVo> {

}
