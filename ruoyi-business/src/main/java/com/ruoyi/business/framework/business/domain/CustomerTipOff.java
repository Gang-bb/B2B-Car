package com.ruoyi.business.framework.business.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;

import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 客户举报对象 b_customer_tip_off
 *
 * @author FYZ
 * @date 2023-04-23
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("b_customer_tip_off")
public class CustomerTipOff extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     * 客户举报id
     */
    @TableId(value = "customer_tip_off_id")
    private Long customerTipOffId;
    /**
     * 关联用户id;(sys_user.id)
     */
    private Long userId;
    /**
     * 关联车源id;(car_source.id)
     */
    private Long carSourceId;
    /**
     * 举报人id;(sys_user.id)
     */
    private Long tipOffUserId;
    /**
     * 举报类型;(字典：tip_off_type)
     */
    private String tipOffType;
    /**
     * 举报内容
     */
    private String tipOffContent;
    /**
     * 删除标志（0代表存在 2代表删除）
     */
    @TableLogic
    private String delFlag;

}
