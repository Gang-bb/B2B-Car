package com.ruoyi.business.framework.car.mapper;

import com.ruoyi.business.app.car.domain.vo.CarSeriesPickListOneVo;
import com.ruoyi.business.framework.car.domain.CarSeries;
import com.ruoyi.business.framework.car.domain.vo.CarSeriesVo;
import com.ruoyi.common.core.mapper.BaseMapperPlus;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 汽车车系Mapper接口
 *
 * @author Gangbb
 * @date 2023-03-25
 */
public interface CarSeriesMapper extends BaseMapperPlus<CarSeriesMapper, CarSeries, CarSeriesVo> {

    /**
     * 选择车系页面1 对应查询
     *
     * @param carBrandId 品牌id
     * @return List<CarSeriesPickListOneVo>
     * @date 2023-04-15
     **/
    List<CarSeriesPickListOneVo> pickListOne(@Param("carBrandId") Long carBrandId);
}
