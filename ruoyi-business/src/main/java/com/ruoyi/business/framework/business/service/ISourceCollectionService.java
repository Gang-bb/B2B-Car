package com.ruoyi.business.framework.business.service;

import com.ruoyi.business.app.business.domain.bo.SourceCollectionAppBo;
import com.ruoyi.business.app.business.domain.vo.SourceCollectionAppVo;
import com.ruoyi.business.framework.business.domain.SourceCollection;
import com.ruoyi.business.framework.business.domain.vo.SourceCollectionVo;
import com.ruoyi.business.framework.business.domain.bo.SourceCollectionBo;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.core.domain.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 车源收藏Service接口
 *
 * @author Gangbb
 * @date 2023-04-15
 */
public interface ISourceCollectionService {

    /**
     * 查询车源收藏
     */
    SourceCollection queryById(Long sourceCollectionId);

    /**
     * 查询车源收藏列表
     */
    TableDataInfo<SourceCollectionVo> queryPageList(SourceCollectionBo bo, PageQuery pageQuery);

    /**
     * 查询车源收藏列表
     */
    List<SourceCollectionVo> queryList(SourceCollectionBo bo);

    /**
     * 新增车源收藏
     */
    Boolean insertByBo(SourceCollectionBo bo);

    /**
     * 修改车源收藏
     */
    Boolean updateByBo(SourceCollectionBo bo);

    /**
     * 校验并批量删除车源收藏信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);

    /**
     * app 查询车源收藏列表
     */
    TableDataInfo<SourceCollectionAppVo> appList(PageQuery pageQuery);

    /**
     * app 根据车源id查询车源收藏信息
     */
    Long queryByCarSourceId(Long carSourceId);

    /**
     * app 新增车源收藏
     */
    Boolean insertBycCarSourceId(SourceCollectionAppBo bo);

    /**
     * app 根据车源id删除车源收藏信息
     */
    Boolean deleteByCarSourceId(Long carSourceId);

    /**
     * app 根据用户id删除所有相关车源收藏信息
     */
    Boolean deleteAllByUserId();
}
