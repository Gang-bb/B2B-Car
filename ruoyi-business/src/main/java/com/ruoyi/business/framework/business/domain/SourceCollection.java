package com.ruoyi.business.framework.business.domain;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 车源收藏对象 b_source_collection
 *
 * @author Gangbb
 * @date 2023-04-15
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("b_source_collection")
public class SourceCollection extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     * 车源收藏id
     */
    @TableId(value = "source_collection_id")
    private Long sourceCollectionId;
    /**
     * 关联用户id;(sys_user.id)
     */
    private Long userId;
    /**
     * 关联车源id;(b_car_source.id)
     */
    private Long carSourceId;
    /**
     * 删除标志（0代表存在 2代表删除）
     */
    @TableLogic
    private String delFlag;

}
