package com.ruoyi.business.framework.business.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;

import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 通话记录对象 b_call_history
 *
 * @author FYZ
 * @date 2023-04-22
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("b_call_history")
public class CallHistory extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     * 通话记录id
     */
    @TableId(value = "call_history_id")
    private Long callHistoryId;
    /**
     * 关联用户id;(sys_user.id)
     */
    private Long userId;
    /**
     * 关联车源id;(car_source.id)
     */
    private Long carSourceId;
    /**
     * 来电用户id;(sys_user.id)
     */
    private Long callerUserId;
    /**
     * 通话标志;(字典：call_flag；1:联系我的，2:我联系的)
     */
    private String callFlag;
    /**
     * 通话状态;(字典：call_status；1:未接通，2:已接通)
     */
    private String callStatus;
    /**
     * 删除标志（0代表存在 2代表删除）
     */
    @TableLogic
    private String delFlag;

}
