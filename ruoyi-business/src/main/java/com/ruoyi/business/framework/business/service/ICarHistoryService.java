package com.ruoyi.business.framework.business.service;

import com.ruoyi.business.app.business.domain.bo.CarHistoryAppBo;
import com.ruoyi.business.app.business.domain.vo.CarHistoryAppVo;
import com.ruoyi.business.framework.business.domain.CarHistory;
import com.ruoyi.business.framework.business.domain.vo.CarHistoryVo;
import com.ruoyi.business.framework.business.domain.bo.CarHistoryBo;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.core.domain.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 车源浏览足迹Service接口
 *
 * @author FYZ
 * @date 2023-04-16
 */
public interface ICarHistoryService {

    /**
     * 查询车源浏览足迹
     */
    CarHistory queryById(Long carHistoryId);

    /**
     * 查询车源浏览足迹列表
     */
    TableDataInfo<CarHistoryVo> queryPageList(CarHistoryBo bo, PageQuery pageQuery);

    /**
     * 查询车源浏览足迹列表
     */
    List<CarHistoryVo> queryList(CarHistoryBo bo);

    /**
     * 新增车源浏览足迹
     */
    Boolean insertByBo(CarHistoryBo bo);

    /**
     * 修改车源浏览足迹
     */
    Boolean updateByBo(CarHistoryBo bo);

    /**
     * 校验并批量删除车源浏览足迹信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);

    /**
     * app 查询车源浏览足迹列表
     */
    TableDataInfo<CarHistoryAppVo> appList(PageQuery pageQuery);

    /**
     * app 新增车源浏览足迹
     */
    Boolean insertBycCarSourceId(CarHistoryAppBo bo);

    /**
     * app 根据车源id删除车源浏览足迹信息
     */
    Boolean deleteByCarSourceId(Long carSourceId);

    /**
     * app 根据用户id删除所有相关车源浏览足迹信息
     */
    Boolean deleteAllByUserId();
}
