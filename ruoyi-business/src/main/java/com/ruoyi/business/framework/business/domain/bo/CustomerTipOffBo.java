package com.ruoyi.business.framework.business.domain.bo;

import com.ruoyi.common.core.validate.AddGroup;
import com.ruoyi.common.core.validate.EditGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;
import javax.validation.constraints.*;

import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 客户举报业务对象 b_customer_tip_off
 *
 * @author FYZ
 * @date 2023-04-23
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class CustomerTipOffBo extends BaseEntity {

    /**
     * 客户举报id
     */
    @NotNull(message = "客户举报id不能为空", groups = { EditGroup.class })
    private Long customerTipOffId;

    /**
     * 关联用户id;(sys_user.id)
     */
    @NotNull(message = "关联用户id;(sys_user.id)不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long userId;

    /**
     * 关联车源id;(car_source.id)
     */
    @NotNull(message = "关联车源id;(car_source.id)不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long carSourceId;

    /**
     * 举报人id;(sys_user.id)
     */
    @NotNull(message = "举报人id;(sys_user.id)不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long tipOffUserId;

    /**
     * 举报类型;(字典：tip_off_type)
     */
    @NotBlank(message = "举报类型;(字典：tip_off_type)不能为空", groups = { AddGroup.class, EditGroup.class })
    private String tipOffType;

    /**
     * 举报内容
     */
    @NotBlank(message = "举报内容不能为空", groups = { AddGroup.class, EditGroup.class })
    private String tipOffContent;


}
