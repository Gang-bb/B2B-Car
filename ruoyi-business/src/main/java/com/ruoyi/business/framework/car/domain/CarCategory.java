package com.ruoyi.business.framework.car.domain;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 汽车类型对象 car_category
 *
 * @author Gangbb
 * @date 2023-04-02
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("car_category")
public class CarCategory extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     * 汽车类型id
     */
    @TableId(value = "car_category_id")
    private Long carCategoryId;
    /**
     * 汽车类型名
     */
    private String carCategoryName;
    /**
     * 经营类别(字典：operate_category)
     */
    private String operateCategory;
    /**
     * 排序字段
     */
    private Integer sort;
    /**
     * 状态(0-正常 2-停用)
     */
    private String status;
    /**
     * 删除标志（0代表存在 2代表删除）
     */
    @TableLogic
    private String delFlag;

}
