package com.ruoyi.business.framework.car.mapper;

import com.ruoyi.business.framework.car.domain.CarBrand;
import com.ruoyi.business.framework.car.domain.vo.CarBrandVo;
import com.ruoyi.common.core.mapper.BaseMapperPlus;

/**
 * 汽车品牌Mapper接口
 *
 * @author Gangbb
 * @date 2023-03-24
 */
public interface CarBrandMapper extends BaseMapperPlus<CarBrandMapper, CarBrand, CarBrandVo> {

}
