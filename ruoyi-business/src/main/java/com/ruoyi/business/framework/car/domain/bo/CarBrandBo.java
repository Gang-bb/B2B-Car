package com.ruoyi.business.framework.car.domain.bo;

import com.ruoyi.common.core.domain.BaseEntity;
import com.ruoyi.common.core.validate.AddGroup;
import com.ruoyi.common.core.validate.EditGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * 汽车品牌业务对象 car_brand
 *
 * @author Gangbb
 * @date 2023-03-25
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class CarBrandBo extends BaseEntity {

    /**
     * 汽车品牌id
     */
    @NotNull(message = "汽车品牌id不能为空", groups = { EditGroup.class })
    private Long carBrandId;

    /**
     * 汽车品牌名称
     */
    @NotBlank(message = "汽车品牌名称不能为空", groups = { AddGroup.class, EditGroup.class })
    private String carBrandName;

    /**
     * 汽车品牌logo
     */
    @NotBlank(message = "汽车品牌logo不能为空", groups = { AddGroup.class, EditGroup.class })
    private String carBrandLogo;

    /**
     * 检索首字母(多个逗号隔开)
     */
    private String firstLetter;

    /**
     * 汽车品牌介绍
     */
    private String carBrandDescription;


}
