package com.ruoyi.business.framework.car.mapper;

import com.ruoyi.business.framework.car.domain.CarCategory;
import com.ruoyi.business.framework.car.domain.vo.CarCategoryVo;
import com.ruoyi.common.core.mapper.BaseMapperPlus;

/**
 * 汽车类型Mapper接口
 *
 * @author Gangbb
 * @date 2023-04-02
 */
public interface CarCategoryMapper extends BaseMapperPlus<CarCategoryMapper, CarCategory, CarCategoryVo> {

}
