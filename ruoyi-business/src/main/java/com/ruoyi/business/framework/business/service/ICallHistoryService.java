package com.ruoyi.business.framework.business.service;

import com.ruoyi.business.app.business.domain.bo.CallHistoryAppBo;
import com.ruoyi.business.app.business.domain.bo.SourceCollectionAppBo;
import com.ruoyi.business.app.business.domain.vo.CallHistoryAppVo;
import com.ruoyi.business.app.business.domain.vo.SourceCollectionAppVo;
import com.ruoyi.business.framework.business.domain.CallHistory;
import com.ruoyi.business.framework.business.domain.vo.CallHistoryVo;
import com.ruoyi.business.framework.business.domain.bo.CallHistoryBo;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.core.domain.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 通话记录Service接口
 *
 * @author FYZ
 * @date 2023-04-16
 */
public interface ICallHistoryService {

    /**
     * 查询通话记录
     */
    CallHistory queryById(Long callHistoryId);

    /**
     * 查询通话记录列表
     */
    TableDataInfo<CallHistoryVo> queryPageList(CallHistoryBo bo, PageQuery pageQuery);

    /**
     * 查询通话记录列表
     */
    List<CallHistoryVo> queryList(CallHistoryBo bo);

    /**
     * 新增通话记录
     */
    Boolean insertByBo(CallHistoryBo bo);

    /**
     * 修改通话记录
     */
    Boolean updateByBo(CallHistoryBo bo);

    /**
     * 校验并批量删除通话记录信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);

    /**
     * app 查询通话记录列表
     *
     * @param callFlag 通话标志 1:联系我的，2:我联系的
     */
    TableDataInfo<CallHistoryAppVo> appList(String callFlag, PageQuery pageQuery);

    /**
     * app 新增通话记录
     */
    Boolean insertBycCarSourceId(CallHistoryAppBo bo);

    /**
     * app 根据车源id删除通话记录信息
     */
    Boolean deleteByCarSourceId(Long carSourceId);

    /**
     * app 根据用户id删除所有相关通话记录信息
     */
    Boolean deleteAllByUserId();
}
