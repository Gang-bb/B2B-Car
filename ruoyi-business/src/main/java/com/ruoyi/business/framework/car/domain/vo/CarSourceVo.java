package com.ruoyi.business.framework.car.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

import java.math.BigDecimal;



/**
 * 车源视图对象 car_source
 *
 * @author Gangbb
 * @date 2023-04-04
 */
@Data
@ExcelIgnoreUnannotated
public class CarSourceVo {

    private static final long serialVersionUID = 1L;

    /**
     * 车源id
     */
    @ExcelProperty(value = "车源id")
    private Long carSourceId;

    /**
     * 关联汽车类型id;(car_category.id)
     */
    @ExcelProperty(value = "关联汽车类型id;(car_category.id)")
    private Long carCategoryId;

    /**
     * 关联汽车品牌id;(car_brand.id)
     */
    @ExcelProperty(value = "关联汽车品牌id;(car_brand.id)")
    private Long carBrandId;

    /**
     * 关联汽车车系id;(car_series.id)
     */
    @ExcelProperty(value = "关联汽车车系id;(car_series.id)")
    private Long carSeriesId;

    /**
     * 关联汽车型号id;(car_model.id)
     */
    @ExcelProperty(value = "关联汽车型号id;(car_model.id)")
    private Long carModelId;

    /**
     * 车源信息(汽车型号名 +指导价)
     */
    @ExcelProperty(value = "车源信息(汽车型号名 +指导价)")
    private String carSourceInfo;

    /**
     * 最终价格
     */
    @ExcelProperty(value = "最终价格")
    private BigDecimal finalPrice;

    /**
     * 车源有效期(天数)
     */
    @ExcelProperty(value = "车源有效期(天数)")
    private Integer sourceTermOfValidity;

    /**
     * 车源所在地
     */
    @ExcelProperty(value = "车源所在地")
    private String sourceLocation;

    /**
     * 车架号
     */
    @ExcelProperty(value = "车架号")
    private String vehicleFrame;

    /**
     * 是否自营车源(0-否 1-是)
     */
    @ExcelProperty(value = "是否自营车源(0-否 1-是)")
    private Integer isSelfSupport;


}
