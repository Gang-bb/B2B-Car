package com.ruoyi.business.framework.car.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.ruoyi.business.framework.car.domain.CarSource;
import com.ruoyi.business.framework.car.domain.bo.CarSourceBo;
import com.ruoyi.business.framework.car.domain.vo.CarSourceVo;
import com.ruoyi.business.framework.car.service.ICarSourceService;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.annotation.RepeatSubmit;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.PageQuery;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.core.validate.AddGroup;
import com.ruoyi.common.core.validate.EditGroup;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Arrays;
import java.util.List;

/**
 * 车源
 *
 * @author Gangbb
 * @date 2023-04-04
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/car/source")
public class CarSourceController extends BaseController {

    private final ICarSourceService iCarSourceService;

    /**
     * 查询车源列表
     */
    @SaCheckPermission("car:source:list")
    @GetMapping("/list")
    public TableDataInfo<CarSourceVo> list(CarSourceBo bo, PageQuery pageQuery) {
        return iCarSourceService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出车源列表
     */
    @SaCheckPermission("car:source:export")
    @Log(title = "车源", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(CarSourceBo bo, HttpServletResponse response) {
        List<CarSourceVo> list = iCarSourceService.queryList(bo);
        ExcelUtil.exportExcel(list, "车源", CarSourceVo.class, response);
    }

    /**
     * 获取车源详细信息
     *
     * @param carSourceId 主键
     */
    @SaCheckPermission("car:source:query")
    @GetMapping("/{carSourceId}")
    public R<CarSource> getInfo(@NotNull(message = "主键不能为空")
                                @PathVariable Long carSourceId) {
        return R.ok(iCarSourceService.queryById(carSourceId));
    }

    /**
     * 新增车源
     */
    @SaCheckPermission("car:source:add")
    @Log(title = "车源", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody CarSourceBo bo) {
        return toAjax(iCarSourceService.insertByBo(bo));
    }

    /**
     * 修改车源
     */
    @SaCheckPermission("car:source:edit")
    @Log(title = "车源", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody CarSourceBo bo) {
        return toAjax(iCarSourceService.updateByBo(bo));
    }

    /**
     * 删除车源
     *
     * @param carSourceIds 主键串
     */
    @SaCheckPermission("car:source:remove")
    @Log(title = "车源", businessType = BusinessType.DELETE)
    @DeleteMapping("/{carSourceIds}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] carSourceIds) {
        return toAjax(iCarSourceService.deleteWithValidByIds(Arrays.asList(carSourceIds), true));
    }
}
