package com.ruoyi.business.framework.car.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.core.domain.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import com.ruoyi.business.framework.car.domain.bo.CarModelBo;
import com.ruoyi.business.framework.car.domain.vo.CarModelVo;
import com.ruoyi.business.framework.car.domain.CarModel;
import com.ruoyi.business.framework.car.mapper.CarModelMapper;
import com.ruoyi.business.framework.car.service.ICarModelService;

import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 汽车型号Service业务层处理
 *
 * @author Gangbb
 * @date 2023-04-03
 */
@RequiredArgsConstructor
@Service
public class CarModelServiceImpl implements ICarModelService {

    private final CarModelMapper baseMapper;

    /**
     * 查询汽车型号
     */
    @Override
    public CarModel queryById(Long carModelId){
        return baseMapper.selectById(carModelId);
    }

    /**
     * 查询汽车型号列表
     */
    @Override
    public TableDataInfo<CarModelVo> queryPageList(CarModelBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<CarModel> lqw = buildQueryWrapper(bo);
        Page<CarModelVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询汽车型号列表
     */
    @Override
    public List<CarModelVo> queryList(CarModelBo bo) {
        LambdaQueryWrapper<CarModel> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<CarModel> buildQueryWrapper(CarModelBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<CarModel> lqw = Wrappers.lambdaQuery();
        lqw.eq(bo.getParentId() != null, CarModel::getParentId, bo.getParentId());
        lqw.eq(bo.getCarBrandId() != null, CarModel::getCarBrandId, bo.getCarBrandId());
        lqw.eq(bo.getCarSeriesId() != null, CarModel::getCarSeriesId, bo.getCarSeriesId());
        lqw.like(StringUtils.isNotBlank(bo.getCarSeriesName()), CarModel::getCarSeriesName, bo.getCarSeriesName());
        lqw.like(StringUtils.isNotBlank(bo.getCarModelName()), CarModel::getCarModelName, bo.getCarModelName());
        return lqw;
    }

    /**
     * 新增汽车型号
     */
    @Override
    public Boolean insertByBo(CarModelBo bo) {
        CarModel add = BeanUtil.toBean(bo, CarModel.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setCarModelId(add.getCarModelId());
        }
        return flag;
    }

    /**
     * 修改汽车型号
     */
    @Override
    public Boolean updateByBo(CarModelBo bo) {
        CarModel update = BeanUtil.toBean(bo, CarModel.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(CarModel entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除汽车型号
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }
}
