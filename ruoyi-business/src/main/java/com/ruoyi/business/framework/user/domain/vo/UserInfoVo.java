package com.ruoyi.business.framework.user.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;



/**
 * 用户平台信息视图对象 u_user_info
 *
 * @author Gangbb
 * @date 2023-03-28
 */
@Data
@ExcelIgnoreUnannotated
public class UserInfoVo {

    private static final long serialVersionUID = 1L;

    /**
     * 用户平台信息id
     */
    @ExcelProperty(value = "用户平台信息id")
    private Long userInfoId;

    /**
     * 登陆手机号
     */
    @ExcelProperty(value = "登陆手机号")
    private String loginPhone;

    /**
     * 个人认证是否通过(0-否 1-是)
     */
    @ExcelProperty(value = "个人认证是否通过(0-否 1-是)")
    private Integer personalAuthRes;

    /**
     * 用户真实姓名
     */
    @ExcelProperty(value = "用户真实姓名")
    private String realName;

    /**
     * 身份证号
     */
    @ExcelProperty(value = "身份证号")
    private String idCard;

    /**
     * 公司认证是否通过(0-否 1-是)
     */
    @ExcelProperty(value = "公司认证是否通过(0-否 1-是)")
    private Integer companyAuthRes;

    /**
     * 授信认证是否通过(0-否 1-是)
     */
    @ExcelProperty(value = "授信认证是否通过(0-否 1-是)")
    private Integer creditAuth;

    /**
     * 授信认证评级id;(u_user_grade.id)
     */
    @ExcelProperty(value = "授信认证评级id;(u_user_grade.id)")
    private Long userGradeId;


}
