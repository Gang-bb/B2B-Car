package com.ruoyi.business.framework.car.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.ruoyi.business.framework.car.domain.CarModel;
import com.ruoyi.business.framework.car.domain.bo.CarModelBo;
import com.ruoyi.business.framework.car.domain.vo.CarModelVo;
import com.ruoyi.business.framework.car.service.ICarModelService;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.annotation.RepeatSubmit;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.PageQuery;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.core.validate.AddGroup;
import com.ruoyi.common.core.validate.EditGroup;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Arrays;
import java.util.List;

/**
 * 汽车型号
 *
 * @author Gangbb
 * @date 2023-04-03
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/car/model")
public class CarModelController extends BaseController {

    private final ICarModelService iCarModelService;

    /**
     * 查询汽车型号列表
     */
    @SaCheckPermission("car:model:list")
    @GetMapping("/list")
    public TableDataInfo<CarModelVo> list(CarModelBo bo, PageQuery pageQuery) {
        return iCarModelService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出汽车型号列表
     */
    @SaCheckPermission("car:model:export")
    @Log(title = "汽车型号", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(CarModelBo bo, HttpServletResponse response) {
        List<CarModelVo> list = iCarModelService.queryList(bo);
        ExcelUtil.exportExcel(list, "汽车型号", CarModelVo.class, response);
    }

    /**
     * 获取汽车型号详细信息
     *
     * @param carModelId 主键
     */
    @SaCheckPermission("car:model:query")
    @GetMapping("/{carModelId}")
    public R<CarModel> getInfo(@NotNull(message = "主键不能为空")
                               @PathVariable Long carModelId) {
        return R.ok(iCarModelService.queryById(carModelId));
    }

    /**
     * 新增汽车型号
     */
    @SaCheckPermission("car:model:add")
    @Log(title = "汽车型号", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody CarModelBo bo) {
        return toAjax(iCarModelService.insertByBo(bo));
    }

    /**
     * 修改汽车型号
     */
    @SaCheckPermission("car:model:edit")
    @Log(title = "汽车型号", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody CarModelBo bo) {
        return toAjax(iCarModelService.updateByBo(bo));
    }

    /**
     * 删除汽车型号
     *
     * @param carModelIds 主键串
     */
    @SaCheckPermission("car:model:remove")
    @Log(title = "汽车型号", businessType = BusinessType.DELETE)
    @DeleteMapping("/{carModelIds}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] carModelIds) {
        return toAjax(iCarModelService.deleteWithValidByIds(Arrays.asList(carModelIds), true));
    }
}
