package com.ruoyi.business.framework.business.service;

import com.ruoyi.business.app.business.domain.bo.CustomerTipOffAppBo;
import com.ruoyi.business.app.business.domain.bo.SourceCollectionAppBo;
import com.ruoyi.business.app.business.domain.vo.CustomerTipOffAppVo;
import com.ruoyi.business.app.business.domain.vo.SourceCollectionAppVo;
import com.ruoyi.business.framework.business.domain.CustomerTipOff;
import com.ruoyi.business.framework.business.domain.vo.CustomerTipOffVo;
import com.ruoyi.business.framework.business.domain.bo.CustomerTipOffBo;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.core.domain.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 客户举报Service接口
 *
 * @author FYZ
 * @date 2023-04-16
 */
public interface ICustomerTipOffService {

    /**
     * 查询客户举报
     */
    CustomerTipOff queryById(Long customerTipOffId);

    /**
     * 查询客户举报列表
     */
    TableDataInfo<CustomerTipOffVo> queryPageList(CustomerTipOffBo bo, PageQuery pageQuery);

    /**
     * 查询客户举报列表
     */
    List<CustomerTipOffVo> queryList(CustomerTipOffBo bo);

    /**
     * 新增客户举报
     */
    Boolean insertByBo(CustomerTipOffBo bo);

    /**
     * 修改客户举报
     */
    Boolean updateByBo(CustomerTipOffBo bo);

    /**
     * 校验并批量删除客户举报信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);

    /**
     * app 查询客户举报列表
     */
    TableDataInfo<CustomerTipOffAppVo> appList(PageQuery pageQuery);

    /**
     * app 根据车源id查询客户举报信息
     */
    Long queryByCarSourceId(Long carSourceId);

    /**
     * app 新增客户举报
     */
    Boolean insertBycCarSourceId(CustomerTipOffAppBo bo);

    /**
     * app 根据车源id删除客户举报信息
     */
    Boolean deleteByCarSourceId(Long carSourceId);

    /**
     * app 根据用户id删除所有相关客户举报信息
     */
    Boolean deleteAllByUserId();
}
