package com.ruoyi.business.framework.car.domain;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ruoyi.common.core.domain.TreeEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 汽车车系对象 car_series
 *
 * @author Gangbb
 * @date 2023-04-04
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("car_series")
public class CarSeries extends TreeEntity<CarSeries> {

    private static final long serialVersionUID=1L;

    /**
     * 汽车车系id
     */
    @TableId(value = "car_series_id")
    private Long carSeriesId;
    /**
     * 关联汽车类型id(car_category.id)
     */
    private Long carCategoryId;
    /**
     * 关联汽车品牌id;(car_brand.id)
     */
    private Long carBrandId;
    /**
     * 汽车车系名称
     */
    private String carSeriesName;
    /**
     * 排序字段
     */
    private Integer sort;
    /**
     * 状态(0-正常 2-停用)
     */
    private String status;
    /**
     * 删除标志（0代表存在 2代表删除）
     */
    @TableLogic
    private String delFlag;

}
