package com.ruoyi.business.framework.business.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.ruoyi.business.app.business.domain.bo.CustomerTipOffAppBo;
import com.ruoyi.business.app.business.domain.vo.CustomerTipOffAppVo;
import com.ruoyi.business.app.car.domain.bo.CarSourceAppBo;
import com.ruoyi.business.app.car.domain.vo.CarSourceAppVo;
import com.ruoyi.business.framework.car.domain.CarSource;
import com.ruoyi.business.framework.car.service.ICarSourceService;
import com.ruoyi.common.helper.LoginHelper;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.core.domain.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import com.ruoyi.business.framework.business.domain.bo.CustomerTipOffBo;
import com.ruoyi.business.framework.business.domain.vo.CustomerTipOffVo;
import com.ruoyi.business.framework.business.domain.CustomerTipOff;
import com.ruoyi.business.framework.business.mapper.CustomerTipOffMapper;
import com.ruoyi.business.framework.business.service.ICustomerTipOffService;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 客户举报Service业务层处理
 *
 * @author FYZ
 * @date 2023-04-16
 */
@RequiredArgsConstructor
@Service
public class CustomerTipOffServiceImpl implements ICustomerTipOffService {

    private final CustomerTipOffMapper baseMapper;
    private final ICarSourceService iCarSourceService;

    /**
     * 查询客户举报
     */
    @Override
    public CustomerTipOff queryById(Long customerTipOffId){
        return baseMapper.selectById(customerTipOffId);
    }

    /**
     * 查询客户举报列表
     */
    @Override
    public TableDataInfo<CustomerTipOffVo> queryPageList(CustomerTipOffBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<CustomerTipOff> lqw = buildQueryWrapper(bo);
        Page<CustomerTipOffVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询客户举报列表
     */
    @Override
    public List<CustomerTipOffVo> queryList(CustomerTipOffBo bo) {
        LambdaQueryWrapper<CustomerTipOff> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<CustomerTipOff> buildQueryWrapper(CustomerTipOffBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<CustomerTipOff> lqw = Wrappers.lambdaQuery();
        lqw.eq(bo.getUserId() != null, CustomerTipOff::getUserId, bo.getUserId());
        lqw.eq(bo.getCarSourceId() != null, CustomerTipOff::getCarSourceId, bo.getCarSourceId());
        lqw.eq(bo.getTipOffUserId() != null, CustomerTipOff::getTipOffUserId, bo.getTipOffUserId());
        lqw.orderByDesc(CustomerTipOff::getUpdateTime);
        return lqw;
    }

    /**
     * 新增客户举报
     */
    @Override
    public Boolean insertByBo(CustomerTipOffBo bo) {
        CustomerTipOff add = BeanUtil.toBean(bo, CustomerTipOff.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setCustomerTipOffId(add.getCustomerTipOffId());
        }
        return flag;
    }

    /**
     * 修改客户举报
     */
    @Override
    public Boolean updateByBo(CustomerTipOffBo bo) {
        CustomerTipOff update = BeanUtil.toBean(bo, CustomerTipOff.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(CustomerTipOff entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除客户举报
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }

    /**
     * app 查询客户举报列表
     */
    @Override
    public TableDataInfo<CustomerTipOffAppVo> appList(PageQuery pageQuery) {
        // 根据当前登录用户id 获取客户举报列表
        Long userId = LoginHelper.getUserId();
        CustomerTipOffBo customerTipOffBo = new CustomerTipOffBo();
        customerTipOffBo.setUserId(userId);
        List<CustomerTipOffVo> customerTipOffVoList = queryList(customerTipOffBo);
//        TableDataInfo<SourceCollectionVo> scTableDataInfo = queryPageList(sourceCollectionBo, pageQuery);
//        List<SourceCollectionVo> sourceCollectionVoList = scTableDataInfo.getRows();

        // 获取车源id 集合，集合数据量可能会过大，导致 select in() sql 报错
        List<Long> onlyNeedIdList = new ArrayList<>();
        customerTipOffVoList.forEach((item) -> {
            onlyNeedIdList.add(item.getCarSourceId());
        });

        // 根据车源id查询车源
        CarSourceAppBo carSourceAppBo = new CarSourceAppBo();
        carSourceAppBo.setOnlyNeedIdList(onlyNeedIdList);
        TableDataInfo<CarSourceAppVo> csTableDataInfo = iCarSourceService.appList(carSourceAppBo, pageQuery);

        // 加上客户举报id
        List<CustomerTipOffAppVo> appVoList = new ArrayList<>();
        for (CarSourceAppVo vo : csTableDataInfo.getRows()) {
            customerTipOffVoList.forEach((item) -> {
                if (vo.getCarSourceId() == item.getCarSourceId()) {
                    CustomerTipOffAppVo appVo = BeanUtil.toBean(vo, CustomerTipOffAppVo.class);
                    appVo.setCustomerTipOffId(item.getCustomerTipOffId());
                    appVo.setTipOffType(item.getTipOffType());
                    appVo.setTipOffContent(item.getTipOffContent());
                    appVo.setTipOffUserId(item.getTipOffUserId());
                    appVoList.add(appVo);
                }
            });
        }

        // TODO 排序还有点问题，从 iCarSourceService.appList() 查询回来的数据 和
        //  从 车源收藏表查询回来的数据排序不一样

        return TableDataInfo.build(appVoList);
    }

    /**
     * app 根据车源id查询客户举报信息
     */
    @Override
    public Long queryByCarSourceId(Long carSourceId) {
        Long userId = LoginHelper.getUserId();
        LambdaQueryWrapper<CustomerTipOff> lqw = Wrappers.lambdaQuery();
        lqw.eq(userId != null, CustomerTipOff::getUserId, userId);
        lqw.eq(carSourceId != null, CustomerTipOff::getCarSourceId, carSourceId);
        CustomerTipOff customerTipOff = baseMapper.selectOne(lqw);
        if (customerTipOff != null) {
            return customerTipOff.getCustomerTipOffId();
        }
        return null;
    }

    /**
     * app 新增客户举报
     */
    @Override
    public Boolean insertBycCarSourceId(CustomerTipOffAppBo bo) {
        Long userId = LoginHelper.getUserId();
        //举报方
        CustomerTipOffBo customerTipOffBo = new CustomerTipOffBo();
        customerTipOffBo.setCarSourceId(bo.getCarSourceId());
        customerTipOffBo.setUserId(userId);
        customerTipOffBo.setTipOffType(bo.getTipOffType());
        customerTipOffBo.setTipOffContent(bo.getTipOffContent());
        customerTipOffBo.setTipOffUserId(userId);
        insertByBo(customerTipOffBo);

        //被举报方
        CustomerTipOffBo customerTipOffBo2 = new CustomerTipOffBo();
        customerTipOffBo2.setCarSourceId(bo.getCarSourceId());
        CarSource carSource = iCarSourceService.queryById(bo.getCarSourceId());
        customerTipOffBo2.setUserId(carSource.getUserId());
        customerTipOffBo2.setTipOffType(bo.getTipOffType());
        customerTipOffBo2.setTipOffContent(bo.getTipOffContent());
        customerTipOffBo2.setTipOffUserId(userId);
        return insertByBo(customerTipOffBo2);
    }

    /**
     * app 根据车源id删除客户举报信息
     */
    @Override
    public Boolean deleteByCarSourceId(Long carSourceId) {
        Long userId = LoginHelper.getUserId();
        LambdaQueryWrapper<CustomerTipOff> lqw = Wrappers.lambdaQuery();
        lqw.eq(userId != null, CustomerTipOff::getUserId, userId);
        lqw.eq(carSourceId != null, CustomerTipOff::getCarSourceId, carSourceId);

        return baseMapper.delete(lqw) > 0;
    }

    /**
     * app 根据用户id删除所有相关客户举报信息
     */
    @Override
    public Boolean deleteAllByUserId() {
        Long userId = LoginHelper.getUserId();
        LambdaQueryWrapper<CustomerTipOff> lqw = Wrappers.lambdaQuery();
        lqw.eq(userId != null, CustomerTipOff::getUserId, userId);

        Long count = baseMapper.selectCount(lqw);
        if (count == 0) {
            return true;
        }
        return baseMapper.delete(lqw) > 0;
    }
}
