package com.ruoyi.business.framework.car.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.ruoyi.business.app.car.domain.vo.CarSeriesPickListOneVo;
import com.ruoyi.business.app.car.domain.vo.SeriesPickListItemVo;
import com.ruoyi.business.framework.car.domain.CarBrand;
import com.ruoyi.business.framework.car.domain.CarCategory;
import com.ruoyi.business.framework.car.domain.CarSeries;
import com.ruoyi.business.framework.car.domain.bo.CarSeriesBo;
import com.ruoyi.business.framework.car.domain.vo.CarSeriesVo;
import com.ruoyi.business.framework.car.mapper.CarBrandMapper;
import com.ruoyi.business.framework.car.mapper.CarCategoryMapper;
import com.ruoyi.business.framework.car.mapper.CarSeriesMapper;
import com.ruoyi.business.framework.car.service.ICarSeriesService;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.TransRelationUtils;
import com.ruoyi.common.utils.TreeUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * 汽车车系Service业务层处理
 *
 * @author Gangbb
 * @date 2023-04-04
 */
@RequiredArgsConstructor
@Service
public class CarSeriesServiceImpl implements ICarSeriesService {

    private final CarSeriesMapper baseMapper;
    private final CarBrandMapper carBrandMapper;

    /**
     * 查询汽车车系
     */
    @Override
    public CarSeries queryById(Long carSeriesId){
        return baseMapper.selectById(carSeriesId);
    }


    /**
     * 查询汽车车系列表
     */
    @Override
    public List<CarSeriesVo> queryList(CarSeriesBo bo) {
        LambdaQueryWrapper<CarSeries> lqw = buildQueryWrapper(bo);
        List<CarSeriesVo> carSeriesVoList = baseMapper.selectVoList(lqw);
        if(CollUtil.isNotEmpty(carSeriesVoList)){
            // 处理 汽车品牌名称
            Map<?, ?> carBrandIdToNameMap = TransRelationUtils.getTransMap(carSeriesVoList, CarSeriesVo::getCarBrandId,
                    CarBrandMapper.class, CarBrand::getCarBrandId, CarBrand::getCarBrandName);

            // 处理 汽车类型名称
            Map<?, ?> carCategoryIdToNameMap = TransRelationUtils.getTransMap(carSeriesVoList, CarSeriesVo::getCarCategoryId,
                    CarCategoryMapper.class, CarCategory::getCarCategoryId, CarCategory::getCarCategoryName);

            carSeriesVoList.forEach(item -> {
                item.setCarBrandName((String) carBrandIdToNameMap.get(item.getCarBrandId()));
                item.setCarCategoryName((String) carCategoryIdToNameMap.get(item.getCarCategoryId()));
            });
        }

        return carSeriesVoList;
    }

    private LambdaQueryWrapper<CarSeries> buildQueryWrapper(CarSeriesBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<CarSeries> lqw = Wrappers.lambdaQuery();
        lqw.eq(bo.getParentId() != null, CarSeries::getParentId, bo.getParentId());
        lqw.eq(bo.getCarCategoryId() != null, CarSeries::getCarCategoryId, bo.getCarCategoryId());
        lqw.eq(bo.getCarBrandId() != null, CarSeries::getCarBrandId, bo.getCarBrandId());
        lqw.like(StringUtils.isNotBlank(bo.getCarSeriesName()), CarSeries::getCarSeriesName, bo.getCarSeriesName());
        return lqw;
    }

    /**
     * 新增汽车车系
     */
    @Override
    public Boolean insertByBo(CarSeriesBo bo) {
        CarSeries add = BeanUtil.toBean(bo, CarSeries.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setCarSeriesId(add.getCarSeriesId());
        }
        return flag;
    }

    /**
     * 修改汽车车系
     */
    @Override
    public Boolean updateByBo(CarSeriesBo bo) {
        CarSeries update = BeanUtil.toBean(bo, CarSeries.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(CarSeries entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除汽车车系
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }

    /**
     * 选择车系页面1 对应查询
     *
     */
    @Override
    public List<CarSeriesPickListOneVo> pickListOne(Long carBrandId) {
        List<CarSeriesPickListOneVo> oneVoList = baseMapper.pickListOne(carBrandId);
        oneVoList.forEach(vo -> {
            List<SeriesPickListItemVo> seriesPickList = vo.getSeriesPickList();
            if(CollUtil.isNotEmpty(seriesPickList)){
                vo.setSeriesPickList(TreeUtils.build(seriesPickList, "carSeriesId"));
            }
        });

        return oneVoList;
    }
}
