package com.ruoyi.business.framework.car.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;



/**
 * 汽车型号视图对象 car_model
 *
 * @author Gangbb
 * @date 2023-04-03
 */
@Data
@ExcelIgnoreUnannotated
public class CarModelVo {

    private static final long serialVersionUID = 1L;

    /**
     * 汽车型号id
     */
    @ExcelProperty(value = "汽车型号id")
    private Long carModelId;

    /**
     * 父id;(顶层为0)
     */
    @ExcelProperty(value = "父id;(顶层为0)")
    private Long parentId;

    /**
     * 关联汽车品牌id;(car_brand.id)
     */
    @ExcelProperty(value = "关联汽车品牌id;(car_brand.id)")
    private Long carBrandId;

    /**
     * 关联汽车车系id;(car_series.id)
     */
    @ExcelProperty(value = "关联汽车车系id;(car_series.id)")
    private Long carSeriesId;

    /**
     * 关联汽车车系名称
     */
    @ExcelProperty(value = "关联汽车车系名称")
    private String carSeriesName;

    /**
     * 汽车型号名称
     */
    @ExcelProperty(value = "汽车型号名称")
    private String carModelName;

    /**
     * 汽车型号指导价
     */
    @ExcelProperty(value = "汽车型号指导价")
    private BigDecimal carModelGuidePrice;

    /**
     * 汽车型号发布时间
     */
    @ExcelProperty(value = "汽车型号发布时间")
    private Date carModelReleaseTime;

    /**
     * 可选颜色(多个逗号隔开)
     */
    @ExcelProperty(value = "可选颜色(多个逗号隔开)")
    private String colorOptional;

    /**
     * 可选排放标准(多个逗号隔开)
     */
    @ExcelProperty(value = "可选排放标准(多个逗号隔开)")
    private String dischargeOptional;

    /**
     * 排序字段
     */
    @ExcelProperty(value = "排序字段")
    private Integer sort;


}
