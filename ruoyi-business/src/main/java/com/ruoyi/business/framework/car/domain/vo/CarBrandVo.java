package com.ruoyi.business.framework.car.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;



/**
 * 汽车品牌视图对象 car_brand
 *
 * @author Gangbb
 * @date 2023-03-25
 */
@Data
@ExcelIgnoreUnannotated
public class CarBrandVo {

    private static final long serialVersionUID = 1L;

    /**
     * 汽车品牌id
     */
    @ExcelProperty(value = "汽车品牌id")
    private Long carBrandId;

    /**
     * 汽车品牌名称
     */
    @ExcelProperty(value = "汽车品牌名称")
    private String carBrandName;

    /**
     * 汽车品牌logo
     */
    @ExcelProperty(value = "汽车品牌logo")
    private String carBrandLogo;

    /**
     * 汽车品牌介绍
     */
    @ExcelProperty(value = "汽车品牌介绍")
    private String carBrandDescription;

    /**
     * 检索首字母
     */
    @ExcelProperty(value = "检索首字母")
    private String firstLetter;

}
