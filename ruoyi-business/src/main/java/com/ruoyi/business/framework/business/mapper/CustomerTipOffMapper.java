package com.ruoyi.business.framework.business.mapper;

import com.ruoyi.business.framework.business.domain.CustomerTipOff;
import com.ruoyi.business.framework.business.domain.vo.CustomerTipOffVo;
import com.ruoyi.common.core.mapper.BaseMapperPlus;

/**
 * 客户举报Mapper接口
 *
 * @author FYZ
 * @date 2023-04-16
 */
public interface CustomerTipOffMapper extends BaseMapperPlus<CustomerTipOffMapper, CustomerTipOff, CustomerTipOffVo> {

}
