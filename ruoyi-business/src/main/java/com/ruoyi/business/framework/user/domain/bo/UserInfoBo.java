package com.ruoyi.business.framework.user.domain.bo;

import com.ruoyi.common.core.domain.BaseEntity;
import com.ruoyi.common.core.validate.AddGroup;
import com.ruoyi.common.core.validate.EditGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * 用户平台信息业务对象 u_user_info
 *
 * @author Gangbb
 * @date 2023-03-28
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class UserInfoBo extends BaseEntity {

    /**
     * 用户平台信息id
     */
    @NotNull(message = "用户平台信息id不能为空", groups = { EditGroup.class })
    private Long userInfoId;

    /**
     * 用户平台信息id
     */
    @NotNull(message = "用户id不能为空")
    private Long userId;

    /**
     * 账号头像
     */
    private String avatar;

    /**
     * 展示照片url(多个逗号,最多4张)
     */
    private String showImgUrl;

    /**
     * 一句话简介
     */
    private String oneWord;

    /**
     * 登陆手机号
     */
    @NotBlank(message = "登陆手机号不能为空", groups = { AddGroup.class, EditGroup.class })
    private String loginPhone;

    /**
     * 备用手机号
     */
    private String reservePhone;

    /**
     * 个人认证是否通过(0-否 1-是)
     */
    private Integer personalAuthRes;

    /**
     * 用户真实姓名
     */
    @NotBlank(message = "用户真实姓名不能为空", groups = { AddGroup.class, EditGroup.class })
    private String realName;

    /**
     * 身份证号
     */
    private String idCard;

    /**
     * 公司认证是否通过(0-否 1-是)
     */
    private Integer companyAuthRes;

    /**
     * 用户公司名称
     */
    private String companyName;

    /**
     * 所在地区id
     */
    private Long areaId;

    /**
     * 所在地区全称
     */
    private String areaMergeName;

    /**
     * 实际经营地址
     */
    private String operateAddress;

    /**
     * 公司性质(字典：company_property)
     */
    private String companyProperty;

    /**
     * 经营汽车品牌id(多个逗号隔开,最多12个)
     */
    private String operateBrandId;

    /**
     * 经营汽车品牌名称(多个逗号隔开,最多12个)
     */
    private String operateBrandName;

    /**
     * 经营类别(字典：operate_category)
     */
    private Integer operateCategory;

    /**
     * 授信认证是否通过(0-否 1-是)
     */
    private Integer creditAuth;

    /**
     * 授信认证评级id;(u_user_grade.id)
     */
    private Long userGradeId;

    /**
     * 授信有效截止日期
     */
    private Date creditExpiration;


}
