package com.ruoyi.business.framework.business.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.ruoyi.business.framework.business.domain.SourceCollection;
import com.ruoyi.business.framework.business.domain.bo.SourceCollectionBo;
import com.ruoyi.business.framework.business.domain.vo.SourceCollectionVo;
import com.ruoyi.business.framework.business.service.ISourceCollectionService;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.annotation.RepeatSubmit;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.PageQuery;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.core.validate.AddGroup;
import com.ruoyi.common.core.validate.EditGroup;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Arrays;
import java.util.List;

/**
 * 车源收藏
 *
 * @author Gangbb
 * @date 2023-04-15
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/business/sourceCollection")
public class SourceCollectionController extends BaseController {

    private final ISourceCollectionService iSourceCollectionService;

    /**
     * 查询车源收藏列表
     */
    @SaCheckPermission("business:sourceCollection:list")
    @GetMapping("/list")
    public TableDataInfo<SourceCollectionVo> list(SourceCollectionBo bo, PageQuery pageQuery) {
        return iSourceCollectionService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出车源收藏列表
     */
    @SaCheckPermission("business:sourceCollection:export")
    @Log(title = "车源收藏", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(SourceCollectionBo bo, HttpServletResponse response) {
        List<SourceCollectionVo> list = iSourceCollectionService.queryList(bo);
        ExcelUtil.exportExcel(list, "车源收藏", SourceCollectionVo.class, response);
    }

    /**
     * 获取车源收藏详细信息
     *
     * @param sourceCollectionId 主键
     */
    @SaCheckPermission("business:sourceCollection:query")
    @GetMapping("/{sourceCollectionId}")
    public R<SourceCollection> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long sourceCollectionId) {
        return R.ok(iSourceCollectionService.queryById(sourceCollectionId));
    }

    /**
     * 新增车源收藏
     */
    @SaCheckPermission("business:sourceCollection:add")
    @Log(title = "车源收藏", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody SourceCollectionBo bo) {
        return toAjax(iSourceCollectionService.insertByBo(bo));
    }

    /**
     * 修改车源收藏
     */
    @SaCheckPermission("business:sourceCollection:edit")
    @Log(title = "车源收藏", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody SourceCollectionBo bo) {
        return toAjax(iSourceCollectionService.updateByBo(bo));
    }

    /**
     * 删除车源收藏
     *
     * @param sourceCollectionIds 主键串
     */
    @SaCheckPermission("business:sourceCollection:remove")
    @Log(title = "车源收藏", businessType = BusinessType.DELETE)
    @DeleteMapping("/{sourceCollectionIds}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] sourceCollectionIds) {
        return toAjax(iSourceCollectionService.deleteWithValidByIds(Arrays.asList(sourceCollectionIds), true));
    }
}
