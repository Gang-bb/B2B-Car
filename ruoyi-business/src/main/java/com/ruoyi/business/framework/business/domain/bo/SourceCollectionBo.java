package com.ruoyi.business.framework.business.domain.bo;

import com.ruoyi.common.core.domain.BaseEntity;
import com.ruoyi.common.core.validate.AddGroup;
import com.ruoyi.common.core.validate.EditGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;

/**
 * 车源收藏业务对象 b_source_collection
 *
 * @author Gangbb
 * @date 2023-04-15
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class SourceCollectionBo extends BaseEntity {

    /**
     * 车源收藏id
     */
    @NotNull(message = "车源收藏id不能为空", groups = { EditGroup.class })
    private Long sourceCollectionId;

    /**
     * 关联用户id;(sys_user.id)
     */
    @NotNull(message = "关联用户id;(sys_user.id)不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long userId;

    /**
     * 关联车源id;(b_car_source.id)
     */
    @NotNull(message = "关联车源id;(b_car_source.id)不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long carSourceId;


}
