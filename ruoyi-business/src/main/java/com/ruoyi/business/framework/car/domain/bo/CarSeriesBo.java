package com.ruoyi.business.framework.car.domain.bo;

import com.ruoyi.common.core.domain.TreeEntity;
import com.ruoyi.common.core.validate.AddGroup;
import com.ruoyi.common.core.validate.EditGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * 汽车车系业务对象 car_series
 *
 * @author Gangbb
 * @date 2023-04-04
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class CarSeriesBo extends TreeEntity<CarSeriesBo> {

    /**
     * 汽车车系id
     */
    @NotNull(message = "汽车车系id不能为空", groups = { EditGroup.class })
    private Long carSeriesId;

    /**
     * 关联汽车类型id(car_category.id)
     */
    @NotNull(message = "关联汽车类型id(car_category.id)不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long carCategoryId;

    /**
     * 关联汽车品牌id;(car_brand.id)
     */
    @NotNull(message = "关联汽车品牌id;(car_brand.id)不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long carBrandId;

    /**
     * 汽车车系名称
     */
    @NotBlank(message = "汽车车系名称不能为空", groups = { AddGroup.class, EditGroup.class })
    private String carSeriesName;

    /**
     * 排序字段
     */
    private Integer sort;

    /**
     * 状态(0-正常 2-停用)
     */
    private String status;


}
