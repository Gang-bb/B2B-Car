package com.ruoyi.business.framework.business.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;



/**
 * 通话记录视图对象 b_call_history
 *
 * @author FYZ
 * @date 2023-04-22
 */
@Data
@ExcelIgnoreUnannotated
public class CallHistoryVo {

    private static final long serialVersionUID = 1L;

    /**
     * 通话记录id
     */
    @ExcelProperty(value = "通话记录id")
    private Long callHistoryId;

    /**
     * 关联用户id;(sys_user.id)
     */
    @ExcelProperty(value = "关联用户id;(sys_user.id)")
    private Long userId;

    /**
     * 关联车源id;(car_source.id)
     */
    @ExcelProperty(value = "关联车源id;(car_source.id)")
    private Long carSourceId;

    /**
     * 来电用户id;(sys_user.id)
     */
    @ExcelProperty(value = "来电用户id;(sys_user.id)")
    private Long callerUserId;

    /**
     * 通话标志;(字典：call_flag；1:联系我的，2:我联系的)
     */
    @ExcelProperty(value = "通话标志;(字典：call_flag；1:联系我的，2:我联系的)")
    private String callFlag;

    /**
     * 通话状态;(字典：call_status；1:未接通，2:已接通)
     */
    @ExcelProperty(value = "通话状态;(字典：call_status；1:未接通，2:已接通)")
    private String callStatus;


}
