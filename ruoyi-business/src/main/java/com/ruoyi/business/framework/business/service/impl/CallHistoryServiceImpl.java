package com.ruoyi.business.framework.business.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.ruoyi.business.app.business.domain.bo.CallHistoryAppBo;
import com.ruoyi.business.app.business.domain.vo.CallHistoryAppVo;
import com.ruoyi.business.app.car.domain.bo.CarSourceAppBo;
import com.ruoyi.business.app.car.domain.vo.CarSourceAppVo;
import com.ruoyi.business.framework.car.domain.CarSource;
import com.ruoyi.business.framework.car.service.ICarSourceService;
import com.ruoyi.business.framework.user.domain.UserInfo;
import com.ruoyi.business.framework.user.service.IUserInfoService;
import com.ruoyi.common.helper.LoginHelper;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.core.domain.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import com.ruoyi.business.framework.business.domain.bo.CallHistoryBo;
import com.ruoyi.business.framework.business.domain.vo.CallHistoryVo;
import com.ruoyi.business.framework.business.domain.CallHistory;
import com.ruoyi.business.framework.business.mapper.CallHistoryMapper;
import com.ruoyi.business.framework.business.service.ICallHistoryService;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 通话记录Service业务层处理
 *
 * @author FYZ
 * @date 2023-04-16
 */
@RequiredArgsConstructor
@Service
public class CallHistoryServiceImpl implements ICallHistoryService {

    private final CallHistoryMapper baseMapper;
    private final ICarSourceService iCarSourceService;

    private final IUserInfoService iUserInfoService;

    /**
     * 查询通话记录
     */
    @Override
    public CallHistory queryById(Long callHistoryId){
        return baseMapper.selectById(callHistoryId);
    }

    /**
     * 查询通话记录列表
     */
    @Override
    public TableDataInfo<CallHistoryVo> queryPageList(CallHistoryBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<CallHistory> lqw = buildQueryWrapper(bo);
        Page<CallHistoryVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询通话记录列表
     */
    @Override
    public List<CallHistoryVo> queryList(CallHistoryBo bo) {
        LambdaQueryWrapper<CallHistory> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<CallHistory> buildQueryWrapper(CallHistoryBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<CallHistory> lqw = Wrappers.lambdaQuery();
        lqw.eq(bo.getUserId() != null, CallHistory::getUserId, bo.getUserId());
        lqw.eq(bo.getCarSourceId() != null, CallHistory::getCarSourceId, bo.getCarSourceId());
        lqw.eq(StringUtils.isNotBlank(bo.getCallFlag()), CallHistory::getCallFlag, bo.getCallFlag());
        lqw.orderByDesc(CallHistory::getUpdateTime);
        return lqw;
    }

    /**
     * 新增通话记录
     */
    @Override
    public Boolean insertByBo(CallHistoryBo bo) {
        CallHistory add = BeanUtil.toBean(bo, CallHistory.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setCallHistoryId(add.getCallHistoryId());
        }
        return flag;
    }

    /**
     * 新增或更新通话记录
     */
    public Boolean insertOrUpdate(CallHistoryBo bo) {
        CallHistory add = BeanUtil.toBean(bo, CallHistory.class);
        LambdaQueryWrapper<CallHistory> lqw = Wrappers.lambdaQuery();
        lqw.eq(bo.getUserId() != null, CallHistory::getUserId, bo.getUserId());
        lqw.eq(bo.getCarSourceId() != null, CallHistory::getCarSourceId, bo.getCarSourceId());
        CallHistoryVo callHistoryVo = baseMapper.selectVoOne(lqw);

        boolean flag = false;
        if (callHistoryVo != null) {
            CallHistory update = BeanUtil.toBean(callHistoryVo, CallHistory.class);
            flag = baseMapper.updateById(update) > 0;
        } else {
            flag = baseMapper.insert(add) > 0;
        }
        return flag;
    }

    /**
     * 修改通话记录
     */
    @Override
    public Boolean updateByBo(CallHistoryBo bo) {
        CallHistory update = BeanUtil.toBean(bo, CallHistory.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(CallHistory entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除通话记录
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }

    /**
     * app 查询通话记录列表
     *
     * @param callFlag 通话标志 1:联系我的，2:我联系的
     */
    @Override
    public TableDataInfo<CallHistoryAppVo> appList(String callFlag, PageQuery pageQuery) {
        // 根据当前登录用户id 获取通话记录列表
        Long userId = LoginHelper.getUserId();
        CallHistoryBo callHistoryBo = new CallHistoryBo();
        callHistoryBo.setUserId(userId);
        callHistoryBo.setCallFlag(callFlag);
        List<CallHistoryVo> callHistoryVoList = queryList(callHistoryBo);

        // 获取车源id 集合，集合数据量可能会过大，导致 select in() sql 报错
        List<Long> onlyNeedIdList = new ArrayList<>();
        callHistoryVoList.forEach((item) -> {
            onlyNeedIdList.add(item.getCarSourceId());
        });

        // 根据车源id查询车源
        CarSourceAppBo carSourceAppBo = new CarSourceAppBo();
        carSourceAppBo.setOnlyNeedIdList(onlyNeedIdList);
        TableDataInfo<CarSourceAppVo> csTableDataInfo = iCarSourceService.appList(carSourceAppBo, pageQuery);

        // 加上通话记录id
        List<CallHistoryAppVo> appVoList = new ArrayList<>();
        for (CarSourceAppVo vo : csTableDataInfo.getRows()) {
            callHistoryVoList.forEach((item) -> {
                if (vo.getCarSourceId() == item.getCarSourceId()) {
                    CallHistoryAppVo appVo = BeanUtil.toBean(vo, CallHistoryAppVo.class);

                    if ("1".equals(callFlag)) {//通话标志 1:联系我的  来电用户id
                        UserInfo userInfo = iUserInfoService.queryById(item.getCallerUserId());
                        if (userInfo != null) {
                            appVo.setPhoneNumber(userInfo.getLoginPhone());
                        }

                    } else if ("2".equals(callFlag)) {//通话标志 2:我联系的  车源关联用户id
                        UserInfo userInfo = iUserInfoService.queryById(vo.getUserId());
                        if (userInfo != null) {
                            appVo.setPhoneNumber(userInfo.getLoginPhone());
                        }
                    }

                    appVo.setCallHistoryId(item.getCallHistoryId());
                    appVoList.add(appVo);
                }
            });
        }

        // TODO 排序还有点问题，从 iCarSourceService.appList() 查询回来的数据 和
        //  从 车源收藏表查询回来的数据排序不一样

        return TableDataInfo.build(appVoList);
    }

    /**
     * app 新增通话记录（一次新增两条数据，来电和去电）
     */
    @Override
    public Boolean insertBycCarSourceId(CallHistoryAppBo bo) {
        Long userId = LoginHelper.getUserId();
        // 新增 我联系的（去电） 通话记录（车源绑定的关联用户）
        CallHistoryBo callHistoryBo = new CallHistoryBo();
        callHistoryBo.setCarSourceId(bo.getCarSourceId());
        callHistoryBo.setCallFlag("2");// 通话标志 1:联系我的，2:我联系的
        callHistoryBo.setUserId(userId);
        insertOrUpdate(callHistoryBo);

        // 新增 联系我的（来电） 通话记录（来电用户）
        CallHistoryBo callHistoryBo2 = new CallHistoryBo();
        callHistoryBo2.setCarSourceId(bo.getCarSourceId());
        callHistoryBo2.setCallFlag("1");// 通话标志 1:联系我的，2:我联系的
        // 查询车源
        CarSource carSource = iCarSourceService.queryById(bo.getCarSourceId());
        // 获取车源绑定的关联用户id
        callHistoryBo2.setUserId(carSource.getUserId());
        callHistoryBo2.setCallerUserId(userId);

        return insertOrUpdate(callHistoryBo2);
    }

    /**
     * app 根据车源id删除通话记录信息
     */
    @Override
    public Boolean deleteByCarSourceId(Long carSourceId) {
        Long userId = LoginHelper.getUserId();
        LambdaQueryWrapper<CallHistory> lqw = Wrappers.lambdaQuery();
        lqw.eq(userId != null, CallHistory::getUserId, userId);
        lqw.eq(carSourceId != null, CallHistory::getCarSourceId, carSourceId);

        return baseMapper.delete(lqw) > 0;
    }

    /**
     * app 根据用户id删除所有相关通话记录信息
     */
    @Override
    public Boolean deleteAllByUserId() {
        Long userId = LoginHelper.getUserId();
        LambdaQueryWrapper<CallHistory> lqw = Wrappers.lambdaQuery();
        lqw.eq(userId != null, CallHistory::getUserId, userId);

        Long count = baseMapper.selectCount(lqw);
        if (count == 0) {
            return true;
        }
        return baseMapper.delete(lqw) > 0;
    }
}
