package com.ruoyi.business.framework.business.mapper;

import com.ruoyi.business.framework.business.domain.CallHistory;
import com.ruoyi.business.framework.business.domain.vo.CallHistoryVo;
import com.ruoyi.common.core.mapper.BaseMapperPlus;

/**
 * 通话记录Mapper接口
 *
 * @author FYZ
 * @date 2023-04-16
 */
public interface CallHistoryMapper extends BaseMapperPlus<CallHistoryMapper, CallHistory, CallHistoryVo> {

}
