package com.ruoyi.business.framework.car.domain.bo;

import com.ruoyi.common.core.domain.BaseEntity;
import com.ruoyi.common.core.validate.AddGroup;
import com.ruoyi.common.core.validate.EditGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 汽车型号业务对象 car_model
 *
 * @author Gangbb
 * @date 2023-04-03
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class CarModelBo extends BaseEntity {

    /**
     * 汽车型号id
     */
    @NotNull(message = "汽车型号id不能为空", groups = { EditGroup.class })
    private Long carModelId;

    /**
     * 父id;(顶层为0)
     */
    @NotNull(message = "父id;(顶层为0)不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long parentId;

    /**
     * 关联汽车品牌id;(car_brand.id)
     */
    @NotNull(message = "关联汽车品牌id;(car_brand.id)不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long carBrandId;

    /**
     * 关联汽车车系id;(car_series.id)
     */
    @NotNull(message = "关联汽车车系id;(car_series.id)不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long carSeriesId;

    /**
     * 关联汽车车系名称
     */
    @NotBlank(message = "关联汽车车系名称不能为空", groups = { AddGroup.class, EditGroup.class })
    private String carSeriesName;

    /**
     * 汽车型号名称
     */
    @NotBlank(message = "汽车型号名称不能为空", groups = { AddGroup.class, EditGroup.class })
    private String carModelName;

    /**
     * 汽车型号指导价
     */
    @NotNull(message = "汽车型号指导价不能为空", groups = { AddGroup.class, EditGroup.class })
    private BigDecimal carModelGuidePrice;

    /**
     * 汽车型号发布时间
     */
    private Date carModelReleaseTime;

    /**
     * 可选颜色(多个逗号隔开)
     */
    private String colorOptional;

    /**
     * 可选排放标准(多个逗号隔开)
     */
    private String dischargeOptional;

    /**
     * 排序字段
     */
    private Integer sort;


}
