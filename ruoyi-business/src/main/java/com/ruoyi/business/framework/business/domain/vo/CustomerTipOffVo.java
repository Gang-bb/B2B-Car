package com.ruoyi.business.framework.business.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;



/**
 * 客户举报视图对象 b_customer_tip_off
 *
 * @author FYZ
 * @date 2023-04-23
 */
@Data
@ExcelIgnoreUnannotated
public class CustomerTipOffVo {

    private static final long serialVersionUID = 1L;

    /**
     * 客户举报id
     */
    @ExcelProperty(value = "客户举报id")
    private Long customerTipOffId;

    /**
     * 关联用户id;(sys_user.id)
     */
    @ExcelProperty(value = "关联用户id;(sys_user.id)")
    private Long userId;

    /**
     * 关联车源id;(car_source.id)
     */
    @ExcelProperty(value = "关联车源id;(car_source.id)")
    private Long carSourceId;

    /**
     * 举报人id;(sys_user.id)
     */
    @ExcelProperty(value = "举报人id;(sys_user.id)")
    private Long tipOffUserId;

    /**
     * 举报类型;(字典：tip_off_type)
     */
    @ExcelProperty(value = "举报类型;(字典：tip_off_type)")
    private String tipOffType;

    /**
     * 举报内容
     */
    @ExcelProperty(value = "举报内容")
    private String tipOffContent;


}
