package com.ruoyi.business.framework.business.mapper;

import com.ruoyi.business.framework.business.domain.CarHistory;
import com.ruoyi.business.framework.business.domain.vo.CarHistoryVo;
import com.ruoyi.common.core.mapper.BaseMapperPlus;

/**
 * 车源浏览足迹Mapper接口
 *
 * @author FYZ
 * @date 2023-04-16
 */
public interface CarHistoryMapper extends BaseMapperPlus<CarHistoryMapper, CarHistory, CarHistoryVo> {

}
