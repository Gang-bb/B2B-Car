package com.ruoyi.business.framework.car.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ruoyi.business.app.car.domain.bo.CarSourceAppBo;
import com.ruoyi.business.app.car.domain.vo.CarSourceAppVo;
import com.ruoyi.business.framework.car.domain.CarCategory;
import com.ruoyi.business.framework.car.domain.CarSource;
import com.ruoyi.business.framework.car.domain.bo.CarSourceBo;
import com.ruoyi.business.framework.car.domain.vo.CarSourceVo;
import com.ruoyi.business.framework.car.enums.SourceSortType;
import com.ruoyi.business.framework.car.mapper.CarCategoryMapper;
import com.ruoyi.business.framework.car.mapper.CarSourceMapper;
import com.ruoyi.business.framework.car.service.ICarSourceService;
import com.ruoyi.business.framework.user.domain.UserInfo;
import com.ruoyi.business.framework.user.mapper.UserInfoMapper;
import com.ruoyi.common.core.domain.PageQuery;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.TransRelationUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * 车源Service业务层处理
 *
 * @author Gangbb
 * @date 2023-04-04
 */
@RequiredArgsConstructor
@Service
public class CarSourceServiceImpl implements ICarSourceService {

    private final CarSourceMapper baseMapper;

    /**
     * 查询车源
     */
    @Override
    public CarSource queryById(Long carSourceId){
        return baseMapper.selectById(carSourceId);
    }

    /**
     * 查询车源列表
     */
    @Override
    public TableDataInfo<CarSourceVo> queryPageList(CarSourceBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<CarSource> lqw = buildQueryWrapper(bo);
        Page<CarSourceVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询车源列表
     */
    @Override
    public List<CarSourceVo> queryList(CarSourceBo bo) {
        LambdaQueryWrapper<CarSource> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<CarSource> buildQueryWrapper(CarSourceBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<CarSource> lqw = Wrappers.lambdaQuery();
        lqw.eq(bo.getCarCategoryId() != null, CarSource::getCarCategoryId, bo.getCarCategoryId());
        lqw.eq(bo.getCarBrandId() != null, CarSource::getCarBrandId, bo.getCarBrandId());
        lqw.eq(bo.getCarSeriesId() != null, CarSource::getCarSeriesId, bo.getCarSeriesId());
        lqw.eq(bo.getCarModelId() != null, CarSource::getCarModelId, bo.getCarModelId());
        lqw.like(StringUtils.isNotBlank(bo.getCarSourceInfo()), CarSource::getCarSourceInfo, bo.getCarSourceInfo());
        lqw.like(StringUtils.isNotBlank(bo.getSourceLocation()), CarSource::getSourceLocation, bo.getSourceLocation());
        lqw.like(StringUtils.isNotBlank(bo.getVehicleFrame()), CarSource::getVehicleFrame, bo.getVehicleFrame());
        lqw.eq(bo.getIsSelfSupport() != null, CarSource::getIsSelfSupport, bo.getIsSelfSupport());
        return lqw;
    }

    /**
     * 新增车源
     */
    @Override
    public Boolean insertByBo(CarSourceBo bo) {
        CarSource add = BeanUtil.toBean(bo, CarSource.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setCarSourceId(add.getCarSourceId());
        }
        return flag;
    }

    /**
     * 修改车源
     */
    @Override
    public Boolean updateByBo(CarSourceBo bo) {
        CarSource update = BeanUtil.toBean(bo, CarSource.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(CarSource entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除车源
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }

    /**
     * app查询车源列表
     *
     */
    @Override
    public TableDataInfo<CarSourceAppVo> appList(CarSourceAppBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<CarSource> lqw = getAppVoLqw(bo);
        IPage<CarSourceAppVo> voPage = baseMapper.selectVoPage(pageQuery.build(), lqw, CarSourceAppVo.class);
        // 处理字段
        List<CarSourceAppVo> records = voPage.getRecords();
        if(CollUtil.isNotEmpty(records)){
            Map<?, ?> transCategoryMap = TransRelationUtils.getTransMap(records, CarSourceAppVo::getCarCategoryId,
                    CarCategoryMapper.class, CarCategory::getCarCategoryId, CarCategory::getCarCategoryName);
            Map<?, UserInfo> transUserMapObj = TransRelationUtils.getTransMapObj(records, CarSourceAppVo::getUserId,
                    UserInfoMapper.class, UserInfo::getUserId);
            for (CarSourceAppVo vo : records) {
                // 车源是否有图片
                int isHasImg = StringUtils.isNotBlank(vo.getSourceImg())? 1:0;
                vo.setIsHasImg(isHasImg);
                // 关联汽车类型名称
                vo.setCarCategoryName((String) transCategoryMap.get(vo.getCarCategoryId()));
                // 发布人员姓名(所在省)
                UserInfo userInfo = transUserMapObj.get(vo.getUserId());
                StringBuilder stringBuilder = new StringBuilder(userInfo.getRealName());
                String areaMergeName = userInfo.getAreaMergeName();
                if(StringUtils.isNotBlank(areaMergeName)){
                    List<String> areaStrings = StringUtils.splitList(",");
                    stringBuilder.append("(").append(areaStrings.get(0)).append(")");
                }
                vo.setPublishUserName(stringBuilder.toString());
            }
        }

        return TableDataInfo.build(voPage);
    }


    private LambdaQueryWrapper<CarSource> getAppVoLqw(CarSourceAppBo bo) {
        LambdaQueryWrapper<CarSource> lqw = Wrappers.lambdaQuery();
        lqw.eq(bo.getCarCategoryId() != null, CarSource::getCarCategoryId, bo.getCarCategoryId());
        // todo 处理外观其他颜色
        lqw.like(StringUtils.isNotBlank(bo.getAppearanceColor()), CarSource::getAppearanceColor, bo.getAppearanceColor());
        lqw.like(StringUtils.isNotBlank(bo.getProvince()), CarSource::getSourceAreaMergeName, bo.getProvince());
        lqw.ge(ObjectUtil.isNotEmpty(bo.getLowestPrice()), CarSource::getFinalPrice, bo.getLowestPrice());
        lqw.le(ObjectUtil.isNotEmpty(bo.getHighestPrice()), CarSource::getFinalPrice, bo.getHighestPrice());
        lqw.eq(StringUtils.isNotBlank(bo.getSourceDischarge()), CarSource::getSourceDischarge, bo.getSourceDischarge());
        lqw.eq(StringUtils.isNotBlank(bo.getSourceDelivery()), CarSource::getSourceDelivery, bo.getSourceDelivery());
        lqw.in(CollUtil.isNotEmpty(bo.getOnlyNeedIdList()), CarSource::getCarSourceId, bo.getOnlyNeedIdList());

        // 处理排序
        String sourceSortType = bo.getSourceSortType();
        if(ObjectUtil.isNotEmpty(sourceSortType)){
            if(sourceSortType.equals(SourceSortType.PRICE_ASC.getCode())){
                lqw.orderByAsc(CarSource::getFinalPrice);
            }else if(sourceSortType.equals(SourceSortType.PRICE_DESC.getCode())){
                lqw.orderByDesc(CarSource::getFinalPrice);
            }else {
                lqw.orderByDesc(CarSource::getCreateTime);
            }
        }
        return lqw;
    }
}
