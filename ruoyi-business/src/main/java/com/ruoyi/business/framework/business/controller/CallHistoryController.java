package com.ruoyi.business.framework.business.controller;

import java.util.List;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import lombok.RequiredArgsConstructor;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.*;
import cn.dev33.satoken.annotation.SaCheckPermission;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import com.ruoyi.common.annotation.RepeatSubmit;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.PageQuery;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.core.validate.AddGroup;
import com.ruoyi.common.core.validate.EditGroup;
import com.ruoyi.common.core.validate.QueryGroup;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.business.framework.business.domain.CallHistory;
import com.ruoyi.business.framework.business.domain.vo.CallHistoryVo;
import com.ruoyi.business.framework.business.domain.bo.CallHistoryBo;
import com.ruoyi.business.framework.business.service.ICallHistoryService;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 通话记录
 *
 * @author FYZ
 * @date 2023-04-16
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/business/callHistory")
public class CallHistoryController extends BaseController {

    private final ICallHistoryService iCallHistoryService;

    /**
     * 查询通话记录列表
     */
    @SaCheckPermission("business:callHistory:list")
    @GetMapping("/list")
    public TableDataInfo<CallHistoryVo> list(CallHistoryBo bo, PageQuery pageQuery) {
        return iCallHistoryService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出通话记录列表
     */
    @SaCheckPermission("business:callHistory:export")
    @Log(title = "通话记录", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(CallHistoryBo bo, HttpServletResponse response) {
        List<CallHistoryVo> list = iCallHistoryService.queryList(bo);
        ExcelUtil.exportExcel(list, "通话记录", CallHistoryVo.class, response);
    }

    /**
     * 获取通话记录详细信息
     *
     * @param callHistoryId 主键
     */
    @SaCheckPermission("business:callHistory:query")
    @GetMapping("/{callHistoryId}")
    public R<CallHistory> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long callHistoryId) {
        return R.ok(iCallHistoryService.queryById(callHistoryId));
    }

    /**
     * 新增通话记录
     */
    @SaCheckPermission("business:callHistory:add")
    @Log(title = "通话记录", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody CallHistoryBo bo) {
        return toAjax(iCallHistoryService.insertByBo(bo));
    }

    /**
     * 修改通话记录
     */
    @SaCheckPermission("business:callHistory:edit")
    @Log(title = "通话记录", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody CallHistoryBo bo) {
        return toAjax(iCallHistoryService.updateByBo(bo));
    }

    /**
     * 删除通话记录
     *
     * @param callHistoryIds 主键串
     */
    @SaCheckPermission("business:callHistory:remove")
    @Log(title = "通话记录", businessType = BusinessType.DELETE)
    @DeleteMapping("/{callHistoryIds}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] callHistoryIds) {
        return toAjax(iCallHistoryService.deleteWithValidByIds(Arrays.asList(callHistoryIds), true));
    }
}
