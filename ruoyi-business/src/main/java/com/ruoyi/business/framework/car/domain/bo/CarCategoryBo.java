package com.ruoyi.business.framework.car.domain.bo;

import com.ruoyi.common.core.domain.BaseEntity;
import com.ruoyi.common.core.validate.AddGroup;
import com.ruoyi.common.core.validate.EditGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * 汽车类型业务对象 car_category
 *
 * @author Gangbb
 * @date 2023-04-02
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class CarCategoryBo extends BaseEntity {

    /**
     * 汽车类型id
     */
    @NotNull(message = "汽车类型id不能为空", groups = { EditGroup.class })
    private Long carCategoryId;

    /**
     * 汽车类型名
     */
    @NotBlank(message = "汽车类型名不能为空", groups = { AddGroup.class, EditGroup.class })
    private String carCategoryName;

    /**
     * 经营类别(字典：operate_category)
     */
    @NotBlank(message = "经营类别(字典：operate_category)不能为空", groups = { AddGroup.class, EditGroup.class })
    private String operateCategory;

    /**
     * 排序字段
     */
    @NotNull(message = "排序字段不能为空", groups = { AddGroup.class, EditGroup.class })
    private Integer sort;

    /**
     * 状态(0-正常 2-停用)
     */
    @NotBlank(message = "状态(0-正常 2-停用)不能为空", groups = { AddGroup.class, EditGroup.class })
    private String status;


}
