package com.ruoyi.business.framework.user.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.ruoyi.business.framework.user.domain.UserInfo;
import com.ruoyi.business.framework.user.domain.bo.UserInfoBo;
import com.ruoyi.business.framework.user.domain.vo.UserInfoVo;
import com.ruoyi.business.framework.user.service.IUserInfoService;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.annotation.RepeatSubmit;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.PageQuery;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.core.validate.AddGroup;
import com.ruoyi.common.core.validate.EditGroup;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Arrays;
import java.util.List;

/**
 * 用户平台信息
 *
 * @author Gangbb
 * @date 2023-03-28
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/user/userInfo")
public class UserInfoController extends BaseController {

    private final IUserInfoService iUserInfoService;

    /**
     * 查询用户平台信息列表
     */
    @SaCheckPermission("user:userInfo:list")
    @GetMapping("/list")
    public TableDataInfo<UserInfoVo> list(UserInfoBo bo, PageQuery pageQuery) {
        return iUserInfoService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出用户平台信息列表
     */
    @SaCheckPermission("user:userInfo:export")
    @Log(title = "用户平台信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(UserInfoBo bo, HttpServletResponse response) {
        List<UserInfoVo> list = iUserInfoService.queryList(bo);
        ExcelUtil.exportExcel(list, "用户平台信息", UserInfoVo.class, response);
    }

    /**
     * 获取用户平台信息详细信息
     *
     * @param userInfoId 主键
     */
    @SaCheckPermission("user:userInfo:query")
    @GetMapping("/{userInfoId}")
    public R<UserInfo> getInfo(@NotNull(message = "主键不能为空")
                               @PathVariable Long userInfoId) {
        return R.ok(iUserInfoService.queryById(userInfoId));
    }

    /**
     * 新增用户平台信息
     */
    @SaCheckPermission("user:userInfo:add")
    @Log(title = "用户平台信息", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody UserInfoBo bo) {
        return toAjax(iUserInfoService.insertByBo(bo));
    }

    /**
     * 修改用户平台信息
     */
    @SaCheckPermission("user:userInfo:edit")
    @Log(title = "用户平台信息", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody UserInfoBo bo) {
        return toAjax(iUserInfoService.updateByBo(bo));
    }

    /**
     * 删除用户平台信息
     *
     * @param userInfoIds 主键串
     */
    @SaCheckPermission("user:userInfo:remove")
    @Log(title = "用户平台信息", businessType = BusinessType.DELETE)
    @DeleteMapping("/{userInfoIds}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] userInfoIds) {
        return toAjax(iUserInfoService.deleteWithValidByIds(Arrays.asList(userInfoIds), true));
    }
}
