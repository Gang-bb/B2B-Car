package com.ruoyi.business.framework.car.service;

import com.ruoyi.business.framework.car.domain.CarModel;
import com.ruoyi.business.framework.car.domain.vo.CarModelVo;
import com.ruoyi.business.framework.car.domain.bo.CarModelBo;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.core.domain.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 汽车型号Service接口
 *
 * @author Gangbb
 * @date 2023-04-03
 */
public interface ICarModelService {

    /**
     * 查询汽车型号
     */
    CarModel queryById(Long carModelId);

    /**
     * 查询汽车型号列表
     */
    TableDataInfo<CarModelVo> queryPageList(CarModelBo bo, PageQuery pageQuery);

    /**
     * 查询汽车型号列表
     */
    List<CarModelVo> queryList(CarModelBo bo);

    /**
     * 新增汽车型号
     */
    Boolean insertByBo(CarModelBo bo);

    /**
     * 修改汽车型号
     */
    Boolean updateByBo(CarModelBo bo);

    /**
     * 校验并批量删除汽车型号信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
