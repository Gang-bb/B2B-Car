package com.ruoyi.business.framework.business.domain.bo;

import com.ruoyi.common.core.validate.AddGroup;
import com.ruoyi.common.core.validate.EditGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;
import javax.validation.constraints.*;

import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 通话记录业务对象 b_call_history
 *
 * @author FYZ
 * @date 2023-04-22
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class CallHistoryBo extends BaseEntity {

    /**
     * 通话记录id
     */
    @NotNull(message = "通话记录id不能为空", groups = { EditGroup.class })
    private Long callHistoryId;

    /**
     * 关联用户id;(sys_user.id)
     */
    @NotNull(message = "关联用户id;(sys_user.id)不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long userId;

    /**
     * 关联车源id;(car_source.id)
     */
    @NotNull(message = "关联车源id;(car_source.id)不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long carSourceId;

    /**
     * 来电用户id;(sys_user.id)
     */
    @NotNull(message = "来电用户id;(sys_user.id)不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long callerUserId;

    /**
     * 通话标志;(字典：call_flag；1:联系我的，2:我联系的)
     */
    @NotBlank(message = "通话标志;(字典：call_flag；1:联系我的，2:我联系的)不能为空", groups = { AddGroup.class, EditGroup.class })
    private String callFlag;

    /**
     * 通话状态;(字典：call_status；1:未接通，2:已接通)
     */
    @NotBlank(message = "通话状态;(字典：call_status；1:未接通，2:已接通)不能为空", groups = { AddGroup.class, EditGroup.class })
    private String callStatus;


}
