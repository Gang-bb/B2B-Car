package com.ruoyi.business.framework.car.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.extra.pinyin.PinyinUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ruoyi.business.framework.car.domain.CarBrand;
import com.ruoyi.business.framework.car.domain.bo.CarBrandBo;
import com.ruoyi.business.framework.car.domain.vo.CarBrandVo;
import com.ruoyi.business.framework.car.mapper.CarBrandMapper;
import com.ruoyi.business.framework.car.service.ICarBrandService;
import com.ruoyi.common.core.domain.PageQuery;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.utils.StringUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * 汽车品牌Service业务层处理
 *
 * @author Gangbb
 * @date 2023-03-25
 */
@RequiredArgsConstructor
@Service
public class CarBrandServiceImpl implements ICarBrandService {

    private final CarBrandMapper baseMapper;

    /**
     * 查询汽车品牌
     */
    @Override
    public CarBrand queryById(Long carBrandId){
        return baseMapper.selectById(carBrandId);
    }

    /**
     * 查询汽车品牌列表
     */
    @Override
    public TableDataInfo<CarBrandVo> queryPageList(CarBrandBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<CarBrand> lqw = buildQueryWrapper(bo);
        Page<CarBrandVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询汽车品牌列表
     */
    @Override
    public List<CarBrandVo> queryList(CarBrandBo bo) {
        LambdaQueryWrapper<CarBrand> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<CarBrand> buildQueryWrapper(CarBrandBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<CarBrand> lqw = Wrappers.lambdaQuery();
        String firstLetter = bo.getFirstLetter();
        List<String> firstLetterList = CollUtil.newArrayList();
        if(StringUtils.isNotBlank(firstLetter)){
            firstLetterList = StringUtils.splitList(firstLetter, ",");
        }
        lqw.like(StringUtils.isNotBlank(bo.getCarBrandName()), CarBrand::getCarBrandName, bo.getCarBrandName());
        lqw.in(CollUtil.isNotEmpty((firstLetterList)), CarBrand::getFirstLetter, firstLetterList);
        lqw.orderByAsc(CarBrand::getFirstLetter);
        return lqw;
    }

    /**
     * 新增汽车品牌
     */
    @Override
    public Boolean insertByBo(CarBrandBo bo) {
        CarBrand add = BeanUtil.toBean(bo, CarBrand.class);
        handleCarBrandData(add);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setCarBrandId(add.getCarBrandId());
        }
        return flag;
    }

    /**
     * 修改汽车品牌
     */
    @Override
    public Boolean updateByBo(CarBrandBo bo) {
        CarBrand update = BeanUtil.toBean(bo, CarBrand.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 处理品牌数据
     *
     * @param carBrand 品牌实体
     * @date 2023-04-06
     **/
    private void handleCarBrandData(CarBrand carBrand){
        // 识别拼音首字母
        String firstLetter = PinyinUtil.getFirstLetter(carBrand.getCarBrandName(), ",");
        carBrand.setFirstLetter(firstLetter.substring(0,1));
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(CarBrand entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除汽车品牌
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }
}
