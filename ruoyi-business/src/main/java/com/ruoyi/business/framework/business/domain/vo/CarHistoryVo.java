package com.ruoyi.business.framework.business.domain.vo;

import java.util.Date;
import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;



/**
 * 车源浏览足迹视图对象 b_car_history
 *
 * @author FYZ
 * @date 2023-04-16
 */
@Data
@ExcelIgnoreUnannotated
public class CarHistoryVo {

    private static final long serialVersionUID = 1L;

    /**
     * 车源浏览足迹id
     */
    @ExcelProperty(value = "车源浏览足迹id")
    private Long carHistoryId;

    /**
     * 关联用户id;(sys_user.id)
     */
    @ExcelProperty(value = "关联用户id;(sys_user.id)")
    private Long userId;

    /**
     * 关联车源id;(car_source.id)
     */
    @ExcelProperty(value = "关联车源id;(car_source.id)")
    private Long carSourceId;

    /**
     * 浏览时间
     */
    @ExcelProperty(value = "浏览时间")
    private Date readTime;


}
