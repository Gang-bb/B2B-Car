package com.ruoyi.business.framework.car.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.ruoyi.business.framework.car.domain.CarCategory;
import com.ruoyi.business.framework.car.domain.bo.CarCategoryBo;
import com.ruoyi.business.framework.car.domain.vo.CarCategoryVo;
import com.ruoyi.business.framework.car.service.ICarCategoryService;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.annotation.RepeatSubmit;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.PageQuery;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.core.validate.AddGroup;
import com.ruoyi.common.core.validate.EditGroup;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Arrays;
import java.util.List;

/**
 * 汽车类型
 *
 * @author Gangbb
 * @date 2023-04-02
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/car/category")
public class CarCategoryController extends BaseController {

    private final ICarCategoryService iCarCategoryService;

    /**
     * 分页查询汽车类型列表
     */
    @SaCheckPermission("car:category:list")
    @GetMapping("/list")
    public TableDataInfo<CarCategoryVo> list(CarCategoryBo bo, PageQuery pageQuery) {
        return iCarCategoryService.queryPageList(bo, pageQuery);
    }

    /**
     * 查询汽车类型列表
     */
    @SaCheckPermission("car:category:list")
    @GetMapping("/allList")
    public R<List<CarCategoryVo>> allList(CarCategoryBo bo) {
        return R.ok(iCarCategoryService.queryList(bo));
    }

    /**
     * 导出汽车类型列表
     */
    @SaCheckPermission("car:category:export")
    @Log(title = "汽车类型", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(CarCategoryBo bo, HttpServletResponse response) {
        List<CarCategoryVo> list = iCarCategoryService.queryList(bo);
        ExcelUtil.exportExcel(list, "汽车类型", CarCategoryVo.class, response);
    }

    /**
     * 获取汽车类型详细信息
     *
     * @param carCategoryId 主键
     */
    @SaCheckPermission("car:category:query")
    @GetMapping("/{carCategoryId}")
    public R<CarCategory> getInfo(@NotNull(message = "主键不能为空")
                                  @PathVariable Long carCategoryId) {
        return R.ok(iCarCategoryService.queryById(carCategoryId));
    }

    /**
     * 新增汽车类型
     */
    @SaCheckPermission("car:category:add")
    @Log(title = "汽车类型", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody CarCategoryBo bo) {
        return toAjax(iCarCategoryService.insertByBo(bo));
    }

    /**
     * 修改汽车类型
     */
    @SaCheckPermission("car:category:edit")
    @Log(title = "汽车类型", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody CarCategoryBo bo) {
        return toAjax(iCarCategoryService.updateByBo(bo));
    }

    /**
     * 删除汽车类型
     *
     * @param carCategoryIds 主键串
     */
    @SaCheckPermission("car:category:remove")
    @Log(title = "汽车类型", businessType = BusinessType.DELETE)
    @DeleteMapping("/{carCategoryIds}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] carCategoryIds) {
        return toAjax(iCarCategoryService.deleteWithValidByIds(Arrays.asList(carCategoryIds), true));
    }
}
