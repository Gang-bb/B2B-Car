package com.ruoyi.business.framework.car.mapper;

import com.ruoyi.business.framework.car.domain.CarSource;
import com.ruoyi.business.framework.car.domain.vo.CarSourceVo;
import com.ruoyi.common.core.mapper.BaseMapperPlus;

/**
 * 车源Mapper接口
 *
 * @author Gangbb
 * @date 2023-04-04
 */
public interface CarSourceMapper extends BaseMapperPlus<CarSourceMapper, CarSource, CarSourceVo> {

}
