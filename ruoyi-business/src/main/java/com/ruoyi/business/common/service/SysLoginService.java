package com.ruoyi.business.common.service;

import cn.dev33.satoken.exception.NotLoginException;
import cn.dev33.satoken.secure.BCrypt;
import cn.dev33.satoken.stp.StpUtil;
import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.ruoyi.business.framework.user.domain.bo.UserInfoBo;
import com.ruoyi.business.framework.user.service.IUserInfoService;
import com.ruoyi.common.constant.CacheConstants;
import com.ruoyi.common.constant.Constants;
import com.ruoyi.common.core.domain.dto.RoleDTO;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.core.domain.event.LogininforEvent;
import com.ruoyi.common.core.domain.model.*;
import com.ruoyi.common.enums.*;
import com.ruoyi.common.exception.user.CaptchaException;
import com.ruoyi.common.exception.user.CaptchaExpireException;
import com.ruoyi.common.exception.user.UserException;
import com.ruoyi.common.helper.LoginHelper;
import com.ruoyi.common.utils.*;
import com.ruoyi.common.utils.redis.RedisUtils;
import com.ruoyi.common.utils.spring.SpringUtils;
import com.ruoyi.system.mapper.SysUserMapper;
import com.ruoyi.system.service.ISysConfigService;
import com.ruoyi.system.service.ISysUserService;
import com.ruoyi.system.service.SysPermissionService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.time.Duration;
import java.util.List;
import java.util.Random;
import java.util.function.Supplier;

/**
 * 登录校验方法
 *
 * @author Lion Li
 */
@RequiredArgsConstructor
@Slf4j
@Service
public class SysLoginService {

    private final SysUserMapper userMapper;
    private final ISysConfigService configService;
    private final SysPermissionService permissionService;
    private final ISysUserService userService;
    private final IUserInfoService iUserInfoService;

    @Value("${user.password.maxRetryCount}")
    private Integer maxRetryCount;

    @Value("${user.password.lockTime}")
    private Integer lockTime;

    /**
     * 登录验证
     *
     * @param username 用户名
     * @param password 密码
     * @param code     验证码
     * @param uuid     唯一标识
     * @return 结果
     */
    public String login(String username, String password, String code, String uuid) {
        HttpServletRequest request = ServletUtils.getRequest();
        boolean captchaEnabled = configService.selectCaptchaEnabled();
        // 验证码开关
        if (captchaEnabled) {
            validateCaptcha(username, code, uuid, request);
        }
        SysUser user = loadUserByUsername(username);
        checkLogin(LoginType.PASSWORD, username, () -> !BCrypt.checkpw(password, user.getPassword()));
        // 此处可根据登录用户的数据不同 自行创建 loginUser
        LoginUser loginUser = buildLoginUser(user);
        // 生成token
        LoginHelper.loginByDevice(loginUser, DeviceType.PC);

        recordLogininfor(username, Constants.LOGIN_SUCCESS, MessageUtils.message("user.login.success"));
        recordLoginInfo(user.getUserId(), username);
        return StpUtil.getTokenValue();
    }

    /**
     * app短信登录
     *
     * @param smsLoginBody 请求参数
     * @return Map<String,Object>
     * @date 2023-03-28
     **/
    @Transactional(rollbackFor = Exception.class)
    public SmsLoginVo smsLogin(SmsLoginBody smsLoginBody) {
        SmsLoginVo smsLoginVo = new SmsLoginVo();
        String phonenumber = smsLoginBody.getPhonenumber();

        // 1. 校验验证码是否正确
        boolean validateSmsCode = validateSmsCode(phonenumber, smsLoginBody.getSmsCode());
        if(!validateSmsCode){
            throw new UserException("phone.code.error");
        }
        // 2. 用户是否已经完善信息
        boolean isPerfectInfo = false;
        // 通过手机号查找用户
        SysUser user = userMapper.selectUserByPhonenumber(phonenumber);
        // 如果找不到用户说明第一次登录，帮他注册一个账号
        if(ObjectUtil.isEmpty(user)){
            user = registerNewUser(phonenumber);
        }else {
            // 如果有记录要判断 u_user_info表是否有记录，没有就没有完善过信息
            UserInfoAppMineVo appMyInfo = iUserInfoService.getAppMyInfo(user.getUserId());
            if(ObjectUtil.isNotEmpty(appMyInfo)){
                isPerfectInfo = true;
                smsLoginVo.setUserInfoMine(appMyInfo);
            }
        }

        // 3. 创建loginUser
        LoginUser loginUser = buildLoginUser(user);
        // 生成token
        LoginHelper.loginByDevice(loginUser, DeviceType.APP);

        // 4. 记录日志
        recordLogininfor(user.getUserName(), Constants.LOGIN_SUCCESS, MessageUtils.message("user.login.success"));
        recordLoginInfo(user.getUserId(), user.getUserName());

        // 5. 拼装返回参数
        smsLoginVo.setIsPerfectInfo(isPerfectInfo);
        smsLoginVo.setToken(StpUtil.getTokenValue());

        return smsLoginVo;
    }

    /**
     * 手机号登录创建一个用户
     *
     * @param phonenumber 手机号
     * @return SysUser
     * @date 2023-03-28
     **/
    private SysUser registerNewUser(String phonenumber) {
        SysUser newUser = new SysUser();
        newUser.setUserName(phonenumber);
        newUser.setUserType(UserType.APP_USER.getUserType());
        newUser.setNickName("用户" + phonenumber);
        newUser.setPhonenumber(phonenumber);
        // 角色 = app游客
        newUser.setRoleIds(new Long[]{RoleEnum.VISITOR.getRoleId()});
        // 插入数据库
        userService.insertUser(newUser);

        return newUser;
    }

    /**
     * 退出登录
     */
    public void logout() {
        try {
            LoginUser loginUser = LoginHelper.getLoginUser();
            StpUtil.logout();
            recordLogininfor(loginUser.getUsername(), Constants.LOGOUT, MessageUtils.message("user.logout.success"));
        } catch (NotLoginException ignored) {
        }
    }

    /**
     * 记录登录信息
     *
     * @param username 用户名
     * @param status   状态
     * @param message  消息内容
     * @return
     */
    private void recordLogininfor(String username, String status, String message) {
        LogininforEvent logininforEvent = new LogininforEvent();
        logininforEvent.setUsername(username);
        logininforEvent.setStatus(status);
        logininforEvent.setMessage(message);
        logininforEvent.setRequest(ServletUtils.getRequest());
        SpringUtils.context().publishEvent(logininforEvent);
    }

    /**
     * 校验短信验证码
     */
    private boolean validateSmsCode(String phonenumber, String smsCode) {
        String code = RedisUtils.getCacheObject(getSmsCodeKey(phonenumber));
        if (StringUtils.isBlank(code)) {
            recordLogininfor(phonenumber, Constants.LOGIN_FAIL, MessageUtils.message("user.jcaptcha.expire"));
            throw new CaptchaExpireException();
        }
        // todo 测试环境先用123456
        return "123456".equals(smsCode);
        //return code.equals(smsCode);
    }

    /**
     * 校验验证码
     *
     * @param username 用户名
     * @param code     验证码
     * @param uuid     唯一标识
     */
    public void validateCaptcha(String username, String code, String uuid, HttpServletRequest request) {
        String verifyKey = CacheConstants.CAPTCHA_CODE_KEY + StringUtils.defaultString(uuid, "");
        String captcha = RedisUtils.getCacheObject(verifyKey);
        RedisUtils.deleteObject(verifyKey);
        if (captcha == null) {
            recordLogininfor(username, Constants.LOGIN_FAIL, MessageUtils.message("user.jcaptcha.expire"));
            throw new CaptchaExpireException();
        }
        if (!code.equalsIgnoreCase(captcha)) {
            recordLogininfor(username, Constants.LOGIN_FAIL, MessageUtils.message("user.jcaptcha.error"));
            throw new CaptchaException();
        }
    }

    private SysUser loadUserByUsername(String username) {
        SysUser user = userMapper.selectOne(new LambdaQueryWrapper<SysUser>()
            .select(SysUser::getUserName, SysUser::getStatus)
            .eq(SysUser::getUserName, username));
        if (ObjectUtil.isNull(user)) {
            log.info("登录用户：{} 不存在.", username);
            throw new UserException("user.not.exists", username);
        } else if (UserStatus.DISABLE.getCode().equals(user.getStatus())) {
            log.info("登录用户：{} 已被停用.", username);
            throw new UserException("user.blocked", username);
        }
        return userMapper.selectUserByUserName(username);
    }

    private SysUser loadUserByOpenid(String openid) {
        // 使用 openid 查询绑定用户 如未绑定用户 则根据业务自行处理 例如 创建默认用户
        // todo 自行实现 userService.selectUserByOpenid(openid);
        SysUser user = new SysUser();
        if (ObjectUtil.isNull(user)) {
            log.info("登录用户：{} 不存在.", openid);
            // todo 用户不存在 业务逻辑自行实现
        } else if (UserStatus.DISABLE.getCode().equals(user.getStatus())) {
            log.info("登录用户：{} 已被停用.", openid);
            // todo 用户已被停用 业务逻辑自行实现
        }
        return user;
    }

    /**
     * 构建登录用户
     */
    private LoginUser buildLoginUser(SysUser user) {
        LoginUser loginUser = new LoginUser();
        loginUser.setUserId(user.getUserId());
        loginUser.setDeptId(user.getDeptId());
        loginUser.setUsername(user.getUserName());
        loginUser.setUserType(user.getUserType());
        loginUser.setMenuPermission(permissionService.getMenuPermission(user));
        loginUser.setRolePermission(permissionService.getRolePermission(user));
        loginUser.setDeptName(ObjectUtil.isNull(user.getDept()) ? "" : user.getDept().getDeptName());
        List<RoleDTO> roles = BeanUtil.copyToList(user.getRoles(), RoleDTO.class);
        loginUser.setRoles(roles);
        return loginUser;
    }

    /**
     * 记录登录信息
     *
     * @param userId 用户ID
     */
    public void recordLoginInfo(Long userId, String username) {
        SysUser sysUser = new SysUser();
        sysUser.setUserId(userId);
        sysUser.setLoginIp(ServletUtils.getClientIP());
        sysUser.setLoginDate(DateUtils.getNowDate());
        sysUser.setUpdateBy(username);
        userMapper.updateById(sysUser);
    }

    /**
     * 登录校验
     */
    private void checkLogin(LoginType loginType, String username, Supplier<Boolean> supplier) {
        String errorKey = CacheConstants.PWD_ERR_CNT_KEY + username;
        String loginFail = Constants.LOGIN_FAIL;

        // 获取用户登录错误次数(可自定义限制策略 例如: key + username + ip)
        Integer errorNumber = RedisUtils.getCacheObject(errorKey);
        // 锁定时间内登录 则踢出
        if (ObjectUtil.isNotNull(errorNumber) && errorNumber.equals(maxRetryCount)) {
            recordLogininfor(username, loginFail, MessageUtils.message(loginType.getRetryLimitExceed(), maxRetryCount, lockTime));
            throw new UserException(loginType.getRetryLimitExceed(), maxRetryCount, lockTime);
        }

        if (supplier.get()) {
            // 是否第一次
            errorNumber = ObjectUtil.isNull(errorNumber) ? 1 : errorNumber + 1;
            // 达到规定错误次数 则锁定登录
            if (errorNumber.equals(maxRetryCount)) {
                RedisUtils.setCacheObject(errorKey, errorNumber, Duration.ofMinutes(lockTime));
                recordLogininfor(username, loginFail, MessageUtils.message(loginType.getRetryLimitExceed(), maxRetryCount, lockTime));
                throw new UserException(loginType.getRetryLimitExceed(), maxRetryCount, lockTime);
            } else {
                // 未达到规定错误次数 则递增
                RedisUtils.setCacheObject(errorKey, errorNumber);
                recordLogininfor(username, loginFail, MessageUtils.message(loginType.getRetryLimitCount(), errorNumber));
                throw new UserException(loginType.getRetryLimitCount(), errorNumber);
            }
        }

        // 登录成功 清空错误次数
        RedisUtils.deleteObject(errorKey);
    }

    /**
     * 发送手机验证码
     *
     * @param phone 手机号
     * @return 结果
     * @date 2023-03-27
     **/
    public String sendSmsCode(String phone) {
        // 1.判断缓存中是否有该号码的验证码缓存
        String redisKey = getSmsCodeKey(phone);
        String phoneRedisCode = RedisUtils.getCacheObject(redisKey);
        if(ObjectUtil.isNotNull(phoneRedisCode)){
            throw new UserException("code.frequently");
        }

        // 2.生成随机6位验证码,存到redis(60s有效期)
        String newSmsCode = "123456";
        RedisUtils.setCacheObject(redisKey, newSmsCode, Duration.ofMinutes(Constants.CODE_EXPIRATION));

        // 4.发送验证码到用户手机(对接移动云) todo 测试环境先不发
//        Map<String, String> map = new HashMap<>(1);
//        map.put("code", newSmsCode);
//        SmsResult sms_275430229 = smsTemplate.send(phone, "SMS_275430229", map);
        // String resultMsg = SmsUtils.sendSmsCode(newSmsCode, phone);

        return "操作成功";
    }

    /**
     * 获取手机验证码的redis key
     *
     * @param phone 手机号
     * @return String 拼接好的key
     * @date 2022/5/26
     **/
    private String getSmsCodeKey(String phone){
        return Constants.SMS_CODE_KEY + phone + ":";
    }

    /**
     * app用户首登完善信息
     *
     * @param body 请求参数
     * @return boolean
     * @date 2023-03-28
     **/
    @Transactional(rollbackFor = Exception.class)
    public boolean perfectInfo(PerfectInfoBody body) {
        // 1. 找到sys_user用户记录
        Long userId = LoginHelper.getUserId();
        SysUser sysUser = userService.selectUserById(userId);
        sysUser.setNickName(body.getRealName());
        userMapper.updateById(sysUser);

        // 2. 创建用户平台信息记录
        UserInfoBo userInfoBo = BeanCopyUtils.copy(body, UserInfoBo.class);
        userInfoBo.setUserId(userId);
        iUserInfoService.insertByBo(userInfoBo);

        return true;
    }

    private String generateSmsCode() {
        // 生成随机6位验证码
        return String.format("%06d", new Random().nextInt(999999));
    }


    public String xcxLogin(String xcxCode) {
        // xcxCode 为 小程序调用 wx.login 授权后获取
        // todo 以下自行实现
        // 校验 appid + appsrcret + xcxCode 调用登录凭证校验接口 获取 session_key 与 openid
        String openid = "";
        SysUser user = loadUserByOpenid(openid);

        // 此处可根据登录用户的数据不同 自行创建 loginUser
        XcxLoginUser loginUser = new XcxLoginUser();
        loginUser.setUserId(user.getUserId());
        loginUser.setUsername(user.getUserName());
        loginUser.setUserType(user.getUserType());
        loginUser.setOpenid(openid);
        // 生成token
        LoginHelper.loginByDevice(loginUser, DeviceType.XCX);

        recordLogininfor(user.getUserName(), Constants.LOGIN_SUCCESS, MessageUtils.message("user.login.success"));
        recordLoginInfo(user.getUserId(), user.getUserName());
        return StpUtil.getTokenValue();
    }
}
