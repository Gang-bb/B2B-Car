package com.ruoyi.business.common.domain;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * 地区对象 b_area
 *
 * @author Gangbb
 * @date 2023-03-31
 */
@Data
@TableName("b_area")
public class Area {

    private static final long serialVersionUID=1L;

    /**
     * ID
     */
    @TableId(value = "id")
    private Long id;
    /**
     * 父id
     */
    private Long pid;
    /**
     * 简称
     */
    private String shortname;
    /**
     * 名称
     */
    private String name;
    /**
     * 全称
     */
    private String mergename;
    /**
     * 层级 0 1 2 省市区县
     */
    private Long level;
    /**
     * 拼音
     */
    private String pinyin;
    /**
     * 长途区号
     */
    private String code;
    /**
     * 邮编
     */
    private String zip;
    /**
     * 首字母
     */
    private String first;
    /**
     * 经度
     */
    private String lng;
    /**
     * 纬度
     */
    private String lat;

}
