package com.ruoyi.business.common.domain.bo;

import lombok.Data;

/**
 * 地区业务对象 b_area
 *
 * @author Gangbb
 * @date 2023-03-31
 */

@Data
public class AreaBo {


    /**
     * 父id
     */
    private Long pid;

    /**
     * 简称
     */
    private String shortname;

    /**
     * 名称
     */
    private String name;

    /**
     * 层级 0-省  1-市 2-区县
     */
    private Long level;

    /**
     * 是否转成树 0-否 1-是(默认否)
     */
    private Integer isTree;

}
