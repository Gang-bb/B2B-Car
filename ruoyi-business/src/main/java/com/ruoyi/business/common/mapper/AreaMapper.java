package com.ruoyi.business.common.mapper;

import com.ruoyi.business.common.domain.Area;
import com.ruoyi.business.common.domain.vo.AreaVo;
import com.ruoyi.common.core.mapper.BaseMapperPlus;

/**
 * 地区Mapper接口
 *
 * @author Gangbb
 * @date 2023-03-31
 */
public interface AreaMapper extends BaseMapperPlus<AreaMapper, Area, AreaVo> {

}
