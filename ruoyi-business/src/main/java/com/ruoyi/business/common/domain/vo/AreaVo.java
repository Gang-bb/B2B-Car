package com.ruoyi.business.common.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import lombok.Data;

import java.util.List;


/**
 * 地区视图对象 b_area
 *
 * @author Gangbb
 * @date 2023-03-31
 */
@Data
@ExcelIgnoreUnannotated
public class AreaVo {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    private Long id;

    /**
     * 父id
     */
    private Long pid;

    /**
     * 简称
     */
    private String shortname;

    /**
     * 名称
     */
    private String name;

    /**
     * 全称
     */
    private String mergename;

    /**
     * 层级 0 1 2 省市区县
     */
    private Long level;

    /**
     * 拼音
     */
    private String pinyin;

    /**
     * 长途区号
     */
    private String code;

    /**
     * 邮编
     */
    private String zip;

    /**
     * 首字母
     */
    private String first;

    /**
     * 经度
     */
    private String lng;

    /**
     * 纬度
     */
    private String lat;

    /**
     * 子地区列表
     */
    private List<AreaVo> children;

}
