package com.ruoyi.business.common.service;

import com.ruoyi.business.common.domain.bo.AreaBo;
import com.ruoyi.business.common.domain.vo.AreaVo;

import java.util.List;

/**
 * 地区Service接口
 *
 * @author Gangbb
 * @date 2023-03-31
 */
public interface IAreaService {

    /**
     * 查询地区列表
     */
    List<AreaVo> queryList(AreaBo bo);

}
