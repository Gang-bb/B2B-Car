package com.ruoyi.business.common.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.ruoyi.business.common.domain.bo.AreaBo;
import com.ruoyi.business.common.domain.vo.AreaVo;
import com.ruoyi.business.common.service.IAreaService;
import com.ruoyi.common.core.controller.BaseController;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 选择地区接口
 *
 * @author Gangbb
 * @date 2023-03-31
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/car/area")
public class AreaController extends BaseController {

    private final IAreaService iAreaService;

    /**
     * 查询地区列表
     */
    @SaCheckPermission("car:area:list")
    @GetMapping("/list")
    public List<AreaVo> list(AreaBo bo) {
        return iAreaService.queryList(bo);
    }
}
