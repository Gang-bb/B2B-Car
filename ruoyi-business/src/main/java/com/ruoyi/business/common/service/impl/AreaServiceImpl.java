package com.ruoyi.business.common.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.ruoyi.business.common.domain.Area;
import com.ruoyi.business.common.domain.bo.AreaBo;
import com.ruoyi.business.common.domain.vo.AreaVo;
import com.ruoyi.business.common.mapper.AreaMapper;
import com.ruoyi.business.common.service.IAreaService;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.TreeUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 地区Service业务层处理
 *
 * @author Gangbb
 * @date 2023-03-31
 */
@RequiredArgsConstructor
@Service
public class AreaServiceImpl implements IAreaService {

    private final AreaMapper baseMapper;


    /**
     * 查询地区列表
     */
    @Override
    public List<AreaVo> queryList(AreaBo bo) {
        LambdaQueryWrapper<Area> lqw = buildQueryWrapper(bo);
        List<AreaVo> areaVoList = baseMapper.selectVoList(lqw);
        if(ObjectUtil.isNotEmpty(bo.getIsTree()) && 1 == bo.getIsTree()){
            return TreeUtils.build(areaVoList, "id", "pid");
        }
        return areaVoList;
    }

    private LambdaQueryWrapper<Area> buildQueryWrapper(AreaBo bo) {
        LambdaQueryWrapper<Area> lqw = Wrappers.lambdaQuery();
        lqw.eq(bo.getPid() != null, Area::getPid, bo.getPid());
        lqw.like(StringUtils.isNotBlank(bo.getShortname()), Area::getShortname, bo.getShortname());
        lqw.like(StringUtils.isNotBlank(bo.getName()), Area::getName, bo.getName());
        lqw.eq(bo.getLevel() != null, Area::getLevel, bo.getLevel());
        return lqw;
    }
}
