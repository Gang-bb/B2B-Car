package com.ruoyi.business.app.user;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.ruoyi.business.framework.user.service.IUserInfoService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.core.domain.model.UserInfoAppMineVo;
import com.ruoyi.common.helper.LoginHelper;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 用户平台信息 app接口
 *
 * @author Gangbb
 * @date 2023-04-28
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/app/user/userInfo")
public class UserInfoAppController extends BaseController {

    private final IUserInfoService iUserInfoService;

    /**
     * 我的页面信息接口
     *
     */
    @SaCheckPermission("(app)user:userInfo:getMyInfo")
    @GetMapping("/getMyInfo")
    public R<UserInfoAppMineVo> getInfo() {
        Long userId = LoginHelper.getUserId();
        UserInfoAppMineVo appMyInfo = iUserInfoService.getAppMyInfo(userId);
        return R.ok(appMyInfo);
    }
}
