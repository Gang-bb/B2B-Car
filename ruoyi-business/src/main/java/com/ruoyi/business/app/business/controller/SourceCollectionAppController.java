package com.ruoyi.business.app.business.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.ruoyi.business.app.business.domain.bo.SourceCollectionAppBo;
import com.ruoyi.business.app.business.domain.vo.SourceCollectionAppVo;
import com.ruoyi.business.framework.business.service.ISourceCollectionService;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.annotation.RepeatSubmit;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.PageQuery;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.core.validate.AddGroup;
import com.ruoyi.common.enums.BusinessType;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;

/**
 * 车源收藏 app接口
 *
 * @author FYZ
 * @date 2023-04-16
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/app/business/sourceCollection")
public class SourceCollectionAppController extends BaseController {

    private final ISourceCollectionService iSourceCollectionService;

    /**
     * app 查询车源收藏列表
     */
    @SaCheckPermission("(app)business:sourceCollection:list")
    @GetMapping("/list")
    public TableDataInfo<SourceCollectionAppVo> appList(PageQuery pageQuery) {
        return iSourceCollectionService.appList(pageQuery);
    }

    /**
     * app 获取车源收藏id
     *
     * @param carSourceId 车源id
     */
    @SaCheckPermission("(app)business:sourceCollection:query")
    @GetMapping("/{carSourceId}")
    public R<Long> getSourceCollectionId(@NotNull(message = "车源id不能为空") @PathVariable Long carSourceId) {
        return R.ok(iSourceCollectionService.queryByCarSourceId(carSourceId));
    }

    /**
     * app 新增车源收藏
     */
    @SaCheckPermission("(app)business:sourceCollection:add")
    @Log(title = "新增车源收藏 app接口", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody SourceCollectionAppBo bo) {
        return toAjax(iSourceCollectionService.insertBycCarSourceId(bo));
    }

    /**
     * app 删除车源收藏
     *
     * @param carSourceId 车源id
     */
    @SaCheckPermission("(app)business:sourceCollection:remove")
    @Log(title = "删除车源收藏 app接口", businessType = BusinessType.DELETE)
    @DeleteMapping("/{carSourceId}")
    public R<Void> remove(@NotNull(message = "车源id不能为空") @PathVariable Long carSourceId) {
        return toAjax(iSourceCollectionService.deleteByCarSourceId(carSourceId));
    }

    /**
     * app 清空车源收藏
     *
     */
    @SaCheckPermission("(app)business:sourceCollection:removeAll")
    @Log(title = "清空车源收藏 app接口", businessType = BusinessType.DELETE)
    @DeleteMapping("/removeAll")
    public R<Void> removeAll() {
        return toAjax(iSourceCollectionService.deleteAllByUserId());
    }
}
