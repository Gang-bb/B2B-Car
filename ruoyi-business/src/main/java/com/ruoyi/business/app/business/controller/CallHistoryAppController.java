package com.ruoyi.business.app.business.controller;


import cn.dev33.satoken.annotation.SaCheckPermission;
import com.ruoyi.business.app.business.domain.bo.CallHistoryAppBo;
import com.ruoyi.business.app.business.domain.vo.CallHistoryAppVo;
import com.ruoyi.business.framework.business.service.ICallHistoryService;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.annotation.RepeatSubmit;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.PageQuery;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.core.validate.AddGroup;
import com.ruoyi.common.enums.BusinessType;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * 通话记录 app接口
 *
 * @author FYZ
 * @date 2023-04-16
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/app/business/callHistory")
public class CallHistoryAppController extends BaseController {

    private final ICallHistoryService iCallHistoryService;

    /**
     * app 查询通话记录列表
     *
     * @param callFlag 通话标志 1:联系我的，2:我联系的
     */
    @SaCheckPermission("(app)business:callHistory:list")
    @GetMapping("/list")
    public TableDataInfo<CallHistoryAppVo> list(@NotEmpty(message = "通话标志不能为空") String callFlag, PageQuery pageQuery) {
        return iCallHistoryService.appList(callFlag, pageQuery);
    }

    /**
     * app 新增通话记录
     */
    @SaCheckPermission("(app)business:callHistory:add")
    @Log(title = "新增通话记录 app接口", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody CallHistoryAppBo bo) {
        return toAjax(iCallHistoryService.insertBycCarSourceId(bo));
    }

    /**
     * app 删除通话记录
     *
     * @param carSourceId 车源id
     */
    @SaCheckPermission("(app)business:callHistory:remove")
    @Log(title = "删除通话记录 app接口", businessType = BusinessType.DELETE)
    @DeleteMapping("/{carSourceId}")
    public R<Void> remove(@NotNull(message = "车源id不能为空") @PathVariable Long carSourceId) {
        return toAjax(iCallHistoryService.deleteByCarSourceId(carSourceId));
    }

    /**
     * app 清空通话记录
     *
     */
    @SaCheckPermission("(app)business:sourceCollection:removeAll")
    @Log(title = "清空通话记录 app接口", businessType = BusinessType.DELETE)
    @DeleteMapping("/removeAll")
    public R<Void> removeAll() {
        return toAjax(iCallHistoryService.deleteAllByUserId());
    }
}
