package com.ruoyi.business.app.business.controller;


import cn.dev33.satoken.annotation.SaCheckPermission;
import com.ruoyi.business.app.business.domain.bo.CarHistoryAppBo;
import com.ruoyi.business.app.business.domain.vo.CarHistoryAppVo;
import com.ruoyi.business.framework.business.service.ICarHistoryService;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.annotation.RepeatSubmit;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.PageQuery;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.core.validate.AddGroup;
import com.ruoyi.common.enums.BusinessType;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import javax.validation.constraints.NotNull;

/**
 * 车源浏览足迹 app接口
 *
 * @author FYZ
 * @date 2023-04-16
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/app/business/carHistory")
public class CarHistoryAppController extends BaseController {

    private final ICarHistoryService iCarHistoryService;

    /**
     * app 查询车源浏览足迹列表
     */
    @SaCheckPermission("(app)business:carHistory:list")
    @GetMapping("/list")
    public TableDataInfo<CarHistoryAppVo> list(PageQuery pageQuery) {
        return iCarHistoryService.appList(pageQuery);
    }

    /**
     * app 新增车源浏览足迹
     */
    @SaCheckPermission("(app)business:carHistory:add")
    @Log(title = "新增车源浏览足迹 app接口", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody CarHistoryAppBo bo) {
        return toAjax(iCarHistoryService.insertBycCarSourceId(bo));
    }

    /**
     * app 删除车源浏览足迹
     *
     * @param carSourceId 车源id
     */
    @SaCheckPermission("(app)business:carHistory:remove")
    @Log(title = "删除车源浏览足迹 app接口", businessType = BusinessType.DELETE)
    @DeleteMapping("/{carSourceId}")
    public R<Void> remove(@NotNull(message = "主键不能为空") @PathVariable Long carSourceId) {
        return toAjax(iCarHistoryService.deleteByCarSourceId(carSourceId));
    }

    /**
     * app 清空车源浏览足迹
     *
     */
    @SaCheckPermission("(app)business:carHistory:removeAll")
    @Log(title = "清空车源浏览足迹 app接口", businessType = BusinessType.DELETE)
    @DeleteMapping("/removeAll")
    public R<Void> removeAll() {
        return toAjax(iCarHistoryService.deleteAllByUserId());
    }
}
