package com.ruoyi.business.app.car.domain.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 选择车系页面1 返回参数
 *
 * @author Gangbb
 * @date 2023-04-15
 **/
@Data
public class CarSeriesPickListOneVo implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 汽车类型id
     */
    private Long carCategoryId;

    /**
     * 汽车类型名
     */
    private String carCategoryName;


    /**
     * 车系列表数据
     */
    private List<SeriesPickListItemVo> seriesPickList;

}
