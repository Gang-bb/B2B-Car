package com.ruoyi.business.app.car.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.ruoyi.business.framework.car.domain.bo.CarBrandBo;
import com.ruoyi.business.framework.car.domain.vo.CarBrandVo;
import com.ruoyi.business.framework.car.service.ICarBrandService;
import com.ruoyi.common.core.domain.R;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 汽车品牌app接口
 *
 * @author Gangbb
 * @date 2023-04-06
 **/
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/app/car/brand")
public class CarBrandAppController {

    private final ICarBrandService iCarBrandService;

    /**
     * 查询汽车品牌列表
     */
    @SaCheckPermission("(app)car:brand:list")
    @GetMapping("/list")
    public R<List<CarBrandVo>> list(CarBrandBo bo) {
        return R.ok(iCarBrandService.queryList(bo));
    }
}
