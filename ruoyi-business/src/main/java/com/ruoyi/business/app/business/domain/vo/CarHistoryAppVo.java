package com.ruoyi.business.app.business.domain.vo;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 车源浏览足迹 app接口 响应参数
 *
 * @author FYZ
 * @date 2023-04-16
 */
@Data
public class CarHistoryAppVo implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 车源浏览足迹id
     */
    private Long carHistoryId;
    /**
     * 关联车源id;(b_car_source.id)
     */
    private Long carSourceId;
    /**
     * 关联汽车型号id;(car_model.id)
     */
    private Long carModelId;
    /**
     * 车源信息(汽车型号名 +指导价)
     */
    private String carSourceInfo;
    /**
     * 车源是否有图片(0-否 1-是)
     */
    private Integer isHasImg;
    /**
     * 车源图片id(多个逗号隔开)
     */
    private String sourceImg;
    /**
     * 最终价格
     */
    private BigDecimal finalPrice;
    /**
     * 关联汽车类型id;(car_category.id)
     */
    private Long carCategoryId;
    /**
     * 关联汽车类型名称
     */
    private String carCategoryName;
    /**
     * 排放标准
     */
    private String sourceDischarge;
    /**
     * 外观颜色
     */
    private String appearanceColor;
    /**
     * 内饰颜色
     */
    private String interiorColor;
    /**
     * 车源手续(字典：source_procedures)
     */
    private String sourceProcedures;
    /**
     * 销售区域
     */
    private String salesArea;
    /**
     * 车源所在地
     */
    private String sourceLocation;
    /**
     * 是否自营车源(0-否 1-是)
     */
    private Integer isSelfSupport;
    /**
     * 关联用户id;(sys_user.id)
     */
    private Long userId;
    /**
     * 发布人员姓名(所在省)
     */
    private String publishUserName;
    /**
     * 发布时间
     */
    private Date createTime;
}
