package com.ruoyi.business.app.car.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.ruoyi.business.app.car.domain.vo.CarSeriesPickListOneVo;
import com.ruoyi.business.framework.car.service.ICarSeriesService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.R;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 汽车车系 app端接口
 *
 * @author Gangbb
 * @date 2023-04-15
 **/
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/app/car/series")
public class CarSeriesAppController extends BaseController {

    private final ICarSeriesService iCarSeriesService;


    /**
     * 选择车系页面1 对应接口
     *
     * @param carBrandId  汽车品牌id
     * @return R<List<CarSeriesPickListOneVo>>
     * @date 2023-04-15
     **/
    @SaCheckPermission("(app)car:series:pickList")
    @GetMapping("/pickListOne")
    public R<List<CarSeriesPickListOneVo>> pickListOne(@RequestParam(value = "carBrandId") Long carBrandId) {
        return R.ok(iCarSeriesService.pickListOne(carBrandId));
    }
}
