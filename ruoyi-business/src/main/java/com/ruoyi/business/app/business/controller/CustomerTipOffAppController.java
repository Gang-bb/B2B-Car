package com.ruoyi.business.app.business.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.ruoyi.business.app.business.domain.bo.CustomerTipOffAppBo;
import com.ruoyi.business.app.business.domain.vo.CustomerTipOffAppVo;
import com.ruoyi.business.framework.business.service.ICustomerTipOffService;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.annotation.RepeatSubmit;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.PageQuery;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.core.validate.AddGroup;
import com.ruoyi.common.enums.BusinessType;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;

/**
 * 客户举报 app接口
 *
 * @author FYZ
 * @date 2023-04-16
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/app/business/customerTipOff")
public class CustomerTipOffAppController extends BaseController {

    private final ICustomerTipOffService iCustomerTipOffService;

    /**
     * app 查询客户举报列表
     */
    @SaCheckPermission("(app)business:customerTipOff:list")
    @GetMapping("/list")
    public TableDataInfo<CustomerTipOffAppVo> appList(PageQuery pageQuery) {
        return iCustomerTipOffService.appList(pageQuery);
    }

    /**
     * app 获取客户举报id
     *
     * @param carSourceId 车源id
     */
    @SaCheckPermission("(app)business:customerTipOff:query")
    @GetMapping("/{carSourceId}")
    public R<Long> getSourceCollectionId(@NotNull(message = "车源id不能为空") @PathVariable Long carSourceId) {
        return R.ok(iCustomerTipOffService.queryByCarSourceId(carSourceId));
    }

    /**
     * app 新增客户举报
     */
    @SaCheckPermission("(app)business:customerTipOff:add")
    @Log(title = "新增客户举报 app接口", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody CustomerTipOffAppBo bo) {
        return toAjax(iCustomerTipOffService.insertBycCarSourceId(bo));
    }

    /**
     * app 删除客户举报
     *
     * @param carSourceId 车源id
     */
    @SaCheckPermission("(app)business:customerTipOff:remove")
    @Log(title = "删除客户举报 app接口", businessType = BusinessType.DELETE)
    @DeleteMapping("/{carSourceId}")
    public R<Void> remove(@NotNull(message = "车源id不能为空") @PathVariable Long carSourceId) {
        return toAjax(iCustomerTipOffService.deleteByCarSourceId(carSourceId));
    }

    /**
     * app 清空客户举报
     *
     */
    @SaCheckPermission("(app)business:customerTipOff:removeAll")
    @Log(title = "清空客户举报 app接口", businessType = BusinessType.DELETE)
    @DeleteMapping("/removeAll")
    public R<Void> removeAll() {
        return toAjax(iCustomerTipOffService.deleteAllByUserId());
    }
}
