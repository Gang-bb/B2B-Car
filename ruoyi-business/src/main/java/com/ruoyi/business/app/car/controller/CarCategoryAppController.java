package com.ruoyi.business.app.car.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.ruoyi.business.framework.car.domain.bo.CarCategoryBo;
import com.ruoyi.business.framework.car.domain.vo.CarCategoryVo;
import com.ruoyi.business.framework.car.service.ICarCategoryService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.R;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 汽车类型app接口
 *
 * @author Gangbb
 * @date 2023-04-06
 **/
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/app/car/category")
public class CarCategoryAppController extends BaseController {
    private final ICarCategoryService iCarCategoryService;

    /**
     * 查询汽车类型列表
     */
    @SaCheckPermission("(app)car:category:list")
    @GetMapping("/list")
    public R<List<CarCategoryVo>> list(CarCategoryBo bo) {
        return R.ok(iCarCategoryService.queryList(bo));
    }
}
