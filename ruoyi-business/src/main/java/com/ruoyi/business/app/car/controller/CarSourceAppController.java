package com.ruoyi.business.app.car.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.ruoyi.business.app.car.domain.bo.CarSourceAppBo;
import com.ruoyi.business.app.car.domain.vo.CarSourceAppVo;
import com.ruoyi.business.framework.car.service.ICarSourceService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.PageQuery;
import com.ruoyi.common.core.page.TableDataInfo;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 车源 app接口
 *
 * @author Gangbb
 * @date 2023-04-15
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/app/car/source")
public class CarSourceAppController extends BaseController {

    private final ICarSourceService iCarSourceService;

    /**
     * app查询车源列表
     */
    @SaCheckPermission("(app)car:source:list")
    @GetMapping("/list")
    public TableDataInfo<CarSourceAppVo> appList(CarSourceAppBo bo, PageQuery pageQuery) {
        return iCarSourceService.appList(bo, pageQuery);
    }

    /**
     * todo 类型筛选条件统计
     */

    /**
     * todo 外观筛选条件统计
     */

    /**
     * todo 地区筛选条件统计
     */

    /**
     * todo 发布单台车源、同城车源
     */

    /**
     * todo 发布批量车源
     */

}
