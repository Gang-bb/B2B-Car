package com.ruoyi.business.app.car.domain.bo;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * 车源 app接口 请求参数
 *
 * @author Gangbb
 * @date 2023-04-16
 **/
@Data
public class CarSourceAppBo {

    /**
     * 关联汽车类型id;(car_category.id)
     */
    private Long carCategoryId;
    /**
     * 外观颜色(字典：pick_appearance_color)
     */
    private String appearanceColor;
    /**
     * 车源省份
     */
    private String province;
    /**
     * 最低价格
     */
    private BigDecimal lowestPrice;
    /**
     * 最高价格
     */
    private BigDecimal highestPrice;
    /**
     * 排放标准(字典：pick_source_discharge)
     */
    private String sourceDischarge;
    /**
     * 车源货期(字典：source_delivery)
     */
    private String sourceDelivery;
    /**
     * 排序(字典：pick_source_sort)
     */
    private String sourceSortType;

    /**
     * 只查这些id字段
     */
    private List<Long> onlyNeedIdList;
}
