package com.ruoyi.business.app.business.domain.bo;

import com.ruoyi.common.core.validate.AddGroup;
import com.ruoyi.common.core.validate.EditGroup;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * 通话记录 app接口 请求参数
 *
 * @author FYZ
 * @date 2023-04-22
 */
@Data
public class CallHistoryAppBo {

    /**
     * 关联车源id;(car_source.id)
     */
    @NotNull(message = "关联车源id;(car_source.id)不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long carSourceId;

}
