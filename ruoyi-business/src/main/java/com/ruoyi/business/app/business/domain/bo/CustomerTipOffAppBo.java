package com.ruoyi.business.app.business.domain.bo;

import com.ruoyi.common.core.validate.AddGroup;
import com.ruoyi.common.core.validate.EditGroup;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * 客户举报 app接口 请求参数
 *
 * @author FYZ
 * @date 2023-04-16
 */
@Data
public class CustomerTipOffAppBo {

    /**
     * 关联车源id;(car_source.id)
     */
    @NotNull(message = "关联车源id;(car_source.id)不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long carSourceId;

    /**
     * 举报类型;(字典：tip_off_type)
     * 1-车型不符、2-此车已售、3-虚假车源、4-虚假价格、5-联系不上、6-举报商家
     *
     */
    @NotBlank(message = "举报类型;(字典：tip_off_type)不能为空", groups = { AddGroup.class, EditGroup.class })
    private String tipOffType;

    /**
     * 举报内容
     */
    @NotBlank(message = "举报内容不能为空", groups = { AddGroup.class, EditGroup.class })
    private String tipOffContent;
}
