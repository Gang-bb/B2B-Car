package com.ruoyi.business.app.car.domain.vo;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * 选择车系页面1 车系每项
 *
 * @author Gangbb
 * @date 2023-04-15
 **/
@Data
public class SeriesPickListItemVo {

    /**
     * 汽车车系id
     */
    private Long carSeriesId;
    /**
     * 汽车车系名称
     */
    private String carSeriesName;
    /**
     * 父ID
     */
    private Long parentId;
    /**
     * 子列表
     */
    private List<SeriesPickListItemVo> children = new ArrayList<>();

}
