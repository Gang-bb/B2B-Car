import request from '@/utils/request'

// 查询汽车车系列表
export function listSeries(query) {
    return request({
        url: '/car/series/list',
        method: 'get',
        params: query
    })
}

// 查询汽车车系详细
export function getSeries(carSeriesId) {
    return request({
        url: '/car/series/' + carSeriesId,
        method: 'get'
    })
}

// 新增汽车车系
export function addSeries(data) {
    return request({
        url: '/car/series',
        method: 'post',
        data: data
    })
}

// 修改汽车车系
export function updateSeries(data) {
    return request({
        url: '/car/series',
        method: 'put',
        data: data
    })
}

// 删除汽车车系
export function delSeries(carSeriesId) {
    return request({
        url: '/car/series/' + carSeriesId,
        method: 'delete'
    })
}
