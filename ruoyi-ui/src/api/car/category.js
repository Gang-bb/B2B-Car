import request from '@/utils/request'

// 分页查询汽车类型列表
export function listCategory(query) {
    return request({
        url: '/car/category/list',
        method: 'get',
        params: query
    })
}

// 分页查询汽车类型列表
export function searchListCategory(query) {
    return request({
        url: '/car/category/allList',
        method: 'get',
        params: query
    })
}

// 查询汽车类型详细
export function getCategory(carCategoryId) {
    return request({
        url: '/car/category/' + carCategoryId,
        method: 'get'
    })
}

// 新增汽车类型
export function addCategory(data) {
    return request({
        url: '/car/category',
        method: 'post',
        data: data
    })
}

// 修改汽车类型
export function updateCategory(data) {
    return request({
        url: '/car/category',
        method: 'put',
        data: data
    })
}

// 删除汽车类型
export function delCategory(carCategoryId) {
    return request({
        url: '/car/category/' + carCategoryId,
        method: 'delete'
    })
}
