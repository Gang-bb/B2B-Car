import request from '@/utils/request'

// 查询汽车型号列表
export function listModel(query) {
    return request({
        url: '/car/model/list',
        method: 'get',
        params: query
    })
}

// 查询汽车型号详细
export function getModel(carModelId) {
    return request({
        url: '/car/model/' + carModelId,
        method: 'get'
    })
}

// 新增汽车型号
export function addModel(data) {
    return request({
        url: '/car/model',
        method: 'post',
        data: data
    })
}

// 修改汽车型号
export function updateModel(data) {
    return request({
        url: '/car/model',
        method: 'put',
        data: data
    })
}

// 删除汽车型号
export function delModel(carModelId) {
    return request({
        url: '/car/model/' + carModelId,
        method: 'delete'
    })
}
