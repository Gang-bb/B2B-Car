import request from '@/utils/request'

// 分页查询汽车品牌列表
export function listBrand(query) {
    return request({
        url: '/car/brand/list',
        method: 'get',
        params: query
    })
}

// 查询汽车品牌列表
export function searchListBrand(query) {
    return request({
        url: '/car/brand/allList',
        method: 'get',
        params: query
    })
}

// 查询汽车品牌详细
export function getBrand(carBrandId) {
    return request({
        url: '/car/brand/' + carBrandId,
        method: 'get'
    })
}

// 新增汽车品牌
export function addBrand(data) {
    return request({
        url: '/car/brand',
        method: 'post',
        data: data
    })
}

// 修改汽车品牌
export function updateBrand(data) {
    return request({
        url: '/car/brand',
        method: 'put',
        data: data
    })
}

// 删除汽车品牌
export function delBrand(carBrandId) {
    return request({
        url: '/car/brand/' + carBrandId,
        method: 'delete'
    })
}
