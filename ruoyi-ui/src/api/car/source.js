import request from '@/utils/request'

// 查询车源列表
export function listSource(query) {
    return request({
        url: '/car/source/list',
        method: 'get',
        params: query
    })
}

// 查询车源详细
export function getSource(carSourceId) {
    return request({
        url: '/car/source/' + carSourceId,
        method: 'get'
    })
}

// 新增车源
export function addSource(data) {
    return request({
        url: '/car/source',
        method: 'post',
        data: data
    })
}

// 修改车源
export function updateSource(data) {
    return request({
        url: '/car/source',
        method: 'put',
        data: data
    })
}

// 删除车源
export function delSource(carSourceId) {
    return request({
        url: '/car/source/' + carSourceId,
        method: 'delete'
    })
}
